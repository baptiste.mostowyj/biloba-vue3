<?php
use App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', [UserController::class, 'index']);
$router->group(['prefix' => 'orders'], function () use ($router) {
    $router->get('partielles/{dotype}', [Api\Orders\PartielleController::class, 'index']);
});

$router->group(['prefix' => 'products'], function () use ($router) {
    $router->get('{ref}', [Api\Products\Product::class, 'index']);
    $router->get('/cf/suivi/all', [Api\Products\CF\SuiviCF::class, 'index']);

    $router->get('/cf/creation/fournisseur/all', [Api\Products\CF\CreationCF::class, 'index']);
    $router->get('/cf/creation/{fournisseur}', [Api\Products\CF\CreationCF::class, 'show']);
});

$router->group(['prefix' => 'operations'], function () use ($router) {
    $router->get('', [Api\Operation\Operation::class, 'index']);
    $router->get('{ref}', [Api\Products\Product::class, 'index']);
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->get('/', [Api\Users\User::class, 'index']);
    $router->put('{id}', [Api\Users\User::class, 'update']);
    $router->delete('{id}', [Api\Users\User::class, 'destroy']);  
    $router->post('/post', [Api\Users\User::class, 'store']);  
});

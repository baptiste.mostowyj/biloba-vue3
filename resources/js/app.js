import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap';
import './fontawesome.js'
import { createApp } from 'vue';
import { createPinia } from 'pinia'
import router from './router/index';
import AppComponent from './App.vue';
import VueApexCharts from 'vue-apexcharts'
import { vfmPlugin } from 'vue-final-modal'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueGoodTablePlugin from 'vue-good-table-next';

// import the styles 
import 'vue-good-table-next/dist/vue-good-table-next.css'



const pinia = createPinia()
const app = createApp({
    components:{
        AppComponent,
    }
    
})

app.use(vfmPlugin)
app.use(pinia);
app.use(VueApexCharts)
app.use(router);
app.use(VueGoodTablePlugin);
app.component('font-awesome-icon', FontAwesomeIcon) 
app.component('apexchart', VueApexCharts)
//app.component('EasyDataTable', Vue3EasyDataTable);
app.mount("#app");
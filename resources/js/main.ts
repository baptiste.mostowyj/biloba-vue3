import 'bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import './fontawesome.js'
import 'vue3-easy-data-table/dist/style.css';
import App from './App.vue';
import { AuthenticationResult, EventType } from "@azure/msal-browser";
import { createApp } from 'vue';
import { createPinia } from 'pinia'
import { CustomNavigationClient } from "./router/NavigationClient";
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { msalInstance } from "./authConfig";
import { msalPlugin } from "./plugins/msalPlugin";
import router from './router/index';
import Vue3EasyDataTable from "vue3-easy-data-table";
import VueApexCharts from "vue3-apexcharts";

const pinia = createPinia()
  
const navigationClient = new CustomNavigationClient(router);
msalInstance.setNavigationClient(navigationClient);

// Account selection logic is app dependent. Adjust as needed for different use cases.
const accounts = msalInstance.getAllAccounts();

if (accounts.length > 0) {
    msalInstance.setActiveAccount(accounts[0]);
}
msalInstance.addEventCallback((event) => {
  if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
    const payload = event.payload as AuthenticationResult;
    const account = payload.account;
    msalInstance.setActiveAccount(account);
  }
});

const app = createApp(App);


app.use(pinia)
app.use(msalPlugin, msalInstance);
app.use(router);
app.use(VueApexCharts)
app.component('apexchart', VueApexCharts) 
app.component('font-awesome-icon', FontAwesomeIcon)
app.component("EasyDataTable", Vue3EasyDataTable);
router.isReady().then(() => {
  // Waiting for the router to be ready prevents race conditions when returning from a loginRedirect or acquireTokenRedirect
  app.mount('#app');
});
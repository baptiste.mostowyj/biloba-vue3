import axios from "axios";

const axiosClient = axios.create({
    baseURL: "https://biloba-vue3.test/api/products/cf"
})
export async function getCfsSuivi() {
    try{
        const { data } = await axiosClient.get('/suivi/all');
        return [null,data];
    }catch(error){
        return [error];
    }
}

export async function getFournisseurCF() {
    try{
        const { data } = await axiosClient.get('/creation/fournisseur/all');
        return [null,data];
    }catch(error){
        return [error];
    }
}

export async function getSuggestionCommande(four:String,sugg:Boolean = false) {
    try{
        const { data } = await axiosClient.get(`/creation/${four}?sugg=${sugg}`);
        return [null,data];
    }catch(error){
        return [error];
    }
}
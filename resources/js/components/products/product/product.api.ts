import axios from "axios";

const axiosClient = axios.create({
    baseURL: "https://biloba-vue3.test/api/products"
})
export async function getProduct(ref:string) {
    try{
        const { data } = await axiosClient.get(`/${ref}`);
        return [null,data];
    }catch(error){
        return [error];
    }
}
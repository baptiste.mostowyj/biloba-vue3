import { getCurrentInstance, computed, ref, inject, watch } from 'vue'

export default function useItem (props:object) {
    const isFirstLevel = computed(() => {
        return props.level === 1
    })
}
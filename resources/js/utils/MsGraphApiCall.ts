import { graphConfig } from "../authConfig";

export async function callMsGraph(accessToken: string) {
    const headers = new Headers();
    const bearer = `Bearer ${accessToken}`;

    headers.append("Authorization", bearer);

    const options = {
        method: "GET",
        headers: headers
    };

    return fetch(graphConfig.graphMeEndpoint, options)
        .then(response => response.json())
        .catch(error => {
            console.log(error);
            throw error;
        });
}
export async function callMsGraphImg(accessToken: string) {
    const headers = new Headers();
    const bearer = `Bearer ${accessToken}`;

    headers.append("Authorization", bearer);

    const options = {
        method: "GET",
        headers: headers
    };
    return fetch(graphConfig.graphImgEndpoint, options)
        .then(response => response.blob())
        .catch(error => {
            console.log(error);
            throw error;
        });
}

export async function callMsGraphLastCo(accessToken: string,id:string) {
    const headers = new Headers();
    const bearer = `Bearer ${accessToken}`;

    headers.append("Authorization", bearer);

    const options = {
        method: "GET",
        headers: headers
    };
    return fetch(`${graphConfig.graphLastCo}/${id}/authentication` , options)
        .then(response => response)
        .catch(error => {
            console.log(error);
            throw error;
        });
}
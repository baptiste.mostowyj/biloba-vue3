import { createRouter, createWebHistory } from "vue-router";
import { h, markRaw } from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { registerGuard } from "./Guard";
import AuthCallback from '../views/AuthCallback.vue';
import Administration from '../components/administration/Administration.vue';
import CreationCF from "../components/products/CF/CreationCF.vue"
import Dashboard from "../views/Dashboard.vue";
import Partielle from "../components/orders/partielles/Partielles.vue";
import Product from "../components/products/product/Product.vue";
import Operations from "../components/operations/Operation.vue"
import SuiviCF from "../components/products/CF/SuiviCF.vue"
import Welcome from '../views/Welcome.vue'
const separator = {
  template: '<hr style="border: 1px solid ; margin: 20px;">'
}
const faIcon = (props:any) => {
  return {
    element: markRaw({
      render: () => h('div', [
        h(FontAwesomeIcon, { size: 'lg', ...props })
      ])
    })
  }
}

const routes = [
    { 
      path: '/', 
      component: Welcome,
      name:'Welcome',
      icon:'fa-solid fa-hands-clapping',
      hidden: true,
    },
    { 
      path: '/auth/callback', 
      component: AuthCallback,  
      icon:'fa-solid fa-box',
      hidden: true,
    },
    {
        path: '/orders',
        name: 'Commandes',
        icon: 'fa-solid fa-cart-shopping' ,
        meta: { requiresAuth: true },
        children: [
            {
                path: 'partielles',
                name: 'Partielles',
                component: Partielle,
                icon: 'fa-solid fa-box',
                hidden: false,
                meta: { requiresAuth: true },
            },
            
        ]
    },
    {
      path: '/products',
      name: 'Produits',
      icon:'fa-solid fa-seedling',
      children: [
            {
              path: 'product/:id',
              name:'Product',
              component: Product,
              props: (route:any) => ({
                id: route.params.id,
              }),
              icon:'fa-solid fa-seedling',
              hidden: true
          },
          {
              path: 'cfcreation',
              name: 'Création CF',
              component:  CreationCF,
              icon:'fa-solid fa-receipt',
              hidden: false
          },
          {
            path: 'cfsuivi',
            name: 'Commandes fournisseur',
            component: SuiviCF,
            icon:'fa-solid fa-truck-ramp-box',
            hidden: false
        },
          
      ]
    },
    {
      path: '/supplier',
      name: 'Fournisseur',
      icon: 'fa-solid fa-truck-field',
      children: [
          {
              path: 'cfcreation',
              name: 'TEST 2',
              component: Partielle,
              icon:'fa-solid fa-receipt',
              hidden: false
          },
          
      ]
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      icon: 'fa-solid fa-table-columns',
    },
    {
      path: '/offre',
      name: 'Offre Commerciale',
      icon: 'fa-solid fa-calendar-days',
      children: [
          {
              path: 'operations',
              name: 'Planning',
              component: Operations,
              icon: 'fa-solid fa-calendar-days',
              hidden: false
          },
          
      ]
    },
    {
      path: '/check',
      name: 'Checks',
      icon: 'fa-solid fa-clipboard-check' ,
      children:[
      {
          path: 'cfcreation',
          name: 'TEST 4',
          component: Partielle,
          icon:'fa-solid fa-receipt',
          hidden: false
      },
      ]
    },
    {
      path: '/tools',
      name: 'Outils',
      icon: 'fa-solid fa-screwdriver-wrench',
      children: [
          {
              path: 'cfcreation',
              name: 'Creation CF',
              component: Partielle,
              icon:'fa-solid fa-receipt',
              hidden: false
          },
          
      ]
    },
    {
      path: '/rc',
      name: 'Relation Client',
      icon: 'fa-solid fa-headset',
      children: [
          {
              path: 'cfcreation',
              name: 'TEST 5',
              component: Partielle,
              icon:'fa-solid fa-receipt',
              hidden: false
          },
          
      ]
    },
    {
      path: '/logistque',
      name: 'Logistque',
      icon: 'fa-solid fa-warehouse',
      children: [
          {
              path: 'cfcreation',
              name: 'TEST 6',
              component: Partielle,
              icon:'fa-solid fa-receipt',
              hidden: false
          },
          
      ]
    },
    {
      path: '/admin',
      name: 'Administration',
      icon: 'fa-solid  fa-user-shield',
      component: Administration,
      
    },

  ]
  const router = createRouter({
      history: createWebHistory(),
      routes
  })

registerGuard(router);
export default router;
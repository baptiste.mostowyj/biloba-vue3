import axios from "axios";

const axiosClient = axios.create({
    baseURL: "https://biloba-vue3.test/api/orders/partielles"
})
export async function getPartielles(do_type:number) {
    try{
        const { data } = await axiosClient.get(`/${do_type}`);
        return [null,data];
    }catch(error){
        return [error];
    }
}
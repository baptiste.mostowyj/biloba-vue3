import axios from "axios";

const axiosClient = axios.create({
    baseURL: "https://biloba-vue3.test/api/users"
})
export async function getUsers() {
    try{
        const { data } = await axiosClient.get('/');
        return [null,data];
    }catch(error){
        return [error];
    }
}
export async function putUserRoles(idUser:number,role:number,checked:any) {
    try{
        const { data } = await axiosClient.put(`/${idUser}`,{
            role: role,
            checked: checked,
        });
        
        return [null,data];
    }catch(error){
        return [error];
    }
}

export async function deleteUser(idUser:number) {
    try{
        const { data } = await axiosClient.delete(`/${idUser}`);
        return [null,data];
    }catch(error){
        return [error];
    }
}

export async function postUser(user:any) {
    try{
        console.log(user)
        const { data } = await axiosClient.post('/post',{user: user});
        console.log(data)
        return [null,data];
    }catch(error){
        return [error];
    }
}
import axios from "axios";

const axiosClient = axios.create({
    baseURL: "https://biloba-vue3.test/api/operations"
})
export async function getOperations() {
    try{
        const { data } = await axiosClient.get('/');
        return [null,data];
    }catch(error){
        return [error];
    }
}
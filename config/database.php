<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sqlsrv'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver'                  => 'sqlite',
            'url'                     => env('DATABASE_URL'),
            'database'                => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix'                  => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver'         => 'mysql',
            'url'            => env('DATABASE_URL'),
            'host'           => env('DB_HOST', '127.0.0.1'),
            'port'           => env('DB_PORT', '3306'),
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'unix_socket'    => env('DB_SOCKET', ''),
            'charset'        => 'utf8mb4',
            'collation'      => 'utf8mb4_unicode_ci',
            'prefix'         => '',
            'prefix_indexes' => true,
            'strict'         => true,
            'engine'         => null,
            'options'        => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'scraper' => [
            'driver'         => env('DB_SCRAPER_CONNECTION'),
            'url'            => env('DATABASE_URL'),
            'host'           => env('DB_SCRAPER_HOST', '127.0.0.1'),
            'port'           => env('DB_SCRAPER_PORT', '3306'),
            'database'       => env('DB_SCRAPER_DATABASE', 'scraper'),
            'username'       => env('DB_SCRAPER_USERNAME', 'root'),
            'password'       => env('DB_SCRAPER_PASSWORD', ''),
            'unix_socket'    => env('DB_SCRAPER_SOCKET', ''),
            'charset'        => 'utf8mb4',
            'collation'      => 'utf8mb4_unicode_ci',
            'prefix'         => '',
            'prefix_indexes' => true,
            'strict'         => true,
            'engine'         => null,
            'options'        => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql' => [
            'driver'         => 'pgsql',
            'url'            => env('DATABASE_URL'),
            'host'           => env('DB_PG_HOST', '127.0.0.1'),
            'port'           => env('DB_PG_PORT', '5432'),
            'database'       => env('DB_PG_DATABASE', 'forge'),
            'username'       => env('DB_PG_USERNAME', 'forge'),
            'password'       => env('DB_PG_PASSWORD', ''),
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
        ],

        'sqlsrv' => [
            'driver'                     => 'sqlsrv',
            'url'                        => env('DATABASE_URL'),
            'host'                       => env('DB_HOST', 'localhost'),
            'port'                       => env('DB_PORT', '1433'),
            'database'                   => env('DB_DATABASE', 'forge'),
            'username'                   => env('DB_USERNAME', 'forge'),
            'password'                   => env('DB_PASSWORD', ''),
            'charset'                    => 'utf8',
            'prefix'                     => '',
            'prefix_indexes'             => true,
            'base_maquette'              => '[sage].[WILLEMSE_MAQUETTE]',
            'base_catalog_erp'           => 'sampi.[liste_catalogue_erp]',
            'be'                         => '[BE]',
            'intranet'                   => '[intranet]',
            'article'                    => env('DB_ARTICLE', 'INTRANET.[dbo].[F_ARTICLE_DEV]'),
            'BP_PRODUITS'                => env('DB_BP_PRODUITS', 'INTRANET.[dbo].[BP_PRODUITS_DEV]'),
            'BP_PRODUITS_PICTOS'         => env('DB_BP_PRODUITS_PICTOS', 'INTRANET.[dbo].[BP_PRODUITS_PICTOS_DEV]'),
            'BP_PRODUITS_PERIODES'       => env('DB_BP_PRODUITS_PERIODES', '[INTRANET].[dbo].[BP_PRODUITS_PERIODES_DEV]'),
            'rollback'                   => env('DB_ROLLBACK', 'intranet.dbo.ProductRollback_DEV'),
            'DB_BP_INTERNATIONALISATION' => env('DB_BP_INTERNATIONALISATION', 'intranet.dbo.BP_INTERNATIONALISATION_DEV'),
            'DB_F_ARTFOURNISS'           => env('DB_F_ARTFOURNISS', 'intranet.dbo.F_ARTFOURNISS_DEV'),
            'DB_F_ARTCLIENT'             => env('DB_F_ARTCLIENT', 'intranet.dbo.F_ARTCLIENT_DEV'),
        ],
        'sampi' => [
            'driver'         => 'sqlsrv',
            'host'           => '192.168.10.44\APPS',
            'port'           => null,
            'database'       => 'INTRANET',
            'username'       => 'sa',
            'password'       => env('SAMPI_PASSWORD'),
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix'  => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url'      => env('REDIS_URL'),
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url'      => env('REDIS_URL'),
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];

<?php

namespace App\Classes;

class Saison
{
    /**
     * Date format : YYYY-MM-DD.
     */
    public string $start;

    /**
     * Date format : YYYY-MM-DD.
     */
    public string $end;

    /**
     * Ex : PE.
     */
    public string $saison;

    /**
     * Format : PE 2022.
     */
    public $current_saison;

    /**
     * Format : PE22.
     */
    public string $short_periode;

    public int $current_index = 0;
    public array $saisons = [];

    /**
     * Ex : P.
     */
    public string $short_saison;

    /**
     * Année ex : 2022.
     */
    public int $annee;

    /**
     * Paramètre Saison ex : PE 2022.
     * @param string $saison
     */
    public function __construct(string $saison)
    {
        $this->current_saison = $saison;
        $this->generateSaisonList();
    }

    private function parse()
    {
        $this->annee = (int) mb_substr($this->current_saison, 3, 7);
        $this->saison = mb_substr($this->current_saison, 0, 2);
        $this->short_saison = mb_substr($this->current_saison, 0, 1);
        $this->short_periode = $this->saison.mb_substr((string) $this->annee, 2, 4);
        if ($this->saison == 'AH') {
            $this->start = "$this->annee-07-01";
            $this->end = "$this->annee-31-12";
        } else {
            $this->start = "$this->annee-01-01";
            $this->end = "$this->annee-30-06";
        }
    }

    private function generateSaisonList()
    {
        $this->parse();
        for ($i = ($this->annee - 2); $i <= $this->annee; $i++) {
            $this->saisons[] = "PE $i";
            $this->saisons[] = "AH $i";
        }
        $this->saisons = array_reverse($this->saisons);
        while ($this->current_saison != $this->saisons[$this->current_index]) {
            $this->current_index++;
        }
        $this->current_saison = $this->saisons[$this->current_index];
        $this->parse();
    }

    public function prev()
    {
        $this->current_index++;
        $this->current_saison = $this->saisons[$this->current_index];
        $this->parse();
    }

    public function next()
    {
        $this->current_index--;
        $this->current_saison = $this->saisons[$this->current_index];
        $this->parse();
    }
}

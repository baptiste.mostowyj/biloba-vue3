<?php

namespace App\Classes;

use DB;

class Order
{
    public $commande_oxatis;
    public $date_commande;
    public $dispo_immediate;
    public $DEP;
    public $delaiDEPjours;
    public $date_expedition;
    public $delaiEXPjours;
    public $etat;

    public static function getOrdersFromDayAndDelaiDep(\DateTime $day, $delaiDep)
    {
        $conn = DB::connection()->getPdo();

        $timestampStart = $day->getTimestamp();
        $timestampStart = $timestampStart - ($timestampStart % 86400);
        $timestampEnd = $timestampStart + 86399;

        $sql = 'SELECT * from sampi.liste_catalogue_erp.dbo.GB_DEP_client where delaiDEP=? and date_commande between ? and ?;';
        $params = [$delaiDep, date('Y-m-d', $timestampStart), date('Y-m-d', $timestampEnd)];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $orders = [];
        foreach ($rows as $row) {
            $order = self::fromDbRow($row);
            $orders[] = $order;
        }

        return $orders;
    }

    public static function fromDbRow($row)
    {
        $order = new self();
        $order->commande_oxatis = intval($row['commande_oxatis']);
        $order->date_commande = \DateTime::createFromFormat('Y-m-d', $row['date_commande']);
        $order->dispo_immediate = boolval($row['dispo_immediate']);
        $order->DEP = \DateTime::createFromFormat('Y-m-d', $row['DEP']);
        $order->delaiDEPjours = intval($row['delaiDEP']);
        $order->date_expedition = \DateTime::createFromFormat('Y-m-d', $row['date_expedition']);
        $order->delaiEXPjours = intval($row['delai']);
        $order->etat = $row['etat'];

        return $order;
    }

    public static function getRoudedDateFromTimestamp($timestamp)
    {
        $ts = $timestamp - ($timestamp % 86400);
        $d = new \DateTime("@$ts");

        return $d;
    }
}

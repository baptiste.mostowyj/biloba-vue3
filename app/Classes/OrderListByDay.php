<?php

namespace App\Classes;

use DB;

class OrderListByDay
{
    public $timestampStart;
    public $timestampEnd;
    public $displaymode;
    public $ordersOfDayList = [];

    public function __construct($dateStart, $dateEnd, $displaymode)
    {
        $this->timestampStart = $this->convertDatetimeOrTimestampToDayInt($dateStart);
        $this->timestampEnd = $this->convertDatetimeOrTimestampToDayInt($dateEnd);
        $this->displaymode = $displaymode;
        $this->fetchOrders();
    }

    private function fetchOrders()
    {
        // fait la requete en raw PDO plutot qu'avec Eloquent car x10 plus rapide
        $conn = DB::connection()->getPdo();
        $sql = 'SELECT * from sampi.liste_catalogue_erp.dbo.CommandeEx where do_date between ? and ? order by do_date asc';
        if ($this->displaymode == 1) {
            $sql = "SELECT * from sampi.liste_catalogue_erp.dbo.CommandeEx where do_date between ? and ? and ca_num='web' order by do_date asc";
        }
        $params = [date('Y-m-d', $this->timestampStart), date('Y-m-d', $this->timestampEnd)];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($rows as $row) {
            $commandeEx = CommandeEx::fromDbRow($row);
            $this->addOrder($commandeEx);
        }
    }

    public function addOrder($commandeEx)
    {
        $roundedTimestampOfOrder = $this->convertDatetimeOrTimestampToDayInt($commandeEx->do_date);

        if (!array_key_exists($roundedTimestampOfOrder, $this->ordersOfDayList)) {
            $this->ordersOfDayList[$roundedTimestampOfOrder] = new OrdersOfDay();
        }

        $ordersOfDay = $this->ordersOfDayList[$roundedTimestampOfOrder];
        $ordersOfDay->addOrder($commandeEx, $this->displaymode);
    }

    private function convertDatetimeOrTimestampToDayInt($date)
    {
        $ts = $date->getTimestamp(); // $date doit etre une instance de \DateTime
        $ts = 86400 * intdiv($ts, 86400); // arrondi au jour j à 00h00

        return $ts;
    }

    public function getOrdersOfDayListReverse()
    {
        $ret = array_reverse($this->ordersOfDayList, true);

        return $ret;
    }

    public function getOrdersOfDay($date)
    {
        $roundedTimestampOfOrder = $this->convertDatetimeOrTimestampToDayInt($date);
        $ordersOfDay = $this->ordersOfDayList[$roundedTimestampOfOrder];

        return $ordersOfDay;
    }
}

<?php

namespace App\Classes;

use DB;
use XLSXWriter;

class WillemseData
{
    protected $sql;
    protected $filterable_fields;
    protected $orderable_fields;
    protected $orderway = 'ASC';
    private $builtSql;
    private $filter_pairs;
    private $order_by;
    protected $updateStoredProcedureName;

    public function __construct($sql, $filterable_fields = null, $orderable_fields = null)
    {
        $this->sql = $sql;
        $this->filterable_fields = $filterable_fields;
        $this->orderable_fields = $orderable_fields;
        $this->filter_pairs = null;
        $this->order_by = null;
        $this->updateStoredProcedure = null;
    }

    public function orderAsc()
    {
        $this->orderway = 'ASC';
    }

    public function orderDesc()
    {
        $this->orderway = 'DESC';
    }

    public function setUpdateStoredProcedure($procedureName)
    {
        $this->updateStoredProcedureName = $procedureName;
    }

    // pour utiliser la fonction, attention a bien passer les champs dans le parametre $filterable_fields du constructeur
    public function filterBy($fieldvalue_pairs)
    {
        if (count($fieldvalue_pairs) > 0) {
            $this->filter_pairs = [];
        }
        foreach ($fieldvalue_pairs as $k => $v) {
            $this->filter_pairs[$k] = $v;
        }
    }

    // pour utiliser la fonction, attention a bien passer le champ dans le parametre $orderable_fields du constructeur
    public function orderBy($fieldname)
    {
        if ($this->orderable_fields !== null && in_array($fieldname, $this->orderable_fields)) {
            $this->order_by = $fieldname;
        } else {
            $this->order_by = null;
        }
    }

    private function buildtSql()
    {
        $this->builtSql = $this->sql;

        if ($this->filter_pairs !== null && count($this->filter_pairs) > 0) {
            $this->builtSql .= ' WHERE';
            foreach ($this->filter_pairs as $k => $v) {
                $this->builtSql .= " $k='$v'"; // pour l'instant considere que $v est une string. TODO : autres cas
                break; // pour l'instant un seul where. TODO : accepter +
            }
        }

        if ($this->order_by !== null) {
            $this->builtSql .= ' ORDER BY '.$this->order_by.' '.$this->orderway;
        }
    }

    public function getRows($fetch_mode = \PDO::FETCH_ASSOC)
    {
        $this->buildtSql();
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($this->builtSql);
        $statement->execute();
        $rows = $statement->fetchAll($fetch_mode);

        return $rows;
    }

    public function CreateXls($filepath, $column_names_and_types, $sqlfields = null)
    {
        $rows = $this->getRows();

        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Feuil1', $column_names_and_types);
        foreach ($rows as $row) {
            if ($sqlfields === null) {
                $writer->writeSheetRow('Feuil1', $row);
            } else {
                $craftedRow = [];
                foreach ($sqlfields  as $field) {
                    $craftedRow[] = $row[$field];
                }
                $writer->writeSheetRow('Feuil1', $craftedRow);
            }
        }

        $writer->writeToFile($filepath);
    }

    public static function CreateXlsFromRows($filepath, $column_names_and_types, $rows)
    {
        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Feuil1', $column_names_and_types, ['font-style' => 'bold']);
        foreach ($rows as $row) {
            $writer->writeSheetRow('Feuil1', $row);
        }

        $writer->writeToFile($filepath);
    }

    public function updateWithStoredProcedure()
    {
        if ($this->updateStoredProcedureName != null) {
            $conn = DB::connection()->getPdo();
            $statement = $conn->prepare('EXEC '.$this->updateStoredProcedureName.';');
            $statement->execute();
        }
    }
}

<?php

namespace App\Classes\Checks;

use DB;

class CheckResult
{
    public $testcase;
    public $title;
    public $success;
    public $message;

    public function __construct(string $testcase, string $title, bool $success, string $message)
    {
        $this->testcase = $testcase;
        $this->title = $title;
        $this->success = $success;
        $this->message = $message;
    }

    public static function fromSuccess(string $testcase, string $title)
    {
        $checkResult = new self($testcase, $title, true, 'Aucun produit en erreur.');

        return $checkResult;
    }

    public function fail($errorMessage)
    {
        $this->success = false;
        $this->message = $errorMessage;
    }

    public function save()
    {
        $conn = DB::connection()->getPdo();
        $tablecheckresults = \config('database.connections.sqlsrv.intranet').'.checks.checkresults';

        $sql = "SELECT top(1) * FROM $tablecheckresults WHERE testcase = ? ORDER bY updated_at DESC;";
        $params = [$this->testcase];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        if (!isset($row) || $row == 0 || $row['success'] != $this->success || $row['message'] !== $this->message) {
            $sql = "INSERT INTO $tablecheckresults (testcase, title, success, message) VALUES (?, ?, ?, ?);";
            $params = [$this->testcase, $this->title, $this->success, $this->message];
            $statement = $conn->prepare($sql);
            $statement->execute($params);
        } else {
            $sql = "UPDATE $tablecheckresults SET testcase=testcase where id = ?;"; // ne fait rien, juste pour maj updated_at
            $params = [intval($row['id'])];
            $statement = $conn->prepare($sql);
            $statement->execute($params);
        }
    }

    public static function getLast(string $testcase)
    {
        $conn = DB::connection()->getPdo();
        $tablecheckresults = \config('database.connections.sqlsrv.intranet').'.checks.checkresults';

        $sql = "SELECT top(1) * FROM $tablecheckresults WHERE testcase = ? ORDER bY updated_at DESC;";
        $params = [$testcase];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        if ($row == false) {
            return null;
        }
        $checkResult = new self($testcase, $row['title'], $row['success'] === '1' ? true : false, $row['message']);

        return $checkResult;
    }
}

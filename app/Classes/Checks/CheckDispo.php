<?php

namespace App\Classes\Checks;

use DB;

class CheckDispo
{
    private static $tablename = '[intranet].[checks].dispo';
    public static $checks = [
    [
      'testcase'      => 'reel_avant60j_noorder',
      'method'        => 'Au réel',
      'livrabilite'   => 'avant60',
      'stock'         => '0',
      'cde'           => '0',
      'description'   => 'Produits avant les 60 jours qui précèdent le début de livrabilité, sans stock, sans commande en cours.',
      'where'         => "p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() < DATEADD(DAY, (DATEDIFF(DAY, 0, p.debpe_date) / 7) * 7 + 7, 0) AND p.refart not like 'T0%' and p.dispo<=0 and p.commandé<=0 and i.status<>0 AND p.ldf <> 1",
      'error_message' => '%d produits sont achetables alors qu\'ils ne devraient pas.',
    ],
    [
      'testcase'      => 'reel_avant60j_nostock',
      'method'        => 'Au réel',
      'livrabilite'   => 'avant60',
      'stock'         => '0',
      'cde'           => '>0',
      'description'   => 'Produits avant les 60 jours qui précèdent le début de livrabilité, sans stock, sans commande en cours.',
      'where'         => "p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() < DATEADD(day,-60, p.debpe_date) AND p.refart not like 'T0%' and p.dispo<=0 and p.commandé>0 and i.status<>0 AND p.ldf <> 1",
      'error_message' => '%d produits sont achetables alors qu\'ils ne devraient pas.',
    ],
    [
      'testcase'      => 'reel_dans60j_noorder',
      'method'        => 'Au réel',
      'livrabilite'   => 'avant',
      'stock'         => '0',
      'cde'           => '0',
      'description'   => 'Produits avant les 60 jours qui précèdent le début de livrabilité, sans stock, sans commande en cours.',
      'where'         => "p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() < DATEADD(day,-60, p.debpe_date) and GETDATE() < p.debpe_date AND p.refart not like 'T0%' and p.dispo<=0 and p.commandé<=0 and i.status<>0 AND p.ldf <> 1",
      'error_message' => '%d produits sont achetables alors qu\'ils ne devraient pas.',
    ],
    [
      'testcase'      => 'reel_avantdisponibilite_delaislogistique',
      'method'        => 'Au réel',
      'livrabilite'   => 'delaislogistique',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits avant livrabilité, dans le délais logistique.',
      'where'         => "p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= ex.deb_cur and GETDATE() < DATEADD(day,-3, ex.dispo_cur) AND p.refart not like 'T0%' AND p.ldf <> 1",
      'error_message' => '%d produits ne sont pas achetables alors qu\'ils le devraient.',
    ],
    [
      'testcase'      => 'reel_avantdisponibilite_3j',
      'method'        => 'Au réel',
      'livrabilite'   => '3j',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits 3j avant livrabilité.',
      'where'         => "p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= DATEADD(day,-3, ex.dispo_cur) AND GETDATE() < ex.dispo_cur AND p.refart not like 'T0%' AND p.ldf <> 1",
      'error_message' => '%d produits ne sont pas achetables alors qu\'ils le devraient.',
    ],
    [
      'testcase'      => 'reel_encourslivrabilite_sanscommande',
      'method'        => 'Au réel',
      'livrabilite'   => 'pendant',
      'stock'         => '0',
      'cde'           => '0',
      'description'   => 'Produits en cours de livrabilité, sans stock dispo, sans commande en cours.',
      'where'         => "a.METHODE_VENTE='AU_REEL' and p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' and p.dispo<=0 and p.commandé<=0 AND i.status<>0",
      'error_message' => '%d produits sont achetables alors qu\'ils ne devraient pas.',
    ],
    [
      'testcase'      => 'reel_encourslivrabilite_encommande',
      'method'        => 'Au réel',
      'livrabilite'   => 'pendant',
      'stock'         => '0',
      'cde'           => '>0',
      'description'   => 'Produits en cours de livrabilité, sans stock dispo, avec commande en cours qui sont en rupture alors qu\'ils devraient avoir une DEP.',
      'where'         => "a.METHODE_VENTE='AU_REEL' and p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND p.dispo<=0 and p.commandé>0 AND i.status=0",
      'error_message' => '%d produits ne sont pas achetables alors qu\'ils le devraient avec une date de DEP.',
    ],
    [
      'testcase'      => 'reel_encourslivrabilite_stock',
      'method'        => 'Au réel',
      'livrabilite'   => 'pendant',
      'stock'         => '>0',
      'cde'           => '-',
      'description'   => 'Produits en cours de livrabilité, avec du stock.',
      'where'         => "a.METHODE_VENTE='AU_REEL' and p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND p.dispo > 0 AND i.status<>1",
      'error_message' => '%d produits ne sont pas achetables alors qu\'ils devraient être DISPONIBLEs IMMEDIATEMENT.',
    ],
    [
      'testcase'      => 'surcommande',
      'method'        => 'Sur commande',
      'livrabilite'   => 'pendant',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits en cours de livrabilité.',
      'where'         => "a.METHODE_VENTE='SUR_COMMANDE' and p.statut='Commercialisé' and ex.dispo_fournisseur=1 and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND i.status=0",
      'error_message' => '%d produits ne sont pas en vente alors qu\'ils sont disponibles.',
    ],
    [
      'testcase'      => 'surcommande_indispo_four',
      'method'        => 'Sur commande',
      'livrabilite'   => 'pendant indispo fournisseur',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits en cours de livrabilité.',
      'where'         => "a.METHODE_VENTE='SUR_COMMANDE' and (ex.dispo_fournisseur = 0 or p.statut<>'Commercialisé') and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND p.dispo <=0 AND i.status<>0",
      'error_message' => '%d produits sont proposés à la vente alors qu\'ils ne sont pas pas commercialisés ou indisponibles chez le fournisseur.',
    ],
    [
      'testcase'      => 'surstock',
      'method'        => 'Sur stock',
      'livrabilite'   => 'pendant',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits en cours de livrabilité.',
      'where'         => "a.METHODE_VENTE='SUR_STOCK' and p.statut='Commercialisé' and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND i.status<>1",
      'error_message' => '%d produits "Commercialisés" ne sont pas disponibles immédiatement.',
    ],
    [
      'testcase'      => 'surstock_avecstock',
      'method'        => 'Sur stock',
      'livrabilite'   => 'pendant',
      'stock'         => '-',
      'cde'           => '-',
      'description'   => 'Produits en cours de livrabilité.',
      'where'         => "a.METHODE_VENTE='SUR_STOCK' and p.dispo>0 and p.categorie_web<>'Jardins' and GETDATE() >= ex.dispo_cur and GETDATE() <= ex.fin_cur AND p.refart not like 'T0%' AND i.status<>1",
      'error_message' => '%d produits avec du stock dispo ne sont pas disponibles immédiatement.',
    ],
  ];

    public static function getTestCaseRows($testcase)
    {
        (new self())->check();
        $conn = DB::connection()->getPdo();
        $sql = 'SELECT
          r.testcase as testcase,
          r.refart as refart,
          ex.[name] as[name],
          a.METHODE_VENTE as methodevente
        FROM '.self::$tablename.' r
        INNER JOIN [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=r.refart
        INNER JOIN SCRAPER.dbo.item i on i.ref=r.refart
        INNER JOIN liste_catalogue_erp.dbo.ArticleEx ex on ex.refart=r.refart
        WHERE testcase = ?;';

        $params = [$testcase];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public static function cleanTestCase($testcase)
    {
        $sql = 'DELETE FROM '.self::$tablename.' WHERE testcase=?';
        $conn = DB::connection()->getPdo();
        $params = [$testcase];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
    }

    public function check()
    {
        $checkResults = [];

        foreach (self::$checks as $check) {
            $this->checkOne($check);
        }

        return $checkResults;
    }

    private function checkOne($check)
    {
        $this->cleanTestCase($check['testcase']);

        $sql = 'INSERT INTO '.self::$tablename.'
              SELECT
              ? as testcase,
              a.ar_ref as refart,
              ex.name as nom,
              a.METHODE_VENTE as methodevente,
              0 as success,
              \'\' as message,
              GETDATE() as updated_at
            FROM sampi.liste_catalogue_erp.dbo.produits_stock_dep as p
            inner join liste_catalogue_erp.dbo.ArticleEx as ex ON ex.refart = p.refart
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=p.refart
            inner join SCRAPER.dbo.item i on i.ref=p.refart
            where '.$check['where'];
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $params = [$check['testcase']];
        $statement->execute($params);

        $count = $this->getCount($check['testcase']);
        $checkResult = CheckResult::fromSuccess($check['testcase'], $check['description']);
        if ($count > 0) {
            $checkResult->fail(sprintf($check['error_message'], $count));
        }
        $checkResult->save();
        $checkResults[$checkResult->testcase] = $checkResult;

        return $checkResult;
    }

    public function getCount($testcase)
    {
        $conn = DB::connection()->getPdo();
        $sql = 'SELECT count(*) FROM '.self::$tablename.' where testcase = ?';
        $params = [$testcase];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch();
        $row_count = $row[0];

        return $row_count;
    }

    public function getLastResults()
    {
        $checkResults = [];

        foreach (self::$checks as $c) {
            $testcase = $c['testcase'];
            $checkResult = CheckResult::getLast($testcase);
            if (!empty($checkResult)) {
                $checkResult->method = $c['method'];
                $checkResult->livrabilite = $c['livrabilite'];
                $checkResult->stock = $c['stock'];
                $checkResult->cde = $c['cde'];
                $checkResults[] = $checkResult;
            }
        }

        return $checkResults;
    }

    public static function getCheck($testcase)
    {
        foreach (self::$checks as $check) {
            if ($check['testcase'] == $testcase) {
                return $check;
            }
        }

        return null;
    }
}

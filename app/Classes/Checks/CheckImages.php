<?php

namespace App\Classes\Checks;

use App\Classes\Product\Item;
use DB;

class CheckImages
{
    public static function getItems()
    {
        $conn = DB::connection('scraper')->getPdo();

        $sql = 'SELECT * from item where coverImage is NULL and idMerchant=1 and active=1;';
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $items = [];
        foreach ($rows as $row) {
            $item = Item::fromDbRow($row);
            $items[] = $item;
        }

        return $items;
    }
}

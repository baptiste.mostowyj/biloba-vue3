<?php

namespace App\Classes\Prevision;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SimpleXLSX;
use voku\helper\ASCII;

class PrevisionXlsManager
{
    private $headerLine = 0;
    private $fullpath;
    private $xlsx;
    private $success = false;
    private $errors = [];
    private $hasErrors = false;
    private $messages = [];
    public $validation;
    public $column_labels;
    private $table = 'INTRANET.dbo.previsions';
    public int $changeCount = 0;
    public array $requiredRefCol = [
        'ref',
    ];
    public string $productKey;
    public array $base_column;
    public int $errorCount = 0;
    public array $requiredCol = [
        'selection catalogue', 'etat', 'engagement', 'prev_web_cat_ah', 'prev_web_cat_pe',
        'prev vp ah', 'prev vp pe',
        'pa ah', 'pa vp',
        'pv',
        'surcout',
        'dispo ah', 'dispo pe',
        'date de debut de livrabilite ah', 'date de fin de livrabilite ah',
        'date de debut de livrabilite pe', 'date de fin de livrabilite pe',
        'commentaires',
    ];

    public function __construct($fullpath = null)
    {
        $this->F_ARTICLE = config('database.connections.sqlsrv.article');
        $this->fullpath = $fullpath;
    }

    public function check(): bool
    {
        $this->success = true;
        if (filesize($this->fullpath) > 1024 * 1024) {
            $this->success = false;
            $this->errors[] = 'Erreur, le fichier XLS fait plus de 1 Mo. ';
        } else {
            if ($this->xlsx = SimpleXLSX::parse($this->fullpath)) {
                $ref = array_intersect($this->getHeaders(), $this->requiredRefCol);
//                $all = array_intersect($this->getHeaders(), $this->requiredCol);
                if (empty($ref)) {
                    $this->success = false;
                    $this->errors[] = 'Le fichier XLS doit avoir ces colonnes : ref';
                } else {
                    $ref = implode('', $ref);
                    $this->productKey = $ref;
                }
            } else {
                $this->success = false;
                $this->errors[] = SimpleXLSX::parseError();
            }
        }

        return $this->success;
    }

    public function getHeaders(): array
    {
        $headers = [];
        if ($this->xlsx) {
            $row1 = $this->xlsx->rows()[$this->headerLine];
            for ($i = 0; $i < count($row1); $i++) {
                $name = mb_strtolower(trim($row1[$i]));
                $name = ASCII::to_ascii(($name));
                $headers[] = $name;
            }
        }

        return $headers;
    }

    private function isYes($string)
    {
        $string = trim(mb_strtolower($string));

        return in_array($string, ['oui', 'o', 'yes', 'y', '1']);
    }

    private function processEtat($string)
    {
        $string = ASCII::to_ascii(($string)); //supprime acent
        $string = mb_strtolower($string);
        $string = trim($string);
        if ($string == 'brouillon') {
            return 0;
        }
        if ($string == 'a suivre' || $string == 'suivre') {
            return 1;
        }
        if ($string == 'valide') {
            return 2;
        }

        return null;
    }

    public function verifyExcel($saison): array
    {
        $refList = [];
        if ($this->check()) {
            $headers = $this->getHeaders();
            $productRefKey = array_search($this->productKey, $headers);
            $keys = [
                'ref'                       => '',
                'saison'                    => '',
                'annee'                     => '',
                'etat'                      => array_search('etat', $headers),
                'ah_prev_web_cat'           => array_search('prev_web_cat_ah', $headers),
                'pe_prev_web_cat'           => array_search('prev_web_cat_pe', $headers),
                'prev_vp_ah'                => array_search('prev vp ah', $headers),
                'prev_vp_pe'                => array_search('prev vp pe', $headers),
                'engagement'                => array_search('engagement', $headers),
                'ah_pa'                     => array_search('pa ah', $headers),
                'pe_pa'                     => array_search('pa pe', $headers),
                'pa_vp'                     => array_search('pa vp', $headers),
                'pv'                        => array_search('pv', $headers),
                'surcout'                   => array_search('surcout', $headers),
                'ah_disponibilite'          => array_search('dispo ah', $headers),
                'pe_disponibilite'          => array_search('dispo pe', $headers),
                'commentaire'               => array_search('commentaires', $headers),
                'ah_date_debut_livrabilite' => array_search('date de debut de livrabilite ah', $headers),
                'ah_date_fin_livrabilite'   => array_search('date de fin de livrabilite ah', $headers),
                'pe_date_debut_livrabilite' => array_search('date de debut de livrabilite pe', $headers),
                'pe_date_fin_livrabilite'   => array_search('date de fin de livrabilite pe', $headers),
                'selection_catalogue'       => array_search('selection catalogue', $headers),
            ];

            $prevList = $ahMap = $peMap = [];
            foreach ($this->xlsx->rows() as $i => $exelRow) {
                if ($i < 1) {
                    continue;
                }
                $ref = $exelRow[$productRefKey]; //article ref database
                $ref = ref_pad($ref);
                $refList[] = $ref;
                //AH
                $prevList[$ref]['ah'] = [
                    'ref'                    => $ref,
                    'etat'                   => ($this->processEtat($exelRow[$keys['etat']])),
                    'saison'                 => 'AH',
                    'annee'                  => $saison['annee'],
                    'prev_web_cat'           => round((float) $exelRow[$keys['ah_prev_web_cat']]),
                    'prev_vp'                => round((float) $exelRow[$keys['prev_vp_ah']]),
                    'engagement'             => (int) $exelRow[$keys['engagement']],
                    'pa'                     => (float) $exelRow[$keys['ah_pa']],
                    'pa_vp'                  => (float) (empty($exelRow[$keys['pa_vp']]) ? $exelRow[$keys['ah_pa']] : $exelRow[$keys['pa_vp']]),
                    'pv'                     => (float) $exelRow[$keys['pv']],
                    'surcout'                => (float) $exelRow[$keys['surcout']],
                    'commentaire'            => ($exelRow[$keys['commentaire']]),
                    'date_debut_livrabilite' => empty($exelRow[$keys['ah_date_debut_livrabilite']]) ? null : (new Carbon($exelRow[$keys['ah_date_debut_livrabilite']]))->format('d/m'),
                    'date_fin_livrabilite'   => empty($exelRow[$keys['ah_date_fin_livrabilite']]) ? null : (new Carbon($exelRow[$keys['ah_date_fin_livrabilite']]))->format('d/m'),
                    'disponibilite'          => ($this->isYes($exelRow[$keys['ah_disponibilite']])) ? 1 : 0,
                    'selection_catalogue'    => ($this->isYes($exelRow[$keys['selection_catalogue']])) ? 1 : 0,
                ];
                $ahMap = [
                    'ref'                    => 'ref',
                    'saison'                 => 'saison',
                    'annee'                  => 'annee',
                    'etat'                   => 'etat',
                    'prev_web_cat'           => 'ah_prev_web_cat',
                    'prev_vp'                => 'prev_vp_ah',
                    'engagement'             => 'engagement',
                    'pa'                     => 'ah_pa',
                    'pa_vp'                  => 'pa_vp',
                    'pv'                     => 'pv',
                    'surcout'                => 'surcout',
                    'disponibilite'          => 'ah_disponibilite',
                    'commentaire'            => 'commentaire',
                    'date_debut_livrabilite' => 'ah_date_debut_livrabilite',
                    'date_fin_livrabilite'   => 'ah_date_fin_livrabilite',
                    'selection_catalogue'    => 'selection_catalogue',
                ];
                //PE
                $pa = (float) (empty($exelRow[$keys['pe_pa']]) ? $exelRow[$keys['ah_pa']] : $exelRow[$keys['pe_pa']]);
                if (empty($exelRow[$keys['pa_vp']])) {
                    $pa_vp = $pa;
                } else {
                    $pa_vp = (float) $exelRow[$keys['pa_vp']];
                }
                $prevList[$ref]['pe'] = [
                    'ref'                    => $ref,
                    'etat'                   => ($this->processEtat($exelRow[$keys['etat']])),
                    'saison'                 => 'PE',
                    'annee'                  => $saison['annee'] + 1,
                    'prev_web_cat'           => round((float) $exelRow[$keys['pe_prev_web_cat']]),
                    'prev_vp'                => round((float) $exelRow[$keys['prev_vp_pe']]),
                    'engagement'             => (int) $exelRow[$keys['engagement']],
                    'pa'                     => $pa,
                    'pa_vp'                  => $pa_vp,
                    'pv'                     => (float) $exelRow[$keys['pv']],
                    'surcout'                => (float) $exelRow[$keys['surcout']],
                    'commentaire'            => ($exelRow[$keys['commentaire']]),
                    'date_debut_livrabilite' => empty($exelRow[$keys['pe_date_debut_livrabilite']]) ? null : (new Carbon($exelRow[$keys['pe_date_debut_livrabilite']]))->format('d/m'),
                    'date_fin_livrabilite'   => empty($exelRow[$keys['pe_date_fin_livrabilite']]) ? null : (new Carbon($exelRow[$keys['pe_date_fin_livrabilite']]))->format('d/m'),
                    'disponibilite'          => ($this->isYes($exelRow[$keys['pe_disponibilite']])) ? 1 : 0,
                    'selection_catalogue'    => ($this->isYes($exelRow[$keys['selection_catalogue']])) ? 1 : 0,
                ];
                $peMap = [
                    'ref'                    => 'ref',
                    'etat'                   => 'etat',
                    'saison'                 => 'saison',
                    'annee'                  => 'annee',
                    'prev_web_cat'           => 'pe_prev_web_cat',
                    'prev_vp'                => 'prev_vp_pe',
                    'engagement'             => 'engagement',
                    'pa'                     => 'pe_pa',
                    'pa_vp'                  => 'pa_vp',
                    'pv'                     => 'pv',
                    'surcout'                => 'surcout',
                    'disponibilite'          => 'pe_disponibilite',
                    'commentaire'            => 'commentaire',
                    'date_debut_livrabilite' => 'pe_date_debut_livrabilite',
                    'date_fin_livrabilite'   => 'pe_date_fin_livrabilite',
                    'selection_catalogue'    => 'selection_catalogue',
                ];
            }

            foreach ($prevList as $ref => $t) {
                $d = $t['ah'];
                //filtre les colonnes non présente dans l'excel
                $d = array_filter($d, function ($v, $k) use ($keys, $ahMap) {
                    return $keys[$ahMap[$k]] !== false;
                }, ARRAY_FILTER_USE_BOTH);
                DB::table('previsions')->upsert(
                    $d,
                    ['ref', 'annee', 'saison'],
                );

                $d = $t['pe'];
                //filtre les colonnes non présente dans l'excel
                $d = array_filter($d, function ($v, $k) use ($keys, $peMap) {
                    return $keys[$peMap[$k]] !== false;
                }, ARRAY_FILTER_USE_BOTH);
                DB::table('previsions')->upsert(
                    $d,
                    ['ref', 'annee', 'saison'],
                );
            }
        }

        return $refList;
    }

    public function isSuccess()
    {
        return $this->success;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->hasErrors;
    }
}

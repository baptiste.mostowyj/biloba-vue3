<?php

namespace App\Classes\Product;

// Mappe la table sage F_ARTICLE
class Article
{
    public $AR_Ref;

    // public $cbAR_Ref;
    // public $AR_Design;
    // public $cbAR_Design;
    // public $FA_CodeFamille;
    // public $cbFA_CodeFamille;
    // public $AR_Substitut;
    // public $cbAR_Substitut;
    // public $AR_Raccourci;
    // public $cbAR_Raccourci;
    // public $AR_Garantie;
    // public $AR_UnitePoids;
    // public $AR_PoidsNet;
    // public $AR_PoidsBrut;
    // public $AR_UniteVen;
    // public $AR_PrixAch;
    // public $AR_Coef;
    // public $AR_PrixVen;
    // public $AR_PrixTTC;
    // public $AR_Gamme1;
    // public $AR_Gamme2;
    // public $AR_SuiviStock;
    // public $AR_Nomencl;
    // public $AR_Stat01;
    // public $AR_Stat02;
    // public $AR_Stat03;
    // public $AR_Stat04;
    // public $AR_Stat05;
    // public $AR_Escompte;
    // public $AR_Delai;
    // public $AR_HorsStat;
    // public $AR_VteDebit;
    // public $AR_NotImp;
    // public $AR_Sommeil;
    // public $AR_Langue1;
    // public $AR_Langue2;
    // public $AR_EdiCode;
    // public $cbAR_EdiCode;
    // public $AR_CodeBarre;
    // public $cbAR_CodeBarre;
    // public $AR_CodeFiscal;
    // public $AR_Pays;
    // public $AR_Frais01FR_Denomination;
    // public $AR_Frais01FR_Rem01REM_Valeur;
    // public $AR_Frais01FR_Rem01REM_Type;
    // public $AR_Frais01FR_Rem02REM_Valeur;
    // public $AR_Frais01FR_Rem02REM_Type;
    // public $AR_Frais01FR_Rem03REM_Valeur;
    // public $AR_Frais01FR_Rem03REM_Type;
    // public $AR_Frais02FR_Denomination;
    // public $AR_Frais02FR_Rem01REM_Valeur;
    // public $AR_Frais02FR_Rem01REM_Type;
    // public $AR_Frais02FR_Rem02REM_Valeur;
    // public $AR_Frais02FR_Rem02REM_Type;
    // public $AR_Frais02FR_Rem03REM_Valeur;
    // public $AR_Frais02FR_Rem03REM_Type;
    // public $AR_Frais03FR_Denomination;
    // public $AR_Frais03FR_Rem01REM_Valeur;
    // public $AR_Frais03FR_Rem01REM_Type;
    // public $AR_Frais03FR_Rem02REM_Valeur;
    // public $AR_Frais03FR_Rem02REM_Type;
    // public $AR_Frais03FR_Rem03REM_Valeur;
    // public $AR_Frais03FR_Rem03REM_Type;
    // public $AR_Condition;
    // public $AR_PUNet;
    // public $AR_Contremarque;
    // public $AR_FactPoids;
    // public $AR_FactForfait;
    // public $AR_SaisieVar;
    // public $AR_Transfere;
    // public $AR_Publie;
    // public $AR_DateModif;
    // public $AR_Photo;
    // public $AR_PrixAchNouv;
    // public $AR_CoefNouv;
    // public $AR_PrixVenNouv;
    // public $AR_DateApplication;
    // public $AR_CoutStd;
    // public $AR_QteComp;
    // public $AR_QteOperatoire;
    // public $CO_No;
    // public $cbCO_No;
    // public $AR_Prevision;
    // public $CL_No1;
    // public $cbCL_No1;
    // public $CL_No2;
    // public $cbCL_No2;
    // public $CL_No3;
    // public $cbCL_No3;
    // public $CL_No4;
    // public $cbCL_No4;
    // public $AR_Type;
    // public $RP_CodeDefaut;
    // public $AR_Nature;
    // public $AR_DelaiFabrication;
    // public $AR_NbColis;
    // public $AR_DelaiPeremption;
    // public $AR_DelaiSecurite;
    // public $AR_Fictif;
    // public $AR_SousTraitance;
    // public $AR_TypeLancement;
    // public $AR_Cycle;
    // public $AR_Criticite;
    // public $cbProt;
    // public $cbMarq;
    // public $cbCreateur;
    // public $cbModification;
    // public $cbReplication;
    // public $cbFlag;
    // public $DEBUT_LIVRABILITE_PRINTEMPS;
    // public $FIN_LIVRABILITE_PRINTEMPS;
    // public $DEBUT_LIVRABILITE_AUTOMNE;
    // public $FIN_LIVRABILITE_AUTOMNE;
    // public $LIVRABLE_TOUTE_ANNEE;
    // public $MARQUE;
    // public $NB_PIECES_PAQUET;
    // public $HAUTEUR;
    // public $LARGEUR;
    // public $LONGUEUR;
    // public $HORS_NORME;
    // public $LDF;
    // public $BLOCAGE_CORSE;
    // public $BLOCAGE_BELGIQUE;
    // public $BLOCAGE_PAYS_BAS;
    // public $BLOCAGE_GRANDE_BRETAGNE;
    // public $BLOCAGE_ALLEMAGNE;
    // public $BLOCAGE_AUTRICHE;
    // public $BLOCAGE_PAYS_ETRANGERS;
    // public $EMPLACEMENT_PICKING;
    // public $CODE_STATUT;
    // public $INTITULE_FACTURE;
    // public $INTITULE_LATIN;
    // public $JYG;
    // public $VISIBLE;
    // public $POIDS_PLASTIQUE;
    // public $VENTE_SUGGESTIVE;
    // public $BLOCAGE_CONDITIONNEMENT;
    // public $CATEGORIE_WEB;
    // public $SOUS_CATEGORIE_WEB;
    // public $COEFF_PR;
    // public $cbCreation;
    // public $cbCreationUser;
    // public $STATUT_PRODUIT;
    // public $BLOCAGE_VENTE_WEB;
    // public $ECO_PARTICIPATION;
    // public $TYPE_CONDITIONNEMENT;
    // public $DIMENSION_CONDITIONNEMENT;
    // public $DEB_LIVRABILITE_P_THEORIQ;
    // public $FIN_LIVRABILITE_P_THEORIQ;
    // public $DEB_LIVRABILITE_A_THEORIQ;
    // public $FIN_LIVRABILITE_A_THEORIQ;
    // public $PCB;
    public $METHODE_VENTE;

    public static function fromDbRow($row)
    {
        $article = new self();
        $article->AR_Ref = $row['AR_Ref'];
        // cbAR_Ref	YES	varbinary
        // AR_Design	YES	varchar
        // cbAR_Design	YES	varbinary
        // FA_CodeFamille	NO	varchar
        // cbFA_CodeFamille	YES	varbinary
        // AR_Substitut	YES	varchar
        // cbAR_Substitut	YES	varbinary
        // AR_Raccourci	YES	varchar
        // cbAR_Raccourci	YES	varbinary
        // AR_Garantie	YES	smallint
        // AR_UnitePoids	YES	smallint
        // AR_PoidsNet	YES	numeric
        // AR_PoidsBrut	YES	numeric
        // AR_UniteVen	YES	smallint
        // AR_PrixAch	YES	numeric
        // AR_Coef	YES	numeric
        // AR_PrixVen	YES	numeric
        // AR_PrixTTC	YES	smallint
        // AR_Gamme1	YES	smallint
        // AR_Gamme2	YES	smallint
        // AR_SuiviStock	YES	smallint
        // AR_Nomencl	YES	smallint
        // AR_Stat01	YES	varchar
        // AR_Stat02	YES	varchar
        // AR_Stat03	YES	varchar
        // AR_Stat04	YES	varchar
        // AR_Stat05	YES	varchar
        // AR_Escompte	YES	smallint
        // AR_Delai	YES	smallint
        // AR_HorsStat	YES	smallint
        // AR_VteDebit	YES	smallint
        // AR_NotImp	YES	smallint
        // AR_Sommeil	YES	smallint
        // AR_Langue1	YES	varchar
        // AR_Langue2	YES	varchar
        // AR_EdiCode	YES	varchar
        // cbAR_EdiCode	YES	varbinary
        // AR_CodeBarre	YES	varchar
        // cbAR_CodeBarre	YES	varbinary
        // AR_CodeFiscal	YES	varchar
        // AR_Pays	YES	varchar
        // AR_Frais01FR_Denomination	YES	varchar
        // AR_Frais01FR_Rem01REM_Valeur	YES	numeric
        // AR_Frais01FR_Rem01REM_Type	YES	smallint
        // AR_Frais01FR_Rem02REM_Valeur	YES	numeric
        // AR_Frais01FR_Rem02REM_Type	YES	smallint
        // AR_Frais01FR_Rem03REM_Valeur	YES	numeric
        // AR_Frais01FR_Rem03REM_Type	YES	smallint
        // AR_Frais02FR_Denomination	YES	varchar
        // AR_Frais02FR_Rem01REM_Valeur	YES	numeric
        // AR_Frais02FR_Rem01REM_Type	YES	smallint
        // AR_Frais02FR_Rem02REM_Valeur	YES	numeric
        // AR_Frais02FR_Rem02REM_Type	YES	smallint
        // AR_Frais02FR_Rem03REM_Valeur	YES	numeric
        // AR_Frais02FR_Rem03REM_Type	YES	smallint
        // AR_Frais03FR_Denomination	YES	varchar
        // AR_Frais03FR_Rem01REM_Valeur	YES	numeric
        // AR_Frais03FR_Rem01REM_Type	YES	smallint
        // AR_Frais03FR_Rem02REM_Valeur	YES	numeric
        // AR_Frais03FR_Rem02REM_Type	YES	smallint
        // AR_Frais03FR_Rem03REM_Valeur	YES	numeric
        // AR_Frais03FR_Rem03REM_Type	YES	smallint
        // AR_Condition	YES	smallint
        // AR_PUNet	YES	numeric
        // AR_Contremarque	YES	smallint
        // AR_FactPoids	YES	smallint
        // AR_FactForfait	YES	smallint
        // AR_SaisieVar	YES	smallint
        // AR_Transfere	YES	smallint
        // AR_Publie	YES	smallint
        // AR_DateModif	YES	datetime
        // AR_Photo	YES	varchar
        // AR_PrixAchNouv	YES	numeric
        // AR_CoefNouv	YES	numeric
        // AR_PrixVenNouv	YES	numeric
        // AR_DateApplication	YES	datetime
        // AR_CoutStd	YES	numeric
        // AR_QteComp	YES	numeric
        // AR_QteOperatoire	YES	numeric
        // CO_No	YES	int
        // cbCO_No	YES	int
        // AR_Prevision	YES	smallint
        // CL_No1	YES	int
        // cbCL_No1	YES	int
        // CL_No2	YES	int
        // cbCL_No2	YES	int
        // CL_No3	YES	int
        // cbCL_No3	YES	int
        // CL_No4	YES	int
        // cbCL_No4	YES	int
        // AR_Type	YES	smallint
        // RP_CodeDefaut	YES	varchar
        // AR_Nature	YES	smallint
        // AR_DelaiFabrication	YES	smallint
        // AR_NbColis	YES	smallint
        // AR_DelaiPeremption	YES	smallint
        // AR_DelaiSecurite	YES	smallint
        // AR_Fictif	YES	smallint
        // AR_SousTraitance	YES	smallint
        // AR_TypeLancement	YES	smallint
        // AR_Cycle	YES	smallint
        // AR_Criticite	YES	smallint
        // cbProt	YES	smallint
        // cbMarq	NO	int
        // cbCreateur	YES	char
        // cbModification	YES	datetime
        // cbReplication	YES	int
        // cbFlag	YES	smallint
        // DEBUT_LIVRABILITE_PRINTEMPS	YES	varchar
        // FIN_LIVRABILITE_PRINTEMPS	YES	varchar
        // DEBUT_LIVRABILITE_AUTOMNE	YES	varchar
        // FIN_LIVRABILITE_AUTOMNE	YES	varchar
        // LIVRABLE_TOUTE_ANNEE	YES	varchar
        // MARQUE	YES	varchar
        // NB_PIECES_PAQUET	YES	varchar
        // HAUTEUR	YES	varchar
        // LARGEUR	YES	varchar
        // LONGUEUR	YES	varchar
        // HORS_NORME	YES	varchar
        // LDF	YES	varchar
        // BLOCAGE_CORSE	YES	varchar
        // BLOCAGE_BELGIQUE	YES	varchar
        // BLOCAGE_PAYS_BAS	YES	varchar
        // BLOCAGE_GRANDE_BRETAGNE	YES	varchar
        // BLOCAGE_ALLEMAGNE	YES	varchar
        // BLOCAGE_AUTRICHE	YES	varchar
        // BLOCAGE_PAYS_ETRANGERS	YES	varchar
        // EMPLACEMENT_PICKING	YES	varchar
        // CODE_STATUT	YES	varchar
        // INTITULE_FACTURE	YES	varchar
        // INTITULE_LATIN	YES	varchar
        // JYG	YES	varchar
        // VISIBLE	YES	numeric
        // POIDS_PLASTIQUE	YES	numeric
        // VENTE_SUGGESTIVE	YES	varchar
        // BLOCAGE_CONDITIONNEMENT	YES	varchar
        // CATEGORIE_WEB	YES	varchar
        // SOUS_CATEGORIE_WEB	YES	varchar
        // COEFF_PR	YES	numeric
        // cbCreation	YES	datetime
        // cbCreationUser	YES	uniqueidentifier
        // STATUT_PRODUIT	YES	varchar
        // BLOCAGE_VENTE_WEB	YES	varchar
        // ECO_PARTICIPATION	YES	numeric
        // TYPE_CONDITIONNEMENT	YES	varchar
        // DIMENSION_CONDITIONNEMENT	YES	varchar
        // DEB_LIVRABILITE_P_THEORIQ	YES	varchar
        // FIN_LIVRABILITE_P_THEORIQ	YES	varchar
        // DEB_LIVRABILITE_A_THEORIQ	YES	varchar
        // FIN_LIVRABILITE_A_THEORIQ	YES	varchar
        // PCB	YES	numeric
        $article->METHODE_VENTE = $row['METHODE_VENTE'];

        return $article;
    }

    public static function updateArticleTest($debpe, $finpe, $debah, $finah)
    {
        // test 1 : repousse les dates pe et
    }
}

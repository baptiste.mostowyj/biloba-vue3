<?php

namespace App\Classes\Product;

use App\Classes\Helper;
use App\Classes\WillemseData;
use App\Rules\Periode;
use App\Rules\TypeEnsoleillement;
use DB;

// La classe Product aggrege les classes Article, Item et Produit
class Product
{
    public $item; // resultats du scraping : data finale présentée sur le web
    public $produit; // table produits_stock_dep qui contient des données de stock, de disponibilité et de dep précalculés
    public $article; // sage F_article

    public static function selectAll($where = null)
    {
        $sql = 'SELECT top(100)
    a.*,
    p.refart as p_refart,
    p.name as p_name,
    p.statut as p_statut,
    p.nomencl as p_nomencl,
    p.fournisseur as p_fournisseur,
    p.delai_fournisseur as p_delai_fournisseur,
    p.methode_vente as p_methode_vente,
    p.ldf as p_ldf,
    p.en_sommeil as p_en_sommeil,
    p.debpe as p_debpe,
    p.finpe as p_finpe,
    p.debah as p_debah,
    p.finah as p_finah,
    p.[finah_date-1] as [p_finah_date-1],
    p.debpe_date as p_debpe_date,
    p.finpe_date as p_finpe_date,
    p.debah_date as p_debah_date,
    p.finah_date as p_finah_date,
    p.[debpe_date+1] as [p_debpe_date+1],
    p.date_achat as p_date_achat,
    p.date_achat2 as p_date_achat2,
    p.date_reception as p_date_reception,
    p.date_reception2 as p_date_reception2,
    p.date_dep as p_date_dep,
    p.date_dep_x_jours as p_date_dep_x_jours,
    p.x_jours as p_x_jours,
    p.commentaire as p_commentaire,
    p.stock as p_stock,
    p.[commandé] as [p_commandé],
    p.reserve as p_reserve,
    p.prepare as p_prepare,
    p.dispo as p_dispo,
    p.stock_terme as p_stock_terme,
    p.a_receptionner as p_a_receptionner,
    p.nom_fournisseur as p_nom_fournisseur,
    p.blocage_vente_web as p_blocage_vente_web,
    p.categorie_web as p_categorie_web,
    p.ShowStockLevel as p_ShowStockLevel,
    p.ShowIfOutOfStock as p_ShowIfOutOfStock,
    p.SaleIfOutOfStock as p_SaleIfOutOfStock,
    p.SaleIfOutOfStockScenario as p_SaleIfOutOfStockScenario,
    p.ShowDaysToship as p_ShowDaysToship,
    p.Visible as p_Visible,
    p.ShowInStockNote as p_ShowInStockNote,
    p.DaysToShip as p_DaysToShip,
    p.QtyInStock as p_QtyInStock,
    p.date_dep_PRINT as p_date_dep_PRINT,
    p.stock_SAGE as p_stock_SAGE,
    p.qte_derniere_reception as p_qte_derniere_reception,
    p.date_derniere_reception as p_date_derniere_reception,
    i.id as i_id,
    i.platformId as i_platformId,
    i.name as i_name,
    i.ref as i_ref,
    i.idMerchant as i_idMerchant,
    i.ean as i_ean,
    i.url as i_url,
    i.dep as i_dep,
    i.coverImage as i_coverImage,
    i.dropShipped as i_dropShipped,
    i.status as i_status,
    i.stock as i_stock,
    i.price as i_price,
    i.crossedOutPrice as i_crossedOutPrice,
    i.descriptionLength as i_descriptionLength,
    i.parcelContent as i_parcelContent,
    i.preorderAvailableDate as i_preorderAvailableDate,
    i.active as i_active,
    i.created_at as i_created_at,
    i.updated_at as i_updated_at
    FROM sampi.liste_catalogue_erp.dbo.produits_stock_dep as p
    inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=p.refart
    inner join SCRAPER.dbo.item i on i.ref=p.refart';

        if ($where != null) {
            $sql .= ' where '.$where;
        }
        $sql .= ';';

        $ret = [];
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($rows as $row) {
            $product = new self();
            $product->item = Item::fromDbRow($row, 'i_');
            $product->produit = Produit::fromDbRow($row, 'p_');
            $product->article = Article::fromDbRow($row);
            $ret[] = $product;
        }

        return $ret;
    }

    private static function getEnum($n_file = 11, $n_info = 'TYPE_CONDITIONNEMENT')
    {
        $table = config('database.connections.sqlsrv.base_maquette').'.[dbo].[F_ENUMLIBRECIAL] e';
        $sql = "select EL_Intitule from $table INNER JOIN [sage].[WILLEMSE_MAQUETTE].[dbo].cbSysLibre s ON e.N_Info = s.CB_Pos where n_file = $n_file and CB_Name = '$n_info'";
        $willemseData = new WillemseData($sql);
        $dbRows = $willemseData->getRows(\PDO::FETCH_COLUMN);

        return $dbRows;
    }

    public static function getTypeConditonnement(): array
    {
        return self::getEnum(11, 'TYPE_CONDITIONNEMENT');
    }

    public static function getMethodeVente(): array
    {
        return self::getEnum(11, 'METHODE_VENTE');
    }

    public static function getStatutProduit(): array
    {
        return self::getEnum(11, 'STATUT_PRODUIT');
    }

    public static function getCodeStatut(): array
    {
        return self::getEnum(11, 'CODE_STATUT');
    }

    public static function getCouleur()
    {
        $table = config('database.connections.sqlsrv.be').'.[OW_BP].[BP_COULEURS]';
        $sql = "select  LTRIM(RTRIM(libCouleur)) from $table";

        $willemseData = new WillemseData($sql);
        $dbRows = $willemseData->getRows(\PDO::FETCH_COLUMN);

        return $dbRows;
    }

    public static function getPicto($famille)
    {
        $table = config('database.connections.sqlsrv.be').'.[OW_BP].[BP_PICTOS_ID]';
        $sql = "select lib_picto, val_picto from $table where fam_picto = '$famille' order by val_picto ASC";
        $willemseData = new WillemseData($sql);
        $dbRows = $willemseData->getRows();

        $return = [];
        foreach ($dbRows as $picto) {
            $return[$picto['lib_picto']] = $picto['val_picto'];
        }

        return $return;
    }

    public static function getPorteGreffe()
    {
        $table = config('database.connections.sqlsrv.be').'.[OW_BP].[BP_PORTEGREFFE]';
        $sql = "select [libPorteGreffe] from $table";

        $willemseData = new WillemseData($sql);
        $dbRows = $willemseData->getRows(\PDO::FETCH_COLUMN);

        return $dbRows;
    }

    public static function getInternationalisationFromId($id, $colonne): string
    {
        $table = config('database.connections.sqlsrv.DB_BP_INTERNATIONALISATION');
        $result = select(
            " select TEXTE from $table
            where langue = 'FRA' and colonne = '$colonne' and tbl = 'BP_PRODUITS' and Id = '$id'",
            \PDO::FETCH_COLUMN
        );

        return empty($result) ? '' : $result[0];
    }

    public static function getInternationalisationFromText($texte, $colonne): string
    {
        $table = config('database.connections.sqlsrv.DB_BP_INTERNATIONALISATION');
        $conn = DB::connection('sqlsrv')->getPdo();
        $statement = $conn->prepare(" select Id from $table
            where langue = 'FRA' and colonne = '$colonne' and tbl = 'BP_PRODUITS' and TEXTE = :texte");
        $statement->bindParam(':texte', $texte);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_COLUMN);

        return empty($result) ? '' : $result[0];
    }

    public static function insertInternationalisation($texte, $colonne): string
    {
        $table = config('database.connections.sqlsrv.DB_BP_INTERNATIONALISATION');
        [$newId] = select("select max(IdNum) from $table", \PDO::FETCH_COLUMN, 'sqlsrv');
        $newId++;
        $conn = DB::connection('sqlsrv')->getPdo();
        $prefix = $colonne == 'qualite_client' ? 'PQ' : 'PT';
        $statement = $conn->prepare(" SET IDENTITY_INSERT $table ON;
        INSERT into $table (IdNum, Id, TEXTE, LANGUE, COLONNE, TBL) values($newId, '{$prefix}{$newId}', :texte, 'FRA', '$colonne', 'BP_PRODUITS');
        SET IDENTITY_INSERT $table OFF;");
        $statement->bindParam(':texte', $texte);
        $statement->execute();

        return "{$prefix}{$newId}";
    }

    public static function transformDate($value)
    {
        //format dd/mm/yyyy => dd/mm
        $pattern = '/^(0[1-9]|[1-2][0-9]|3[0-1])(?:-|\/)(0[1-9]|1[0-2])(?:-|\/)[0-9]{4}.*/';
        $replacement = '$1/$2';
        $value = preg_replace($pattern, $replacement, $value);
        //format yyyy-mm-dd => dd/mm
        $pattern = '/^[0-9]{4}(?:-|\/)(0[1-9]|1[0-2])(?:-|\/)(0[1-9]|[1-2][0-9]|3[0-1]).*/';
        $replacement = '$2/$1';

        return preg_replace($pattern, $replacement, $value);
    }

    public static function transformExcelAttributeBeforeValidation($colonne, $value)
    {
        $attributeUcFirst = ['idTypeSol', 'idClimat', 'idTypeUtilisation', 'coul'];
        $formatDates = ['DEBUT_LIVRABILITE_PRINTEMPS',
                        'FIN_LIVRABILITE_PRINTEMPS',
                        'DEBUT_LIVRABILITE_AUTOMNE',
                        'FIN_LIVRABILITE_AUTOMNE',
                        'DEB_LIVRABILITE_P_THEORIQ',
                        'FIN_LIVRABILITE_P_THEORIQ',
                        'DEB_LIVRABILITE_A_THEORIQ',
                        'FIN_LIVRABILITE_A_THEORIQ', ];
        $uppercase = [
            'BLOCAGE_ALLEMAGNE',
            'BLOCAGE_AUTRICHE',
            'BLOCAGE_BELGIQUE',
            'BLOCAGE_CONDITIONNEMENT',
            'BLOCAGE_CORSE',
            'BLOCAGE_GRANDE_BRETAGNE',
            'BLOCAGE_PAYS_BAS',
            'BLOCAGE_PAYS_ETRANGERS',
            'BLOCAGE_VENTE_WEB',
        ];
        $nl2br = ['TEXTE_TRES_LONG', 'TEXTE_COURT'];
        $transformAccent = ['STATUT_PRODUIT'];
        $boolean = ['AR_Publie', 'AR_Sommeil', 'LDF', 'FLGRAR',
                    'FLAGFEUILLAGEPERSIST', 'FLGNOV',
                    'flgplantebleue', 'premier_prix', 'flgcollection', ];

        if (in_array($colonne, $formatDates)) {
            $value = self::transformDate($value);
        }
        if (in_array($colonne, $uppercase)) {
            $value = mb_strtoupper($value);
        }
        if (in_array($colonne, $transformAccent)) {
            $pattern = ['/Arret/', '/Commercialise/', '/En creation/'];
            $replacement = ['Arrêt', 'Commercialisé', 'En création'];
            $value = preg_replace($pattern, $replacement, $value);
        }
        if (in_array($colonne, $nl2br)) {
            $value = nl2br($value);
        }
        if (in_array($colonne, $boolean)) {
            $pattern = ['/oui/i', '/non/i'];
            $replacement = [1, 0];
            $value = preg_replace($pattern, $replacement, $value);
        }
        if (in_array($colonne, $attributeUcFirst)) {
            $value = Helper::explode($value);
            $value = array_map(function ($value) {
                return ucfirst($value);
            }, $value);
            $value = implode(';', $value);
        }

        return $value;
    }

    public static function transformExcelAttributeToShow($colonne, $value)
    {
        $attributeToExplodeAndSort = ['idTypeSol', 'idClimat', 'idTypeUtilisation', 'Type_ensoleillement'];
        $formatDates = ['DEBUT_LIVRABILITE_PRINTEMPS',
                        'FIN_LIVRABILITE_PRINTEMPS',
                        'DEBUT_LIVRABILITE_AUTOMNE',
                        'FIN_LIVRABILITE_AUTOMNE',
                        'DEB_LIVRABILITE_P_THEORIQ',
                        'FIN_LIVRABILITE_P_THEORIQ',
                        'DEB_LIVRABILITE_A_THEORIQ',
                        'FIN_LIVRABILITE_A_THEORIQ', ];
        $periodes = ['PERIODE_SEMIS', 'PERIODE_PLANTATION', 'PERIODE_LIVRAISON', 'PERIODE_FLORAISON', 'PERIODE_RECOLTE'];
        if (in_array($colonne, $attributeToExplodeAndSort)) {
            $pictosInputs = Helper::explode($value);
            $value = implode(';', $pictosInputs);
        } elseif (in_array($colonne, $formatDates)) {
            $value = self::transformDate($value);
        } elseif ($colonne == 'CT_Num') {
            $value = str_pad($value, 3, '0', STR_PAD_LEFT);
        } elseif (in_array($colonne, $periodes)) {
            $value = Periode::simplifyExcel($value);
        }

        return $value;
    }

    /**
     * Transforme la donnée excel avant insert DB.
     */
    public static function transformExcelAttributeForDb($colonne, $value)
    {
        $attributeNullIfEmpty = ['ECO_PARTICIPATION'];
        $attributeStringToBinary = ['idTypeSol', 'idClimat', 'idTypeUtilisation'];
        $internationalText = ['QUALITE_CLIENT', 'TEXTE_TRES_LONG', 'TEXTE_COURT', 'TEXTE_METATITLE'];
        $periodes = ['PERIODE_SEMIS', 'PERIODE_PLANTATION', 'PERIODE_LIVRAISON', 'PERIODE_FLORAISON', 'PERIODE_RECOLTE'];
        if (in_array($colonne, $attributeNullIfEmpty) && empty($value)) {
            $value = null;
        } elseif (in_array($colonne, $attributeStringToBinary)) {
            $famille = ['idTypeSol' => 'sol', 'idClimat' => 'climat', 'idTypeUtilisation' => 'utilisation'];
            $picto = (self::getPicto($famille[$colonne]));
            $pictosInputs = Helper::explode($value);
            $value = 0;
            foreach ($pictosInputs as $pictosInput) {
                $value += $picto[$pictosInput];
            }
        } elseif (in_array($colonne, $internationalText)) {
            $id = (self::getInternationalisationFromText($value, $colonne));
            $value = empty($id) ? self::insertInternationalisation($value, $colonne) : $id;
        } elseif (in_array($colonne, $periodes)) {
            $value = Periode::excelToDb($value);
        }

        return $value;
    }

    public static function transformDbAttribute($colonne, $value)
    {
        $attributeBinaryToString = ['idTypeSol', 'idClimat', 'idTypeUtilisation'];
        $attributeIdPictoToString = ['IDPICTO'];
        $internationalText = ['QUALITE_CLIENT', 'TEXTE_TRES_LONG', 'TEXTE_COURT', 'TEXTE_METATITLE'];
        $famille = ['idTypeSol' => 'sol', 'idClimat' => 'climat', 'idTypeUtilisation' => 'utilisation'];
        if (in_array($colonne, $attributeBinaryToString)) {
            $pictos = (self::getPicto($famille[$colonne]));
            $libelle = [];
            foreach ($pictos as $lib => $val) {
                if ($value % ($val * 2) != 0) {
                    $libelle[] = $lib;
                    $value -= $val;
                }
            }
            sort($libelle);
            $value = implode(';', $libelle);
        } elseif (in_array($colonne, $attributeIdPictoToString)) {
            if ($value) {
                $value = implode(';', TypeEnsoleillement::idsToLabels(explode(',', $value)));
            }
        } elseif (in_array($colonne, $internationalText)) {
            $value = self::getInternationalisationFromId($value, $colonne);
        }

        return $value;
    }
}

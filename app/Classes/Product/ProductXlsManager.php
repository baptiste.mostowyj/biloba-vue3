<?php

namespace App\Classes\Product;

use App\Classes\Helper;
use App\Classes\WillemseData;
use App\Rules\In;
use App\Rules\Periode;
use App\Rules\TypeEnsoleillement;
use App\Rules\TypePicto;
use Cache;
use DB;
use Log;
use SimpleXLSX;
use Validator;
use voku\helper\ASCII;

class ProductXlsManager
{
    private $fullpath;
    private $xlsx;
    private $success;
    protected $errors;
    private $hasErrors;
    protected $messages;
    public $validation;
    public $column_labels;
    public int $changeCount = 0;
    protected int $updated_rows = 0;
    public array $requiredRefCol = [
        'ref', 'reference', 'ar_ref', 'arref', 'reference produit', 'referenceproduit', 'reference willemse', 'code article',
    ];
    public string $productKey;
    public array $base_column;
    private array $baseByCol;
    public int $errorCount = 0;
    private $col_not_to_update = ['CT_Num', 'ac_categorie']; //colonne non mise à jour

    public function __construct($fullpath = null)
    {
        $this->F_ARTICLE = config('database.connections.sqlsrv.article');
        $this->F_ARTFOURNISS = config('database.connections.sqlsrv.DB_F_ARTFOURNISS');
        $this->F_ARTCLIENT = config('database.connections.sqlsrv.DB_F_ARTCLIENT');
        $this->BP_PRODUITS = config('database.connections.sqlsrv.BP_PRODUITS');
        $this->BP_PRODUITS_PICTOS = config('database.connections.sqlsrv.BP_PRODUITS_PICTOS');
        $this->BP_PRODUITS_PERIODES = config('database.connections.sqlsrv.BP_PRODUITS_PERIODES');
        $this->rollback = config('database.connections.sqlsrv.rollback');
        $this->fullpath = $fullpath;
        $this->success = false;
        $this->hasErrors = false;
        $this->errors = [];
        $this->messages = [];

        $this->column_labels = [
            'AR_CodeBarre'    => 'Code Barre / EAN',
            'AR_coutstd'      => 'Coût standard / Prix barré',
            'AR_Design'       => 'Designation',
            'LDF'             => '',
            'PCB'             => '',
            'AR_PRIXACH'      => 'Prix d\'achat',
            'AF_RefFourniss'  => 'Ref fournisseur',
            'CT_Num'          => 'Code fournisseur',
            'AC_PRIXVEN'      => 'Prix de vente',
            'ac_categorie'    => 'Canal',
            'coul'            => 'Couleur',
            'dstplant'        => 'Distance de plantation',
            'FLGRAR'          => 'Flag rare',
            'FLGNOV'          => 'Flag nouveau',
            'flgplantebleue'  => 'Label plante bleue',
            'QUALITE_CLIENT'  => 'Qualité livrée',
            'TEXTE_TRES_LONG' => 'Designation web',
            'TEXTE_COURT'     => 'Descriptif marketplace',
            'PORTEGREFFE'     => 'Porte greffe',
        ];
        $this->base_column = [
            'SAGE' => [
                'AR_CodeBarre'                => 'numeric|max:9999999999999999999',
                'AR_coutstd'                  => 'numeric',
                'AR_Design'                   => 'filled|max:69',
                'AR_PoidsBrut'                => 'filled|numeric|min:1|max:1000000',
                'AR_Publie'                   => 'filled|boolean',
                'AR_Sommeil'                  => 'filled|boolean',
                'BLOCAGE_ALLEMAGNE'           => 'filled|in:OUI,NON',
                'BLOCAGE_AUTRICHE'            => 'filled|in:OUI,NON',
                'BLOCAGE_BELGIQUE'            => 'filled|in:OUI,NON',
                'BLOCAGE_CONDITIONNEMENT'     => 'filled|in:OUI,NON',
                'BLOCAGE_CORSE'               => 'filled|in:OUI,NON',
                'BLOCAGE_GRANDE_BRETAGNE'     => 'filled|in:OUI,NON',
                'BLOCAGE_PAYS_BAS'            => 'filled|in:OUI,NON',
                'BLOCAGE_PAYS_ETRANGERS'      => 'filled|in:OUI,NON',
                'BLOCAGE_VENTE_WEB'           => 'in:OUI,NON',
                'CATEGORIE_WEB'               => 'max:69',
                'DEBUT_LIVRABILITE_PRINTEMPS' => ['nullable', 'date_format:d/m', 'before:FIN_LIVRABILITE_PRINTEMPS'],
                'DEBUT_LIVRABILITE_AUTOMNE'   => ['nullable', 'date_format:d/m'],
                'DEB_LIVRABILITE_P_THEORIQ'   => 'nullable|date_format:d/m|before:FIN_LIVRABILITE_P_THEORIQ|before:DEB_LIVRABILITE_A_THEORIQ|before:FIN_LIVRABILITE_A_THEORIQ',
                'DEB_LIVRABILITE_A_THEORIQ'   => 'nullable|date_format:d/m|before:FIN_LIVRABILITE_A_THEORIQ',
                'DIMENSION_CONDITIONNEMENT'   => 'max:69',
                'ECO_PARTICIPATION'           => 'numeric',
                'FIN_LIVRABILITE_PRINTEMPS'   => ['nullable', 'date_format:d/m', 'after:DEBUT_LIVRABILITE_PRINTEMPS'],
                'FIN_LIVRABILITE_AUTOMNE'     => ['nullable', 'date_format:d/m'],
                'FIN_LIVRABILITE_P_THEORIQ'   => 'nullable|date_format:d/m|before:DEB_LIVRABILITE_A_THEORIQ|before:FIN_LIVRABILITE_A_THEORIQ',
                'FIN_LIVRABILITE_A_THEORIQ'   => 'nullable|date_format:d/m',
                'HAUTEUR'                     => 'integer|min:1|max:500',
                'INTITULE_FACTURE'            => 'filled|max:69',
                'INTITULE_LATIN'              => 'max:69',
                'LARGEUR'                     => 'integer|min:1|max:500',
                'LDF'                         => 'filled|boolean',
                'LONGUEUR'                    => 'integer|min:1|max:500',
                'MARQUE'                      => 'max:69',
                'METHODE_VENTE'               => ['filled', new In(Product::getMethodeVente())],
                'NB_PIECES_PAQUET'            => 'filled|integer|min:1|max:1000',
                'PCB'                         => 'filled|numeric|min:1|max:500',
                'SOUS_CATEGORIE_WEB'          => 'max:69',
                'STATUT_PRODUIT'              => ['filled', new In(Product::getStatutProduit())],
                'CODE_STATUT'                 => ['filled', new In(Product::getCodeStatut())],
                'TYPE_CONDITIONNEMENT'        => ['nullable', new In(Product::getTypeConditonnement())],
                'AR_PRIXACH'                  => 'numeric|min:0.01|max:10000',
                'AF_RefFourniss'              => 'max:19',
                'CT_Num'                      => 'numeric|required_with:AR_PRIXACH,AF_RefFourniss', //code fournisseur,
                'AC_PRIXVEN'                  => 'numeric|min:0.01|max:10000|required_with:ac_categorie',
                'ac_categorie'                => 'numeric|in:1,2,6|required_with:AC_PRIXVEN', //canal,
                'EMPLACEMENT_PICKING'         => 'filled|max:69',
            ],
            'BE' => [
                //BP_PRODUITS
                'coul'                 => [new In(Product::getCouleur())],
                'hautpro'              => 'max:50',
                'Haut_AdulteMin'       => 'nullable|integer|lte:Haut_AdulteMax',
                'Haut_AdulteMax'       => 'nullable|integer|gte:Haut_AdulteMin',
                'envergure'            => 'max:50',
                'Arrosage'             => 'in:faible,modéré,moyen,régulier,fréquent',
                'dstplant'             => 'max:25',
                'idClimat'             => new TypePicto('climat'),
                'idTypeSol'            => new TypePicto('sol'),
                'idTypeUtilisation'    => new TypePicto('utilisation'),
                'FLGRAR'               => 'filled|boolean',
                'FLAGFEUILLAGEPERSIST' => 'filled|boolean',
                'FLGNOV'               => 'filled|boolean',
                'flgplantebleue'       => 'filled|boolean',
                'FLGPARFUM'            => 'filled|boolean',
                'Prod_Entretien'       => 'in:facile,moyen,modéré,régulier,très facile',
                'aufildutemps'         => 'max:200',
                'conseil_entretien'    => 'max:200',
                'premier_prix'         => 'filled|boolean',
                'flgcollection'        => 'filled|boolean',
                'QUALITE_CLIENT'       => 'string',
                'TEXTE_TRES_LONG'      => 'string', //designation web
                'TEXTE_COURT'          => 'string', //descriptif marketplace
                'TEXTE_METATITLE'      => 'string',
                'score'                => 'integer|min:0|max:10000',
                'PORTEGREFFE'          => [new In(Product::getPorteGreffe())],
                //BP_PRODUITS_PERIODES
                'PERIODE_SEMIS'      => new Periode(),
                'PERIODE_PLANTATION' => new Periode(),
                'PERIODE_LIVRAISON'  => new Periode(),
                'PERIODE_FLORAISON'  => new Periode(),
                'PERIODE_RECOLTE'    => new Periode(),
                //BP_PRODUITS_PICTOS
                'Type_ensoleillement' => new TypeEnsoleillement(),
            ],
        ];
        ksort($this->base_column['SAGE'], SORT_STRING | SORT_FLAG_CASE);
        ksort($this->base_column['BE'], SORT_STRING | SORT_FLAG_CASE);
        $this->baseByCol = [];
        foreach ($this->base_column as $base => $col_array) {
            foreach ($col_array as $col => $v) {
                $this->baseByCol[$col] = $base;
            }
        }

        $this->validation = array_merge($this->base_column['SAGE'], $this->base_column['BE']);
        $this->base_column['SAGE'] = array_keys($this->base_column['SAGE']);
        $this->base_column['BE'] = array_keys($this->base_column['BE']);
        $this->BP_PRODUITS_fields = [
            'hautpro', 'Haut_AdulteMin', 'Haut_AdulteMax', 'envergure', 'Arrosage', 'dstplant', 'idClimat',
            'idTypeSol', 'FLGRAR', 'FLAGFEUILLAGEPERSIST', 'FLGNOV', 'flgplantebleue', 'FLGPARFUM', 'Prod_Entretien',
            'idTypeUtilisation', 'aufildutemps', 'conseil_entretien', 'premier_prix', 'flgcollection',
            'TYPE_CONDITIONNEMENT', 'coul', 'QUALITE_CLIENT', 'TEXTE_TRES_LONG', 'TEXTE_COURT', 'TEXTE_METATITLE', 'score', 'PORTEGREFFE',
        ];
        $this->f_article_fields = ['TYPE_CONDITIONNEMENT']; //colonne en double dans F_ARTICLE et BP_PRODUITS
        $this->BP_PRODUITS_PICTOS_fields = ['Type_ensoleillement'];
        $this->F_ARTFOURNISS_fields = ['CT_Num', 'AF_RefFourniss'];
        $this->F_ARTCLIENT_fields = ['AC_PRIXVEN'];
        $this->F_ARTCLIENT_ignore = ['ac_categorie'];
        $this->BP_PRODUITS_PERIODES_fields = ['PERIODE_SEMIS', 'PERIODE_PLANTATION', 'PERIODE_LIVRAISON', 'PERIODE_FLORAISON', 'PERIODE_RECOLTE'];
    }

    private function specificDateRule($validator = null)
    {
        $coupleDate = [
            'DEBUT_LIVRABILITE_PRINTEMPS' => 'FIN_LIVRABILITE_PRINTEMPS',
            'DEBUT_LIVRABILITE_AUTOMNE'   => 'FIN_LIVRABILITE_AUTOMNE',
            'DEB_LIVRABILITE_P_THEORIQ'   => 'FIN_LIVRABILITE_P_THEORIQ',
            'DEB_LIVRABILITE_A_THEORIQ'   => 'FIN_LIVRABILITE_A_THEORIQ',
        ];
        foreach ($coupleDate as $k => $v) {
            //couple date requis si l'une des 2 dates est non null
            $validator->sometimes($k, 'required', function ($input) use ($v) {
                return !empty($input->{$v});
            });
            $validator->sometimes($v, 'required', function ($input) use ($k) {
                return !empty($input->{$k});
            });
        }

        $sometimes = [
            ['DEBUT_LIVRABILITE_AUTOMNE', 'before', 'FIN_LIVRABILITE_AUTOMNE'],
            //  ['DEBUT_LIVRABILITE_AUTOMNE', 'after', 'DEBUT_LIVRABILITE_PRINTEMPS'],
            //  ['DEBUT_LIVRABILITE_AUTOMNE', 'after', 'FIN_LIVRABILITE_PRINTEMPS'],
            ['FIN_LIVRABILITE_PRINTEMPS', 'after', 'DEBUT_LIVRABILITE_PRINTEMPS'],
            //  ['FIN_LIVRABILITE_AUTOMNE', 'after', 'DEBUT_LIVRABILITE_AUTOMNE'],
            //  ['DEB_LIVRABILITE_A_THEORIQ', 'after', 'DEB_LIVRABILITE_P_THEORIQ'],
            //  ['DEB_LIVRABILITE_A_THEORIQ', 'after', 'FIN_LIVRABILITE_P_THEORIQ'],
            ['FIN_LIVRABILITE_P_THEORIQ', 'after', 'DEB_LIVRABILITE_P_THEORIQ'],
            ['FIN_LIVRABILITE_A_THEORIQ', 'after', 'DEB_LIVRABILITE_A_THEORIQ'],
            //            ['FIN_LIVRABILITE_A_THEORIQ', 'after', 'FIN_LIVRABILITE_P_THEORIQ'],
        ];
        foreach ($sometimes as $s) {
            //on compare les dates before/after si la date à comparer n'est pas null, par exemple :
            //DEBUT_LIVRABILITE_AUTOMNE before.FIN_LIVRABILITE_AUTOMNE si FIN_LIVRABILITE_AUTOMNE != null
            $validator->sometimes($s[0], "$s[1]:$s[2]", function ($input) use ($s) {
                return !empty($input->{$s[2]});
            });
        }

        return $validator;
    }

    /**
     * Ajoute les colonnes manquante dans le fichier excel pour la comparaison des dates.
     * Seulement si les colonnes dates sont présente, pour raison de performance.
     */
    private function addMissingComparaisonField($excelRowsByRef, $dbRows, $columnsStr): array
    {
        $fields = [];
        if (preg_match('/PRINTEMPS/', $columnsStr)) {
            $fields = array_merge(
                $fields,
                [
                    'DEBUT_LIVRABILITE_PRINTEMPS',
                    'FIN_LIVRABILITE_PRINTEMPS',
                ]
            );
        } elseif (preg_match('/AUTOMNE/', $columnsStr)) {
            $fields = array_merge(
                $fields,
                [
                    'DEBUT_LIVRABILITE_AUTOMNE',
                    'FIN_LIVRABILITE_AUTOMNE',
                ]
            );
        } elseif (preg_match('/P_THEORIQ/', $columnsStr)) {
            $fields = array_merge(
                $fields,
                [
                    'DEB_LIVRABILITE_P_THEORIQ',
                    'FIN_LIVRABILITE_P_THEORIQ',
                ]
            );
        } elseif (preg_match('/A_THEORIQ/', $columnsStr)) {
            $fields = array_merge(
                $fields,
                [
                    'DEB_LIVRABILITE_A_THEORIQ',
                    'FIN_LIVRABILITE_A_THEORIQ',
                ]
            );
        }
        if (preg_match('/Haut_Adulte/', $columnsStr)) {
            $fields = array_merge(
                $fields,
                [
                    'Haut_AdulteMin',
                    'Haut_AdulteMax',
                ]
            );
        }
        foreach ($excelRowsByRef as $ref => $excelRowByRef) {
            foreach ($fields as $field) {
                if (!isset($excelRowsByRef[$ref][$field]) && isset($dbRows[$ref])) {
                    $excelRowsByRef[$ref][$field] = trim($dbRows[$ref][$field]);
                }
            }
        }

        //ajouter regle longue à executer ici, exemple :
//        if (isset($excelRowsByRef[$ref]['QUALITE_CLIENT'])) {
//            $this->validation['QUALITE_CLIENT'] = [new In(Product::getQualiteClient())];
//        }

        return $excelRowsByRef;
    }

    public function check(): bool
    {
        $this->success = true;
        $this->errors = [];
        $this->messages = [];
        if (filesize($this->fullpath) > 1024 * 1024) {
            $this->success = false;
            $this->errors[] = 'Erreur, le fichier XLS fait plus de 1 Mo. ';
        } else {
            if ($this->xlsx = SimpleXLSX::parse($this->fullpath)) {
                $ref = array_intersect($this->getHeaders(), $this->requiredRefCol);
                if (empty($ref)) {
                    $this->success = false;
                    $this->errors[] = 'Le fichier XLS doit avoir une de ces colonnes : '.implode(', ', $this->requiredRefCol);
                } else {
                    $ref = implode('', $ref);
                    $this->productKey = $ref;
                }
            } else {
                $this->success = false;
                $this->errors[] = SimpleXLSX::parseError();
            }
        }

        return $this->success;
    }

    public function getLineCount(): int
    {
        return count($this->xlsx->rows()) - 1;
    }

    /**
     * Select les produits excel dans la base de donnée.
     */
    private function selectDbProduct(array $listRefProduit, array $listRef, array $excelRowsByRef, array $excelDbLink): array
    {
        //load produits from from db
        $defaultCategorie = empty($excelRowsByRef[array_key_first($excelRowsByRef)]['ac_categorie']) ? 2 : $excelRowsByRef[array_key_first($excelRowsByRef)]['ac_categorie'];
        $sql_columns = array_values($excelDbLink);
        if (!in_array('AR_Design', $sql_columns)) {
            $sql_columns[] = 'AR_Design';
        }
        $table_field = function ($col) {
            if (in_array($col, $this->BP_PRODUITS_fields)) {
                return 'b.'.$col;
            } elseif (in_array($col, $this->BP_PRODUITS_PICTOS_fields)) {
                return null;
            } elseif (in_array($col, $this->F_ARTFOURNISS_fields)) {
                return null;
            } elseif (in_array($col, $this->F_ARTCLIENT_fields)) {
                return 'client.'.$col;
            } elseif (in_array($col, $this->F_ARTCLIENT_ignore) or in_array($col, $this->BP_PRODUITS_PERIODES_fields)) {
                return null;
            }

            return 'a.'.$col;
        };
        $fields = array_map($table_field, array_keys($this->validation));
        $fields = array_filter($fields, function ($e) {
            return !is_null($e);
        });
        $editableDbColumns = implode(',', $fields);

        $sql = "select a.AR_Ref, $editableDbColumns , af.ct_num, af.AF_RefFourniss
                from {$this->F_ARTICLE} a          
                join {$this->BP_PRODUITS} b on a.AR_Ref = b.REFHOL 
                join {$this->F_ARTCLIENT} client on a.AR_Ref = client.AR_Ref and client.ac_categorie=$defaultCategorie    
                join {$this->F_ARTFOURNISS} af on af.ar_ref=a.ar_ref and af.AF_Principal=1
                where  a.AR_Ref in (".implode(',', $listRefProduit).') ;';
        $this->willemseData = new WillemseData($sql);

        $dbRows = $this->willemseData->getRows();

        $countExcel = count($listRef);
        $countDb = count($dbRows);
        if ($countDb != $countExcel) {
            $refs = collect($dbRows)->groupBy('AR_Ref')->keys()->all();
            $diff = array_diff($listRef, $refs);
            if (blank($diff)) {
                $doublons = array_unique(array_diff_assoc($listRef, array_unique($listRef)));
                if (!empty($doublons)) {
                    $this->errors[] = "Attention, ces produits sont en double dans l'excel : ".implode(', ', $doublons);
                }
            } else {
                $this->errors[] = "Attention, $countExcel lignes excel et $countDb lignes trouvées en BDD ! Produits absent en BDD : ".implode(', ', $diff);
            }
        }
        //build tableau db by ref
        $dbRowsByRef = [];
        foreach ($dbRows as $k => $dbRow) {
            $dbRowsByRef[$dbRow['AR_Ref']] = $dbRow;
            if (in_array('Type_ensoleillement', $sql_columns)) {
                $pictoExisting = TypeEnsoleillement::existingPictos($dbRow['AR_Ref']);
                $pictoExisting = TypeEnsoleillement::idsToLabels($pictoExisting);
                $dbRowsByRef[$dbRow['AR_Ref']]['Type_ensoleillement'] = implode(';', $pictoExisting);
            }
            foreach (Periode::$type as $col => $type) {
                if (in_array($col, $sql_columns)) {
                    $dbRowsByRef[$dbRow['AR_Ref']][$col] = Periode::existingPeriodes($dbRow['AR_Ref'], $type);
                }
            }
        }

        return $dbRowsByRef;
    }

    /**
     * Retourne le tableau représentant le fichier excel pour la vue html
     * Avec pour chaque ligne le avant/après ou erreur de validation.
     */
    public function verifyExcel(array $excelDbLink): array
    {
        $columns = array_keys($excelDbLink);
        $columnsStr = implode(array_values($excelDbLink));
        $productRefKey = array_search($this->productKey, $this->getHeaders());
        $columnsKey = [];
        foreach ($columns as $column) {
            $k = array_search($column, $this->getHeaders());
            $columnsKey[$k] = $column;
        }
        $listRefProduit = $listRef = $excelRowsByRef = []; //boucle ligne excel pour faire la liste des produits
        foreach ($this->xlsx->rows() as $i => $exelRow) {
            $i++;
            $ref = ($exelRow[$productRefKey]); //article ref database
            if ($i == 1 || blank($ref)) {
                continue;
            }
            $ref = ref_pad($ref);
            $listRefProduit[] = "'".$ref."'";
            $listRef[] = $ref;

            //créé le tableau qui sera retourné pour la vue
            foreach ($columnsKey as $k => $excelColumnName) {
                $dbColumn = ($excelDbLink[$excelColumnName]);
                $formatExcelValue = Product::transformExcelAttributeBeforeValidation($dbColumn, $exelRow[$k]);
                $excelRowsByRef[$ref][$dbColumn] = $formatExcelValue;
            }
        }
        $sql_columns = array_values($excelDbLink);
        $dbRowsByRef = $this->selectDbProduct($listRefProduit, $listRef, $excelRowsByRef, $excelDbLink);

        //boucle depuis excel :
        // - compare db afin de voir les lignes manquantes
        // - validation des données
        $validateRowByRef = $excelRowsByRef;
        $validateRowByRef = $this->addMissingComparaisonField($excelRowsByRef, $dbRowsByRef, $columnsStr);
        foreach ($excelRowsByRef as $ref => $excelRowByRef) {
            if (!isset($dbRowsByRef[$ref])) { //dont exist in db
                $this->hasErrors = true;
                $excelRowsByRef[$ref] = null;
            } else { //validation de chaque ligne
                $validator = Validator::make($validateRowByRef[$ref], $this->validation);
                $validator = $this->specificDateRule($validator);
                if (isset($validator) && $validator->fails()) {
                    $errors = $validator->errors();
                    $this->hasErrors = true;
                    //affiche les erreur DB non présente dans le excel
                    foreach (array_keys($errors->getMessages()) as $error) {
                        if (!in_array($error, $sql_columns)) {
                            if (empty($this->errors)) {
                                $this->errors[] = 'Erreur detectée dans la base : ';
                            }
                            foreach ($errors->get($error) as $err) {
                                $this->errors[] = "[{$this->baseByCol[$error]}] ".$ref.' : '.$err;
                                $this->errorCount++;
                            }
                        }
                    }
                }
                foreach ($excelRowByRef as $col => $val) { //pour chaque colonne
                    if (isset($errors) && $errors->has($col)) { //on ajoute le message d'erreur validation
                        $excelRowsByRef[$ref][$col] = ['errors' => $errors->get($col), 'value' => $val];
                        $this->errorCount++;
                    } elseif (in_array($col, $this->col_not_to_update)) {
                        $excelRowsByRef[$ref][$col] = Product::transformExcelAttributeToShow($col, $val);
                    } else { // et la diff avant / apres
                        $after = trim($val);
                        $after = Product::transformExcelAttributeToShow($col, $after);
                        $before = $dbRowsByRef[$ref][$col] ?? null;
                        $before = Product::transformDbAttribute($col, $before);
                        $before = trim($before);
                        if ($before != $after) {
                            $this->changeCount++;
                            $excelRowsByRef[$ref][$col] = [$before, $after];
                        } else {
                            $excelRowsByRef[$ref][$col] = $after;
                        }
                    }
                    if (!isset($excelRowsByRef[$ref]['AR_Design'])) { //ajoute colonne designation si absente
                        $excelRowsByRef[$ref]['AR_Design'] = $dbRowsByRef[$ref]['AR_Design'];
                    }
                }
            }
            $errors = null;
        }

        return $excelRowsByRef;
    }

    public function importExcelToDb(array $excelDbLink): void
    {
        $dateImport = now()->format(date_willemse);
        $userId = auth()->id();
        if (Cache::has($this->fullpath)) {
            $excelRowsByRef = Cache::get($this->fullpath);
        } else {
            $excelRowsByRef = $this->verifyExcel($excelDbLink);
        }
        if ($this->hasErrors) {
            return;
        }
        $dbColumns = array_values($excelDbLink);
        $sqlArray = [];
        $rollbackSql = '';

        $conn = DB::connection()->getPdo();

        foreach ($excelRowsByRef as $ref => $excelRowByRef) { //construit sql update + sauvegarde rollback
            $update_sql = [];
            foreach ($dbColumns as $col) {
                if (is_array($excelRowByRef[$col])) {
                    $before = Product::transformExcelAttributeForDb($col, $excelRowByRef[$col][0]);
                    $newVal = Product::transformExcelAttributeForDb($col, $excelRowByRef[$col][1]);
                    $table = '';
                    if (in_array($col, $this->BP_PRODUITS_fields)) {
                        $table = 'BP_PRODUITS';
                    } elseif (!in_array($col, $this->BP_PRODUITS_PICTOS_fields) && !in_array($col, $this->F_ARTFOURNISS_fields)) {
                        $table = 'F_ARTICLE';
                    }

                    //cas periode
                    if (in_array($col, $this->BP_PRODUITS_PERIODES_fields)) {
                        $type = Periode::$type[$col];
                        $table = 'BP_PRODUITS_PERIODES';
                        $update = [];
                        $sqlArray[] = Periode::updatePeriodeQuery($ref, $newVal, $type);

                        $newValImplode = implode(';', $newVal);
                        $oldValImplode = implode(';', $before);
                        $rollbackSql .= "insert into {$this->rollback} (ref,table_,col, col2, value_before,value_after,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '{$table}', '$col', $type ,  '{$oldValImplode}', '{$newValImplode}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);";
                    } //cas picto type ensoleillement (delete and insert)
                    elseif (in_array($col, $this->BP_PRODUITS_PICTOS_fields)) {
                        $table = 'BP_PRODUITS_PICTOS';
                        $pictosInput = Helper::explode($newVal);
                        $pictosInput = TypeEnsoleillement::labelsToIds($pictosInput);
                        $pictoExisting = TypeEnsoleillement::existingPictos($ref);
                        $delete = array_diff($pictoExisting, $pictosInput); //delete this
                        $inserts = array_diff($pictosInput, $pictoExisting); //insert this
                        $delete_implode = implode(',', $delete);
                        if (!empty($delete)) {
                            $sqlArray[] = "delete from {$this->BP_PRODUITS_PICTOS} where REFHOL = '{$ref}' and IDPICTO IN (".$delete_implode."); \n";
                        }
                        $insert_implode = implode(',', $inserts);
                        $rollbackSql .= "insert into {$this->rollback} (ref,table_,col,value_before,value_after,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '{$table}', 'IDPICTO', '{$delete_implode}', '{$insert_implode}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                        foreach ($inserts as $insert) {
                            $sqlArray[] = "INSERT INTO {$this->BP_PRODUITS_PICTOS}  (IDPICTO,REFHOL,CreationDate,CreatedBy) VALUES({$insert}, '{$ref}', GETDATE(), 1 ); \n";
                        }
                    } elseif ($col == 'AR_PRIXACH') {
                        $update_sql['F_ARTICLE'][] = "$col = '{$newVal}'";
                        $rollbackSql .= "insert into {$this->rollback} (ref,table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', 'F_ARTFOURNISS', '{$col}', '{$newVal}', '{$before}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                        $col2 = str_pad($excelRowByRef['CT_Num'], 3, '0', STR_PAD_LEFT);
                        $sqlArray[] = "UPDATE {$this->F_ARTFOURNISS} SET af_prixach = '{$newVal}'  WHERE AR_Ref = '{$ref}' and CT_Num = '{$col2}'; \n";
                        $rollbackSql .= "insert into {$this->rollback} (ref, col2, table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '$col2', 'F_ARTFOURNISS',  '$col', '{$newVal}', '{$before}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                    } elseif ($col == 'AF_RefFourniss') {
                        $col2 = str_pad($excelRowByRef['CT_Num'], 3, '0', STR_PAD_LEFT);
                        $sqlArray[] = "UPDATE {$this->F_ARTFOURNISS} SET AF_RefFourniss = '{$newVal}'  WHERE AR_Ref = '{$ref}' and CT_Num = '{$col2}'; \n";
                        $rollbackSql .= "insert into {$this->rollback} (ref, col2, table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '$col2', 'F_ARTFOURNISS',  '$col', '{$newVal}', '{$before}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                    } elseif ($col == 'AC_PRIXVEN') {
                        $ac_categorie = $excelRowByRef['ac_categorie'];
                        $sqlArray[] = "UPDATE {$this->F_ARTCLIENT} SET AC_PRIXVEN = '{$newVal}' WHERE AR_Ref = '{$ref}' and ac_categorie = '{$ac_categorie}'; \n";
                        $rollbackSql .= "INSERT INTO {$this->rollback} (ref, col2,  table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '{$excelRowByRef['ac_categorie']}' , 'F_ARTCLIENT',  'AC_PRIXVEN', '{$newVal}', '{$before}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                    } else {  //update classique
                        $newVal = $newVal !== null ? $conn->quote($newVal) : $newVal;
                        $before = $conn->quote($before);
                        $update_sql[$table][] = $newVal === null ? "$col = NULL" : "$col = {$newVal}";
                        $rollbackSql .= "insert into {$this->rollback} (ref,table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '{$table}', '{$col}', {$newVal}, {$before}, '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                    }
                    //colonne doublon dans 2 tables (f_article, bp_produit)
                    if (in_array($col, $this->f_article_fields)) {
                        $table = 'F_ARTICLE';
                        $update_sql[$table][] = $newVal === null ? "$col = NULL" : "$col = {$newVal}";
                        $rollbackSql .= "insert into {$this->rollback} (ref,table_,col,value_after,value_before,created_at,filename, rolled_back, user_id)
                                   VALUES('{$ref}', '{$table}', '{$col}', {$newVal}, '{$excelRowByRef[$col][0]}', '{$dateImport}', '".basename($this->fullpath)."', 0, $userId);  ";
                    }
                }
            }
            if (!empty($update_sql['F_ARTICLE'])) {
                $update_sql_txt = implode(', ', $update_sql['F_ARTICLE']);
                $sqlArray[] = "UPDATE {$this->F_ARTICLE} SET {$update_sql_txt}  WHERE AR_Ref = '{$ref}'; \n";
            }
            if (!empty($update_sql['BP_PRODUITS'])) {
                $update_sql_txt = implode(', ', $update_sql['BP_PRODUITS']);
                $sqlArray[] = "UPDATE {$this->BP_PRODUITS} SET {$update_sql_txt}, LastupdateDate = getdate()  WHERE REFHOL = '{$ref}'; \n";
            }
        }

        $sqlChunks = array_chunk($sqlArray, 1000); //nombre de requête update executé en simultané
        $sqlError = false;
        $conn = DB::connection()->getPdo();
//        $conn->beginTransaction();
        foreach ($sqlChunks as $sqlChunk) {
            $sql = implode($sqlChunk);
            try {
                $conn->exec($sql);
                $this->updated_rows += count($sqlChunk);
            } catch (\Exception $e) {
                if (str_contains($e->getMessage(), "en cours d'utilisation")) {
                    $this->retryLock($sqlChunk);
                }
//                $conn->rollBack();
//                $sqlError = true;
                $this->errors[] = "Erreur SQL lors de l'import";
                $this->errors[] = $e->getMessage();
                $this->errors[] = $sql;
                Log::error("[user: $userId] ".$e->getMessage());
                Log::alert('Erreur avec cette requête SQL  : '.$sql);
            }
        }
//        if (!$sqlError) {
//            $conn->commit();
        $this->messages[] = "$this->updated_rows lignes mise à jour en base de données.";
        try {
            $conn->exec($rollbackSql); //save rollback
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::alert('Erreur SQL(sauvegarde rollback)  : '.$rollbackSql);
            $this->errors[] = "Erreur SQL lors de l'insertion du Rollback";
            $this->errors[] = $e->getMessage();
            $this->errors[] = $rollbackSql;
        }
//        }
    }

    private function retryLock(array $sqlChunk)
    {
        foreach ($sqlChunk as $sql) {
            try {
                execute($sql);
            } catch (\Exception $e) {
                $this->errors[] = "Erreur SQL lors de l'import (Ref en cours d'utilisation)";
                $this->errors[] = "La requête posant problème est : $sql";
                Log::error($e->getMessage());
                Log::alert("Erreur SQL lors de l'import (Ref en cours d'utilisation) : ".$sql);
            }
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getHeaders($filterProductKey = false): array
    {
        $headers = [];
        if ($this->xlsx) {
            $row1 = $this->xlsx->rows()[0];
            for ($i = 0; $i < count($row1); $i++) {
                $name = mb_strtolower(trim($row1[$i]));
                $name = ASCII::to_ascii(($name));
                if ($name != '') {
                    $headers[] = $name;
                }
            }
        }

        if ($filterProductKey && isset($this->productKey) && ($key = array_search($this->productKey, $headers)) !== false) {
            unset($headers[$key]);
        }

        return $headers;
    }

    public function getExcelColumn(string $column): array
    {
        $colKey = array_search($column, $this->getHeaders());
        $result = array_column($this->xlsx->rows(), $colKey);
        $result = array_slice($result, 1, 100);

        return array_values($result);
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->hasErrors;
    }

    public function setXlsx(SimpleXLSX $xlsx): void
    {
        $this->xlsx = $xlsx;
    }
}

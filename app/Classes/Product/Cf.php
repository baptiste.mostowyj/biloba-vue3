<?php

namespace App\Classes\Product;

use App\Classes\WillemseData;
use DB;

class Cf
{
    public static $doctype = [
        0  => 'quote',
        1  => 'purchase order',
        2  => 'delivery preparation',
        3  => 'delivery note',
        4  => 'return voucher',
        5  => 'voucher',
        6  => 'invoice',
        7  => 'compatibilized invoice',
        8  => 'archive',
        10 => 'purchase order',
        11 => 'order preparation',
        12 => 'purchase order',
        // > 12 = livré
        13 => 'delivery note',
        14 => 'return voucher',
        15 => 'voucher',
        16 => 'invoice',
        17 => 'compatible invoice',
        18 => 'archive',
    ];

    //DO_domaine =  vente = 0  /  Achat = 1

    public function getInfoCF($do_piece)
    {
        $conn = DB::connection()->getPdo();
        $sql = "SET NOCOUNT ON;
        IF OBJECT_ID('tempdb..#selectcf') IS NOT NULL DROP table #selectcf;
        select [adresseDepot] 
        into #selectcf
        from [sage].[INFORMATIQUE].[dbo].[Depot]
        INNER JOIN [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCENTETE] e with (nolock)
        on e.INFO_LIVRAISON_1 = [libelleDepot]
            where e.do_domaine=1
			and e.DO_Piece = '$do_piece'
        select * from #selectcf";
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public function getAllCFs($CT_Num = null)
    {
        $clauseFournisseur = $CT_Num ? " and ct.CT_Num = '$CT_Num' " : '';
        $sql = "SET NOCOUNT ON;
        IF OBJECT_ID('tempdb..#selectcf') IS NOT NULL DROP table #selectcf;
        select e.do_piece, MAX(l.de_no) as de_no,INFO_LIVRAISON_1, e.do_tiers, e.do_statut, ct.ct_intitule, e.do_totalht, Convert(varchar,cast(e.[do_date] as date),103) as do_date ,Convert(varchar,cast(e.[do_datelivr] as date),103) as do_datelivr , e.do_doctype, MAX(a.nb_pieces_paquet) as nb_pieces_paquet, count(l.ar_ref) nb_ref, CAST(sum(l.dl_qte) as int) nb_pce , count(l.ar_ref) nb_ref_orig, CAST(sum(l.dl_qte) as int) nb_pce_orig, 0 as reliquat, ouvert, MailEnvoye,Convert(varchar,cast(m.[updated_at] as date),103) as updated_at,mf.CT_Num as template,m.do_piece as m_do_piece, m.cc,m.[to],e.CA_Num
        into #selectcf
            from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCENTETE] e with (nolock)
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCLIGNE] l with (nolock) on l.do_domaine=e.do_domaine and (l.do_piece=e.do_piece ) and l.ar_ref is not null
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] ct with (nolock) on ct.CT_Num = e.do_tiers $clauseFournisseur
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a with (nolock) on a.ar_ref=l.ar_ref
            left join [INTRANET].[dbo].[MailsCF] m on m.do_piece=e.do_piece
        left join [INTRANET].[dbo].[MailsFournisseurs] mf on mf.CT_Num=e.do_tiers
            where e.do_domaine=1
            and e.do_type IN (10,11,12)
            and a.METHODE_VENTE<>'LDF'
            group by e.do_piece, e.do_tiers, e.do_statut,INFO_LIVRAISON_1, ct.ct_intitule, e.do_totalht, e.do_date, e.do_datelivr, e.do_doctype, m.ouvert, m.MailEnvoye,m.[updated_at],mf.CT_Num,m.cc,m.[to],m.do_piece,e.CA_Num
            order by e.do_date desc, e.do_piece desc


        IF OBJECT_ID('tempdb..#selectdl') IS NOT NULL DROP table #selectdl;
        select l.dl_piecebc , count(l.ar_ref) nb_ref , CAST(sum(l.dl_qte) as int) nb_pce
        into #selectdl
        from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCLIGNE] l with (nolock)
        where  l.do_type > 12  and   l.ar_ref is not null
        group by dl_piecebc

        update s set s.nb_ref_orig=s.nb_ref_orig + l2.nb_ref, s.nb_pce_orig=s.nb_pce_orig + l2.nb_pce
        from  #selectcf s with (nolock)
        inner join #selectdl l2
        on l2.dl_piecebc=s.do_piece
        where l2.dl_piecebc IN (select  do_piece from #selectcf s)


        update #selectcf set reliquat=1 where nb_pce <> nb_pce_orig

        select * from #selectcf
            ";
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public function getAllCFsimplify()
    {
        $sql = "SET NOCOUNT ON;
            IF OBJECT_ID('tempdb..#selectcf') IS NOT NULL DROP table #selectcf;
            select e.do_piece,e.do_totalht, count(l.ar_ref) nb_ref, CAST(sum(l.dl_qte) as int) nb_pce,e.do_statut
            into #selectcf
                from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCENTETE] e
                inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCLIGNE] l on l.do_domaine=e.do_domaine and (l.do_piece=e.do_piece ) and l.ar_ref is not null
                inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=l.ar_ref
                left join [INTRANET].[dbo].[MailsCF] m on m.do_piece=e.do_piece
                where e.do_domaine=1
                and e.do_type IN (10,11,12)
                and a.METHODE_VENTE<>'LDF'
                group by e.do_piece, e.do_totalht,e.do_date , e.do_piece,e.do_statut
                order by e.do_date desc, e.do_piece desc

            select * from #selectcf";
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public function getCF($do_piece)
    {
        $sql = " SELECT TOP (1000) [DO_Piece]
          ,[MailEnvoye]
          ,[updated_at]
      FROM [INTRANET].[dbo].[MailsCF]
      where DO_Piece='$do_piece';";

        return new WillemseData($sql, null, null);
    }

    public function getImportProgress()
    {
        $conn = DB::connection()->getPdo();
        $sql = 'select resultat, count(id) as c from [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO] group by resultat';
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $nbTotal = 0;
        $nbOk = 0;
        foreach ($rows as $row) {
            if ($row['resultat'] == 'OK') {
                $nbOk += intval($row['c']);
            }
            $nbTotal += intval($row['c']);
        }

        return ['total' => $nbTotal, 'ok' => $nbOk];
    }

    public function getImportedLines()
    {
        $conn = DB::connection()->getPdo();
        $sql = 'select fournisseur, article as refart, n.depot as depot, quantite, pu_net, resultat, date_livraison, erreur, a.ar_design as nom, ct.ct_Intitule as nomfour, a.METHODE_VENTE as methode_vente
        from [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO] n
        left join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref = n.article
        left join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] ct on ct.CT_Num = n.fournisseur
        order by fournisseur,article';
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public function deleteImportedLines()
    {
        $conn = DB::connection()->getPdo();
        $sql = 'delete [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO]';
        $statement = $conn->prepare($sql);
        $statement->execute();
    }

    public static function file_upload_max_size()
    {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $post_max_size = self::parse_size(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = self::parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }

        return $max_size;
    }

    private static function parse_size($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = (int) preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }

        return round($size);
    }
}

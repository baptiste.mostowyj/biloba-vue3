<?php

namespace App\Classes\Product;

use App\Classes\WillemseData;
use SimpleXLSX;
use voku\helper\ASCII;

class TransfertsDepotsXlsManager
{
    private $fullpath;
    private $xlsx;
    private $success;
    private $ispartiel = false;
    protected $errors;
    private $hasErrors;
    protected $messages;
    public $validation;
    public $column_labels;
    public int $changeCount = 0;
    protected int $updated_rows = 0;
    public array $requiredRefCol = [
        'ref', 'reference', 'ar_ref', 'arref', 'reference produit', 'referenceproduit', 'reference willemse', 'code article',
        'qte', 'quantite', 'quantity', 'suggtransferwillvp',
    ];
    private $depotSrc = 'WILLEMSE';
    private $depotDst = 'VEEPEE';
    private array $depots = ['WILLEMSE', 'VEEPEE'];
    public string $productKey,
$stockWillKey,
$stockVpKey,
$SuggTransferWillVPKey;
    public array $base_column;
    public int $errorCount = 0;

    public function __construct(
        $fullpath,
        $depotSrc,
        $depotDst
    ) {
        $this->F_ARTICLE = config('database.connections.sqlsrv.article');
        $this->fullpath = $fullpath;
        $this->depotSrc = $depotSrc;
        $this->depotDst = $depotDst;
    }

    public function check(): bool
    {
        $this->success = true;
        $this->errors = [];
        $this->messages = [];
        if (filesize($this->fullpath) > 1024 * 1024) {
            $this->success = false;
            $this->errors[] = 'Erreur, le fichier XLS fait plus de 1 Mo. ';
        } else {
            if ($this->xlsx = SimpleXLSX::parse($this->fullpath)) {
                $ref = array_intersect($this->getHeaders(), $this->requiredRefCol);
                if (empty($ref) || count($ref) < 2) {
                    $this->success = false;
                    $this->errors[] = "Le fichier XLS doit avoir une colonne ref et quantité qui peuvent s'écrire : ".implode(', ', $this->requiredRefCol);
                } else {
                    $key = array_keys($ref);
                    $this->productKey = $ref[$key[0]];
                    $this->SuggTransferWillVPKey = $ref[$key[1]];
                }
            } else {
                $this->success = false;
                $this->errors[] = SimpleXLSX::parseError();
            }
        }

        return $this->success;
    }

    public function getLineCount(): int
    {
        return count($this->xlsx->rows()) - 1;
    }

    /**
     * Retourne le tableau représentant le fichier excel pour la vue html
     * Avec pour chaque ligne le avant/après ou erreur de validation.
     */
    public function verifyExcel($depotDst, $is_notpart = true)
    {
        if ($this->check()) {
            $productRefKey = array_search($this->productKey, $this->getHeaders());

            $listRefProduit = $listRef = []; //boucle ligne excel pour faire la liste des produits
            foreach ($this->xlsx->rows() as $i => $exelRow) {
                $i++;
                $ref = ($exelRow[$productRefKey]); //article ref database

                if ($i == 1 || blank($ref)) {
                    continue;
                }
                $ref = ref_pad($ref);
                $listRefProduit[] = "'".$ref."'";
                $listRef[] = $ref;
            }
            if ($is_notpart) {
                $this->checkQte($listRefProduit, $depotDst);
                if ($this->checkRefs($listRefProduit)) {
                }
            }
        }

        return ['success' => $this->success, 'partiel' => $this->ispartiel];
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    private function checkDepot()
    {
        if (!($this->depotSrc != $this->depotDst)) {
            $this->errors[] = 'Les Depots Source et Destinataire sont identiques';
            $this->success = false;
        }
        if (!(in_array($this->depotSrc, $this->depots) && in_array($this->depotDst, $this->depots))) {
            $this->errors[] = 'Depot invalide : " Sources: '.$this->depotSrc.' Destinataire: '.$this->depotDst.'".';
            $this->success = false;
        }
    }

    public function checkRefs(array $listRefProduit)
    {
        $sql = "select a.AR_Ref
        from {$this->F_ARTICLE} a          
        where  a.AR_Ref in (".implode(',', $listRefProduit).') ;';
        $this->willemseData = new WillemseData($sql);
        $refDb = $this->willemseData->getRows();
        $listRef = [];
        foreach ($refDb as $ref) {
            array_push($listRef, "'".$ref['AR_Ref']."'");
        }
        if (count($refDb) < count($listRefProduit)) {
            $this->errors[] = 'Les Refs : '.implode(',', array_diff($listRefProduit, $listRef))." n'existent pas";
            $this->success = false;
        }

        return $this->success;
    }

    public function checkQte($listRefProduit, $depotDst)
    {
        $headers = $this->getHeaders();
        $productRefKey = array_search($this->productKey, $headers);
        $not = '';
        $column = 'stockVP';
        $nbEntrepot = 3;
        if ($depotDst == 'VEEPEE') {
            $not = 'NOT';
            $column = 'stockWillemse';
            $nbEntrepot = 1;
        }

        $qteDstKey = array_search($this->SuggTransferWillVPKey, $headers);
        $refStockPart = [];

        $sql = " SET NOCOUNT ON 
            drop table if exists #cdes_annul_intranet;
            drop table if exists #articles_q;

            select do_piece
            into #cdes_annul_intranet
            from SAGE.[WILLEMSE_MAQUETTE].[dbo].[F_DOCENTETE]
            where DO_STATUT <> 2 and do_type IN (1,2,6,7)

            -- On recup toutes les ref
            SELECT a.AR_Ref, 0 as reserve, null as stock_dispo
            into #articles_q 
            from SAGE.[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a

            -- Calcule du stock reserve
            UPDATE aq set reserve = r.reservrQte
            FROM #articles_q aq
            inner join (SELECT aq.ar_ref as ref  , CAST(SUM(dl_qte) as INT) as reservrQte
                FROM #articles_q aq
                inner join SAGE.[WILLEMSE_MAQUETTE].[dbo].[F_DOCLIGNE] l on  aq.AR_Ref = l.AR_Ref and l.do_domaine=0 and l.do_type=1
                WHERE do_piece NOT IN (select do_piece from #cdes_annul_intranet) and aq.ar_ref = '004228' 
                and ca_num $not IN ('VPFLO','SRPFLO','SRPWIL','VPWIL')   -- voir le canal
                group by aq.ar_ref
            ) r -- voir le canal
            on aq.ar_ref = r.ref

            UPDATE aq set stock_dispo=coalesce(s.AS_QteSto - s.AS_QtePrepa - reserve, 0)
            FROM #articles_q aq
            right join SAGE.[WILLEMSE_MAQUETTE].[dbo].[F_ARTSTOCK] s on s.ar_ref=aq.ar_ref
            WHERE s.DE_No=$nbEntrepot -- entrepot : 1 = willemse 3 = VEEPEE

        
        select AR_Ref,stock_dispo as $column FROM #articles_q aq 
        where ar_ref in (".implode(',', $listRefProduit).') ; ';

        $conn = \DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $dbRows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        if (count($dbRows) > 0) {
            $qteSrc = []; // Stock en base ( Source du transfert)
            foreach ($dbRows as $i => $row) {
                $qteSrc[$row['AR_Ref']] = $row[$column];
            }
            foreach ($this->xlsx->rows() as $i => $exelRow) {
                if ($i < 1) {
                    continue;
                }
                $ref = $exelRow[$productRefKey]; //article ref database
                $ref = ref_pad($ref);
                $qteDest = $exelRow[$qteDstKey]; // Stock excel ( Desctinataire du transfert)

                if (isset($qteSrc[$ref])) {
                    if (($qteSrc[$ref] > 0 && $qteSrc[$ref] >= $qteDest) && ($qteDest > 0 && $qteDest <= $qteSrc[$ref]) && (!blank($qteDest) && !blank($qteDest))) {
                        $this->success = true;
                    } elseif ($qteSrc[$ref] && ($qteDest > 0 && $qteDest > $qteSrc[$ref]) && (!empty($qteDest) && !empty($qteDest))) {
                        array_push($refStockPart, $ref);
                    } else {
                        $this->success = false;
                        $this->errors[] = 'Problème de stock pour la ref: '.$ref.' (Stock manquant, Stock négatif ou Stock à 0)';
                        break;
                    }
                } else {
                    $sql = "Select count(ref) FROM [LISTE_CATALOGUE_ERP].[dbo].[operation_produits]
                        where ref in ( SELECT [AR_Ref]
                        FROM [sage].[WILLEMSE_MAQUETTE].[dbo].[F_NOMENCLAT] n
                        WHERE n.[NO_RefDet]='$ref') and date_debut < GETDATE() and date_fin > GETDATE();
                     ";
                    $this->willemseData = new WillemseData($sql);
                    $dbOPe = $this->willemseData->getRows();
                    if (count($dbOPe) > 0) {
                        $this->success = true;
                    } else {
                        $this->success = false;
                        $this->errors[] = 'Problème sur la ref: '.$ref;
                    }
                }
            }
            if (count($refStockPart) > 0) {
                $this->success = false;
                $this->ispartiel = true;
                $this->errors[] = 'Le stock à transfert pour les refs : '.implode(', ', $refStockPart).' sont supérieur au stock source. Un envoi partielle est possible ';
            }
        } else {
            $this->success = false;
            $this->errors[] = 'Aucunes refs ne peut être transferées ';
        }

        return $this->success;
    }

    public function getHeaders($filterProductKey = false): array
    {
        $headers = [];
        if ($this->xlsx) {
            $row1 = $this->xlsx->rows()[0];
            for ($i = 0; $i < count($row1); $i++) {
                $name = mb_strtolower(trim($row1[$i]));
                $name = ASCII::to_ascii(($name));
                if ($name != '') {
                    $headers[] = $name;
                }
            }
        }

        if ($filterProductKey && isset($this->productKey) && ($key = array_search($this->productKey, $headers)) !== false) {
            unset($headers[$key]);
        }

        return $headers;
    }

    public function getJson($src, $dst, $key)
    {
        $headers = $this->getHeaders();
        $productRefKey = array_search($this->productKey, $headers);
        $qteDstKey = array_search($this->SuggTransferWillVPKey, $headers);
        $productsjson = [];

        foreach ($this->xlsx->rows() as $i => $exelRow) {
            if ($i < 1) {
                continue;
            }
            $ref = $exelRow[$productRefKey]; //article ref database
            $ref = ref_pad($ref);
            $qteDest = $exelRow[$qteDstKey];

            array_push($productsjson, ['ref' => $ref, 'qty' => $qteDest]);
        }
        $productsjson = json_encode($productsjson);
        $json = '{"cle": "'.$key.'","depotSrc" : "'.$src.'","depotDst" : "'.$dst.'", "products" : '.$productsjson.'}';

        return $json;
    }

    public function getExcelColumn(string $column): array
    {
        $colKey = array_search($column, $this->getHeaders());
        $result = array_column($this->xlsx->rows(), $colKey);
        $result = array_slice($result, 1, 100);

        return array_values($result);
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->hasErrors;
    }

    public function setXlsx(SimpleXLSX $xlsx): void
    {
        $this->xlsx = $xlsx;
    }
}

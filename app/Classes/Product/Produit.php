<?php

namespace App\Classes\Product;

use DB;

// Mappe la table produits_stock_dep
class Produit
{
    public $refart;
    public $name;
    public $statut;
    public $nomencl;
    public $fournisseur;
    public $delai_fournisseur;
    public $methode_vente;
    public $ldf;
    public $en_sommeil;
    public $debpe;
    public $finpe;
    public $debah;
    public $finah;
    public $finah_date_prev;
    public $debpe_date;
    public $finpe_date;
    public $debah_date;
    public $finah_date;
    public $debpe_date_next;
    public $date_achat;
    public $date_achat2;
    public $date_reception;
    public $date_reception2;
    public $date_dep;
    public $date_dep_x_jours;
    public $x_jours;
    public $commentaire;
    public $stock;
    public $commande;
    public $reserve;
    public $prepare;
    public $dispo;
    public $stock_terme;
    public $a_receptionner;
    public $nom_fournisseur;
    public $blocage_vente_web;
    public $categorie_web;
    public $ShowStockLevel;
    public $ShowIfOutOfStock;
    public $SaleIfOutOfStock;
    public $SaleIfOutOfStockScenario;
    public $ShowDaysToship;
    public $Visible;
    public $ShowInStockNote;
    public $DaysToShip;
    public $QtyInStock;
    public $date_dep_PRINT;
    public $stock_SAGE;
    public $qte_derniere_reception;
    public $date_derniere_reception;
    public $days_until_season_start;

    public static function fromDbRow($row, $prefix = '')
    {
        $product = new self();
        $product->refart = $row[$prefix.'refart'];
        $product->name = $row[$prefix.'name'];
        $product->statut = $row[$prefix.'statut'];
        $product->nomencl = intval($row[$prefix.'nomencl']);
        $product->fournisseur = $row[$prefix.'fournisseur'];
        $product->delai_fournisseur = intval($row[$prefix.'delai_fournisseur']);
        $product->methode_vente = $row[$prefix.'methode_vente'];
        $product->ldf = $row[$prefix.'ldf'];
        $product->en_sommeil = $row[$prefix.'en_sommeil'];
        $product->debpe = $row[$prefix.'debpe'];
        $product->finpe = $row[$prefix.'finpe'];
        $product->debah = $row[$prefix.'debah'];
        $product->finah = $row[$prefix.'finah'];
        $product->finah_date_prev = $row[$prefix.'finah_date-1'] == null ? null : date_create($row[$prefix.'finah_date-1']);
        $product->debpe_date = $row[$prefix.'debpe_date'] == null ? null : date_create($row[$prefix.'debpe_date']);
        $product->finpe_date = $row[$prefix.'finpe_date'] == null ? null : date_create($row[$prefix.'finpe_date']);
        $product->debah_date = $row[$prefix.'debah_date'] == null ? null : date_create($row[$prefix.'debah_date']);
        $product->finah_date = $row[$prefix.'finah_date'] == null ? null : date_create($row[$prefix.'finah_date']);
        $product->debpe_date_next = $row[$prefix.'debpe_date+1'] == null ? null : date_create($row[$prefix.'debpe_date+1']);
        $product->date_achat = $row[$prefix.'date_achat'] == null ? null : date_create($row[$prefix.'date_achat']);
        $product->date_achat2 = $row[$prefix.'date_achat2'] == null ? null : date_create($row[$prefix.'date_achat2']);
        $product->date_reception = $row[$prefix.'date_reception'] == null ? null : date_create($row[$prefix.'date_reception']);
        $product->date_reception2 = $row[$prefix.'date_reception2'] == null ? null : date_create($row[$prefix.'date_reception2']);
        $product->date_dep = $row[$prefix.'date_dep'] == null ? null : date_create($row[$prefix.'date_dep']);
        $product->date_dep_x_jours = $row[$prefix.'date_dep_x_jours'] == null ? null : date_create($row[$prefix.'date_dep_x_jours']);
        $product->x_jours = intval($row[$prefix.'x_jours']);
        $product->commentaire = $row[$prefix.'commentaire'];
        $product->stock = intval($row[$prefix.'stock']);
        $product->commande = intval($row[$prefix.'commandé']);
        $product->reserve = intval($row[$prefix.'reserve']);
        $product->prepare = intval($row[$prefix.'prepare']);
        $product->dispo = intval($row[$prefix.'dispo']);
        $product->stock_terme = intval($row[$prefix.'stock_terme']);
        $product->a_receptionner = intval($row[$prefix.'a_receptionner']);
        $product->nom_fournisseur = $row[$prefix.'nom_fournisseur'];
        $product->blocage_vente_web = $row[$prefix.'blocage_vente_web'];
        $product->categorie_web = $row[$prefix.'categorie_web'];
        $product->ShowStockLevel = boolval($row[$prefix.'ShowStockLevel']);
        $product->ShowIfOutOfStock = boolval($row[$prefix.'ShowIfOutOfStock']);
        $product->SaleIfOutOfStock = boolval($row[$prefix.'SaleIfOutOfStock']);
        $product->SaleIfOutOfStockScenario = boolval($row[$prefix.'SaleIfOutOfStockScenario']);
        $product->ShowDaysToship = boolval($row[$prefix.'ShowDaysToship']);
        $product->Visible = boolval($row[$prefix.'Visible']);
        $product->ShowInStockNote = boolval($row[$prefix.'ShowInStockNote']);
        $product->DaysToShip = intval($row[$prefix.'DaysToShip']);
        $product->QtyInStock = intval($row[$prefix.'QtyInStock']);
        $product->date_dep_PRINT = $row[$prefix.'date_dep_PRINT'] == null ? null : date_create($row[$prefix.'date_dep_PRINT']);
        $product->stock_SAGE = intval($row[$prefix.'stock_SAGE']);
        $product->qte_derniere_reception = intval($row[$prefix.'qte_derniere_reception']);
        $product->date_derniere_reception = $row[$prefix.'date_derniere_reception'] == null ? null : date_create($row[$prefix.'date_derniere_reception']);

        $now = new \DateTime('now');
        $interval = $now->diff($product->debpe_date);
        $product->days_until_season_start = $interval->days + 1;

        return $product;
    }

    public function fetchItem()
    {
        $this->item = Item::getFromRef($this->refart);
    }

    public static function getFromRef($ref)
    {
        $conn = DB::connection()->getPdo();
        $sql = 'select * from sampi.liste_catalogue_erp.dbo.produits_stock_dep WHERE refart=?;';
        $params = [$ref];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);
        $product = self::fromDbRow($row);

        return $product;
    }
}

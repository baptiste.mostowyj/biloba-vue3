<?php

namespace App\Classes\Product;

use App\Models\ProductRollback;
use App\Rules\Periode;
use DB;
use Log;

class RollbackProduct extends ProductXlsManager
{
    public function doRollbacks($date, $refToUpdate = null)
    {
        set_time_limit(300);
        $rollbacksByRef = ProductRollback::where('created_at', $date);
        if ($refToUpdate != null) {
            $rollbacksByRef = $rollbacksByRef->where('ref', $refToUpdate)->where('rolled_back', 0);
        }
        $rollbacksByRef = $rollbacksByRef->get()->groupBy('ref')->toArray();
        $sql = '';
        foreach ($rollbacksByRef as $ref => $rollbacks) {
            $sql_update = [];
            $before = $after = $rollback = [];
            foreach ($rollbacks as $key => $rollback) {
                $rollbacksByRef[$ref][$rollback['col']] = $rollback;
                unset($rollbacksByRef[$ref][$key]);
                $value_before = $rollback['value_before'];
                $col = $rollback['col'];
                $sql_update[$rollback['table_']][] = $value_before === null ? "$col = NULL" : "$col = '{$value_before}'";
                $before[$rollback['table_']] = $value_before;
                $after[$rollback['table_']] = $rollback['value_after'];
            }
            if (!empty($sql_update['F_ARTICLE'])) {
                $update_sql_txt = implode(', ', $sql_update['F_ARTICLE']);
                $sql .= "UPDATE {$this->F_ARTICLE} SET {$update_sql_txt} WHERE AR_Ref = '{$ref}'; \n";
            }
            if (!empty($sql_update['F_ARTFOURNISS'])) {
                $update_sql_txt = implode(', ', $sql_update['F_ARTFOURNISS']);
                $sql .= "UPDATE {$this->F_ARTFOURNISS} SET {$update_sql_txt} WHERE AR_Ref = '{$ref}' and CT_Num = '{$rollback['col2']}'; \n";
            }
            if (!empty($sql_update['F_ARTCLIENT'])) {
                $update_sql_txt = implode(', ', $sql_update['F_ARTCLIENT']);
                $sql .= "UPDATE {$this->F_ARTCLIENT} SET {$update_sql_txt} WHERE AR_Ref = '{$ref}' and ac_categorie = '{$rollback['col2']}'; \n";
            }
            if (!empty($sql_update['BP_PRODUITS'])) {
                $update_sql_txt = implode(', ', $sql_update['BP_PRODUITS']);
                $sql .= "UPDATE {$this->BP_PRODUITS} SET {$update_sql_txt} WHERE REFHOL = '{$ref}'; \n";
            }
            if (!empty($sql_update['BP_PRODUITS_PICTOS'])) {
                $inserts = array_filter(explode(',', $before['BP_PRODUITS_PICTOS']));
                $delete_implode = $after['BP_PRODUITS_PICTOS'];
                if (!empty($delete_implode)) {
                    $sql .= "DELETE FROM {$this->BP_PRODUITS_PICTOS} WHERE REFHOL = '{$ref}' and IDPICTO IN ($delete_implode); \n";
                }
                foreach ($inserts as $insert) {
                    $sql .= "INSERT INTO {$this->BP_PRODUITS_PICTOS}  (IDPICTO,REFHOL,CreationDate,CreatedBy) VALUES('{$insert}', '{$ref}', GETDATE(), 1 ); \n";
                }
            }
            if (!empty($sql_update['BP_PRODUITS_PERIODES'])) {
                $periodes = array_filter(explode(';', $before['BP_PRODUITS_PERIODES']));
                $update = [];
                foreach (Periode::$periodes as $key => $periode) {
                    if (in_array($key, $periodes)) {
                        $update[] = "$key = '1'";
                    } else {
                        $update[] = "$key = '0'";
                    }
                }
                $update = implode(',', $update);
                $sql .= "update {$this->BP_PRODUITS_PERIODES} set {$update} where REFHOL = '{$ref}' and IDTYPPER = '{$rollback['col2']}';";
            }
            $andRef = $refToUpdate ? " and ref = '{$ref}'" : '';
            $sql .= "update {$this->rollback} set rolled_back = 1 where created_at = '{$date}' $andRef; \n";
            $this->updated_rows++;
        }
        try {
            $conn = DB::connection()->getPdo();
            $conn->exec($sql);
            $this->messages[] = "$this->updated_rows produits rollback en base de données.";
        } catch (\Exception $e) {
            $this->errors[] = "Erreur SQL lors de l'import . ";
            $this->errors[] = $e->getMessage();
            Log::error($e->getMessage());
            Log::alert('Erreur avec cette requête SQL  : '.$sql);
        }
    }

    public function cleanOldRollback()
    {
        try {
            $sql = "DELETE FROM {$this->rollback} where created_at < getDate()-30;";
            $conn = DB::connection()->getPdo();
            $conn->exec($sql);
        } catch (\Exception $e) {
            $this->errors[] = 'Erreur SQL lors de cleanOldRollback';
            $this->errors[] = $e->getMessage();
            Log::error($e->getMessage());
            Log::alert('Erreur avec cette requête SQL  : '.$sql);
        }
    }
}

<?php

namespace App\Classes\Product;

use DB;
use SimpleXLSX;

class CfXlsChecker
{
    private $fullpath;
    private $checker;
    private $success = true;
    private $errors = [];
    private $messages = [];
    private $columnIndexes = [];
    private $depot = 'WILLEMSE';
    private static $columnNames = ['référence', 'désignation', 'nom latin', 'n° fou', 'quantité', 'date de livraison'];

    public function __construct($fullpath, $depot)
    {
        $this->fullpath = $fullpath;
        $this->depot = $depot;
    }

    public function check()
    {
        $this->errors = [];
        $this->messages = [];
        $this->columnIndexes = [];
        if (filesize($this->fullpath) > 1024 * 1024) {
            $this->success = false;
            $this->errors[] = 'Erreur, le fichier XLS fait plus de 1 Mo. Les fichiers de commande fournisseur font habituellement moins de 50Ko.';
        } elseif ($this->checker = SimpleXLSX::parse($this->fullpath)) {
            $this->checkHeader();
            if ($this->success) {
                $this->checkRef();
            }
            if ($this->success) {
                $this->checkFournisseur();
            }
            if ($this->success) {
                $this->checkQuantite();
            }
            if ($this->success) {
                $this->checkDate();
            }
            if ($this->success) {
                $this->checkDepot();
            }
        } else {
            $this->success = false;
            $this->errors[] = SimpleXLSX::parseError();
        }
    }

    public function sendDB()
    {
        $conn = DB::connection()->getPdo();
        $sql = 'delete [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP];';
        $statement = $conn->prepare($sql);
        $statement->execute();

        $i = 0;
        $now = date('Y-m-d');

        foreach ($this->checker->rows() as $i => $row) {
            $i++;
            if ($i == 1) {
                continue;
            }

            $sql = 'INSERT INTO [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP] (fournisseur, article, quantite, pu_net, date_livraison, created_at, depot) VALUES (?, ?, ?, 0, ?, ?, ?);';

            $params = [
                $row[$this->columnIndexes['n° fou']],
                $row[$this->columnIndexes['référence']],
                intval($row[$this->columnIndexes['quantité']]),
                $row[$this->columnIndexes['date de livraison']],
                $now,
                $this->depot,
            ];

            $statement = $conn->prepare($sql);
            $statement->execute($params);
        }

        $sql = 'SELECT top(1) created_at from [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP] order by created_at desc;';
        $statement = $conn->prepare($sql);
        $statement->execute();
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        $created_at = $row['created_at'];

        $sql = 'UPDATE t1 set pu_net=t2.[AF_PrixAch] from [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP] t1,
          [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTFOURNISS] t2
          where	t1.article = t2.ar_ref
          and		t2.af_principal = 1 ;';

        $statement = $conn->prepare($sql);
        $statement->execute();

        // màj les prix progressifs de Guido et Lepage
        $sql = "
                select	borne_inf=1,* into #tarifsprogessifs from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_tarifqte] where TQ_RefCF IN ('033', '039');

                -- guido
                update	#tarifsprogessifs set borne_inf = 23 where tq_bornesup = 9999 and TQ_RefCF = '033';
                update	#tarifsprogessifs set borne_inf = 11 where tq_bornesup = 23 and TQ_RefCF = '033';
                update	#tarifsprogessifs set borne_inf = 5 where tq_bornesup = 11 and TQ_RefCF = '033';
                update	#tarifsprogessifs set borne_inf = 1 where tq_bornesup = 5 and TQ_RefCF = '033';
                update	#tarifsprogessifs set borne_inf = 0 where tq_bornesup = 1 and TQ_RefCF = '033';

                -- lepage
                update	#tarifsprogessifs set borne_inf = 119 where tq_bornesup = 9999 and TQ_RefCF = '039';
                update	#tarifsprogessifs set borne_inf = 24 where tq_bornesup = 119 and TQ_RefCF = '039';
                update	#tarifsprogessifs set borne_inf = 5 where tq_bornesup = 24 and TQ_RefCF = '039';
                update	#tarifsprogessifs set borne_inf = 0 where tq_bornesup = 5 and TQ_RefCF = '039';

                update	t1
                set		pu_net = t2.TQ_PrixNet * coalesce(a.NB_PIECES_PAQUET,1)
                from	[sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP] t1,
                        #tarifsprogessifs t2,
                        [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a
                where	t1.article=t2.ar_ref
                    and a.ar_ref = t2.ar_ref
                    and	t1.quantite*coalesce(a.NB_PIECES_PAQUET,1) > borne_inf
                    and	t1.quantite*coalesce(a.NB_PIECES_PAQUET,1) <= t2.TQ_BorneSup
                    and t1.created_at = ?;

                drop table #tarifsprogessifs;
                ";

        $params = [$created_at];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        //$statement->execute();

        // passe les produits dans _NRC_REAPPRO_
        $sql = 'DELETE [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO];
                INSERT INTO [sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO]
                (fournisseur,article,quantite,pu_net,date_livraison,depot)
                select	fournisseur,article,quantite,pu_net,date_livraison,depot from	[sage].[WILLEMSE_MAQUETTE].[dbo].[_NRC_REAPPRO_WIP]
                where		fournisseur is not null and created_at = ?;';

        $params = [$created_at];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        //$statement->execute();
    }

    public function isSuccess()
    {
        return $this->success;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    private function checkDepot()
    {
        if (!($this->depot == 'WILLEMSE' || $this->depot == 'VEEPEE' || $this->depot == 'VEEPEE-WIL')) {
            $this->errors[] = 'Depot invalide : "'.$this->depot.'".';
            $this->success = false;
        }
    }

    private function checkHeader()
    {
        $row1 = $this->checker->rows()[0];
        $names = [];
        for ($i = 0; $i < count($row1); $i++) {
            $name = mb_strtolower(trim($row1[$i]));
            $names[] = $name;
        }

        for ($i = 0; $i < count(self::$columnNames); $i++) {
            $name = self::$columnNames[$i];
            if (!in_array($name, $names)) {
                $this->errors[] = 'Colonne manquante : '.$name.'.';
                $this->success = false;

                return;
            }
            for ($j = 0; $j < count($names); $j++) {
                if ($names[$j] == $name) {
                    $this->columnIndexes[$name] = $j;
                    continue;
                }
            }
        }
        $this->messages[] = 'Nom des colonnes : OK';
    }

    private function checkRef()
    {
        $refs = [];
        $refsAndCells = [];

        foreach ($this->checker->rowsEx() as $i => $row) {
            if ($i == 0) {
                continue;
            } // header
            $cell = $row[$this->columnIndexes['référence']];
            $refName = $row[$this->columnIndexes['désignation']]['value'];
            $value = $cell['value'];
            if (empty($value)) {
                $this->errors[] = 'Ligne incomplète. La cellule '.$cell['name'].' est vide.';
                $this->success = false;
                continue;
            }
            if ($cell['type'] != 's' && $cell['type'] != 'inlineStr') {
                $this->errors[] = 'Type de cellule incorrect : la référence dans la cellule '.$cell['name']." n'est pas du type chaine.";
                $this->success = false;
                continue;
            }
            if (strlen($value) !== 6) {
                $this->errors[] = "Format de ref incorrect : la valeur '$value' de la cellule ".$cell['name'].' ne fait pas 6 caractères.';
                $this->success = false;
                continue;
            }
            // ajoute la ref
            if (!in_array($value, $refs)) {
                $refs[] = "'".$value."'";
                $refsAndCells[$value] = $cell['name'];
            }
        }

        if (!$this->success) {
            return;
        }
        $this->messages[] = 'Format des refs produit : OK';

        // check les refs
        $conn = DB::connection()->getPdo();
        $sql = 'SELECT * from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] where ar_ref IN ('.implode(',', $refs).')';
        $params = [];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $dbrows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($dbrows as $i => $dbrow) {
            $dbref = $dbrow['AR_Ref'];
            unset($refsAndCells[$dbref]);
        }

        if (count($refsAndCells) != 0) {
            $this->errors[] = 'Les références suivantes sont incorrectes : '.implode(',', array_keys($refsAndCells)).' (cellules '.implode(',', array_values($refsAndCells)).').';
            $this->success = false;
        }

        $this->messages[] = 'Existance des refs produit dans Sage : 100%';

        // les refs sont-elles commercialisées ?
        $notSold = [];
        foreach ($dbrows as $i => $dbrow) {
            $dbref = $dbrow['AR_Ref'];
            $dbStatus = $dbrow['STATUT_PRODUIT'];
            $nom = $dbrow['AR_Design'];
            if ($dbStatus != 'Commercialisé') {
                $this->errors[] = "La référence $dbref '$nom' n'est plus commercialisée : '".$dbStatus."'.";
                $this->success = false;
            }
        }

        $this->messages[] = 'Ref en arrêt : Aucune';
    }

    private function checkFournisseur()
    {
        $refAndFour = [];
        $dbfours = [];

        foreach ($this->checker->rowsEx() as $i => $row) {
            if ($i == 0) {
                continue;
            } // header
            $cellRef = $row[$this->columnIndexes['référence']];
            $ref = $cellRef['value'];

            $cell = $row[$this->columnIndexes['n° fou']];
            if ($cell['type'] != 's' && $cell['type'] != 'inlineStr') {
                $this->errors[] = "Type de cellule incorrect : la cellule 'n° fou' ".$cell['name']." n'est pas du type chaine.";
                $this->success = false;
                continue;
            }
            $noFour = $cell['value'];
            if (strlen($noFour) !== 3 && strlen($noFour) !== 4) {
                $this->errors[] = "Format de ref incorrect : le numero de fournisseur '$noFour' de la cellule ".$cell['name'].' ne fait pas 3 caractères.';
                $this->success = false;
                continue;
            }

            // ajoute la ref
            if (isset($refAndFour[$ref]) && $refAndFour[$ref] === $noFour) {
                $this->errors[] = "Ref en double : la ref '$ref' est présente 2 fois avec le même fournisseur n°$noFour.";
                $this->success = false;
                continue;
            }

            $refAndFour[$ref] = $noFour;
        }

        if (!$this->success) {
            return;
        }
        $this->messages[] = 'Format des n° fournisseur : OK';

        $refAndFourDb = [];
        $conn = DB::connection()->getPdo();
        $in = '';
        foreach (array_keys($refAndFour) as $ref) {
            $in .= "'".$ref."',";
        }
        $in = rtrim($in, ", \n\r\t\v\0");
        $sql = 'SELECT af.AR_Ref as AR_Ref, af.CT_Num as CT_Num, c.CT_Intitule as CT_Intitule from [sage].[WILLEMSE_MAQUETTE].[dbo].F_ARTFOURNISS af'
          .' inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] c on c.CT_Num = af.CT_Num'
          .' where AR_Ref IN ('.$in.')';
        $params = [];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $dbrows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($dbrows as $i => $dbrow) {
            $dbref = $dbrow['AR_Ref'];
            $dbfour = $dbrow['CT_Num'];
            $dbfourName = $dbrow['CT_Intitule'];
            $refAndFourDb[] = $dbref.'-'.$dbfour;
            $dbfours[$dbfour] = $dbfourName;
        }

        foreach ($refAndFour as $ref => $noFour) {
            $reffour = $ref.'-'.$noFour;
            if (!in_array($reffour, $refAndFourDb)) {
                $nomFour = '';
                if (isset($dbfours[$noFour])) {
                    $nomFour = $dbfours[$noFour];
                }
                $this->errors[] = "Erreur de fournisseur : la ref '$ref' n'existe pas chez le fournisseur n°$noFour $nomFour.";
                $this->success = false;
            }
        }

        $this->messages[] = 'Validité des founisseurs des refs (Sage) : 100%';
    }

    private function checkQuantite()
    {
        $refs = [];
        $refsAndCells = [];

        foreach ($this->checker->rowsEx() as $i => $row) {
            if ($i == 0) {
                continue;
            } // header
            $cellRef = $row[$this->columnIndexes['référence']];
            $ref = $cellRef['value'];

            $cell = $row[$this->columnIndexes['quantité']];
            if ($cell['type'] != '' && $cell['type'] != 'n') {
                $this->errors[] = "Type de cellule incorrect : la cellule 'quantité' ".$cell['name']." n'est pas du type nombre.";
                $this->success = false;
                continue;
            }
            $q = floatval($cell['value']);
            $qMax = 10000;
            // if (intval($q) === 0) {
            //   $this->errors[] = "quantité incorrecte : la quantité de la ref $ref, cellule ".$cell['name']." est 0.";
            //   $this->success = false;
            //   continue;
            // }
            if ($q < 0) {
                $this->errors[] = "quantité incorrecte : la quantité de la ref $ref, cellule ".$cell['name']." est inférieure à 0 : $q.";
                $this->success = false;
                continue;
            }
            if ($q > $qMax) {
                $this->errors[] = "quantité incorrecte : la quantité de la ref $ref, cellule ".$cell['name']." est supérieure à $qMax : $q.";
                $this->success = false;
                continue;
            }
        }

        $this->messages[] = 'quantité nulle : aucune';
        $this->messages[] = 'quantité négative : aucune';
        $this->messages[] = 'quantité >10000 : aucune';
    }

    private function checkDate()
    {
        $dates = [];
        foreach ($this->checker->rowsEx() as $i => $row) {
            if ($i == 0) {
                continue;
            } // header
            $cellRef = $row[$this->columnIndexes['référence']];
            $ref = $cellRef['value'];

            $cell = $row[$this->columnIndexes['date de livraison']];
            $d = date_parse($cell['value']);

            $timestamp = mktime(
                $d['hour'],
                $d['minute'],
                $d['second'],
                $d['month'],
                $d['day'],
                $d['year']
            );

            $dString = $d['year'].$d['day'].$d['month'];
            if (!in_array($dString, $dates)) {
                $dates[] = $dString;
            }

            if ($d['error_count'] > 0) {
                $this->errors[] = "Date incorrecte pour la ref $ref, cellule ".$cell['name'].' : "'.implode(',', $d['errors']).'"';
                $this->success = false;
                continue;
            }
            if ($timestamp < time()) {
                $this->errors[] = "Date incorrecte pour la ref $ref, cellule ".$cell['name'].', la date est passée : '.$cell['value'].'.';
                $this->success = false;
                continue;
            }
            if ($timestamp > time() + 86400 * 365 * 1.5) {
                $this->errors[] = "Date incorrecte pour la ref $ref, cellule ".$cell['name'].', la date est dans plus 18 mois : '.$cell['value'].'.';
                $this->success = false;
                continue;
            }
        }

        if (!$this->success) {
            return;
        }
        $this->messages[] = 'Formats de date incorrects : aucun';
        $this->messages[] = 'Date livraison dans le passée : aucune';
        $this->messages[] = 'Date livraison dans plus de 18 mois : aucune';

        if (count($dates) > 10) {
            $this->errors[] = 'Erreur, le fichier contient plus de 10 dates de livraison différentes.';
            $this->success = false;
        }

        $this->messages[] = 'Dates de livraison différetes : '.count($dates);
    }
}

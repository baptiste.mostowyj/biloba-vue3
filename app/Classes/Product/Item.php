<?php

namespace App\Classes\Product;

use DB;

// mappe la table SCRAPER.dbo.item
class Item
{
    public $id;
    public $platformId;
    public $name;
    public $ref;
    public $idMerchant;
    public $ean;
    public $url;
    public $dep;
    public $coverImage;
    public $dropShipped;
    public $status;
    public $stock;
    public $price;
    public $active;

    public static function fromDbRow($row, $prefix = '')
    {
        $item = new self();
        $item->id = intval($row[$prefix.'id']);
        $item->platformId = $row[$prefix.'platformId'];
        $item->name = $row[$prefix.'name'];
        $item->ref = $row[$prefix.'ref'];
        $item->idMerchant = intval($row[$prefix.'idMerchant']);
        $item->ean = $row[$prefix.'ean'];
        $item->url = $row[$prefix.'url'];
        $item->dep = $row[$prefix.'dep'] == null ? null : date_create($row[$prefix.'dep']);
        $item->coverImage = $row[$prefix.'coverImage'];
        $item->dropShipped = boolval($row[$prefix.'dropShipped']);
        $item->status = intval($row[$prefix.'status']);
        $item->statusStr = 'unav';
        if ($item->status == 1) {
            $item->statusStr = 'instock';
        }
        if ($item->status == 2) {
            $item->statusStr = 'preorder';
        }
        if ($item->status == 3) {
            $item->statusStr = 'onorder';
        }
        $item->stock = intval($row[$prefix.'stock']);
        $item->price = floatval($row[$prefix.'price']);
        $item->active = boolval($row[$prefix.'active']);

        return $item;
    }

    public static function getFromRef($ref)
    {
        $conn = DB::connection('scraper')->getPdo();
        $sql = 'select * from item WHERE idMerchant=1 and ref=?;';
        $params = [$ref];
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);
        $item = self::fromDbRow($row);

        return $item;
    }
}

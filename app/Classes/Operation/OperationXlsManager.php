<?php

namespace App\Classes\Operation;

use SimpleXLSX;
use voku\helper\ASCII;

class OperationXlsManager
{
    private $headerLine = 0;
    private $fullpath;
    private $xlsx;
    private $success;
    private $errors = [];
    private $hasErrors;
    private $messages = [];
    public $validation;
    public $column_labels;
    public int $changeCount = 0;
    private int $updated_rows = 0;
    public array $requiredRefCol = [
       'ref', 'reference', 'ar_ref', 'arref', 'reference produit', 'referenceproduit', 'reference willemse', 'code article', 'ref produit unitaire',
    ];
    public string $productKey;
    public array $base_column;
    public int $errorCount = 0;

    public function __construct($fullpath = null)
    {
        $this->F_ARTICLE = config('database.connections.sqlsrv.article');
        $this->fullpath = $fullpath;
        $this->success = false;
        $this->hasErrors = false;
    }

    public function check($isvp): bool
    {
        $this->success = true;
        if (filesize($this->fullpath) > 1024 * 1024) {
            $this->success = false;
            $this->errors[] = 'Erreur, le fichier XLS fait plus de 1 Mo. ';
        } else {
            if ($this->xlsx = SimpleXLSX::parse($this->fullpath)) {
                $headers = $this->getHeaders();
                $ref = array_intersect($headers, $this->requiredRefCol);
                $promo = 'taux de promo';
                if (!$isvp) {
                    $promo = array_intersect($headers, ['taux de promo']);
                }
                if (empty($ref) or empty($promo)) {
                    $this->success = false;
                    $this->errors[] = 'Il manque des colonnes obligatoire au fichier XLS';
                } else {
                    $ref = implode('', $ref);
                    $this->productKey = $ref;
                }
            } else {
                $this->success = false;
                $this->errors[] = SimpleXLSX::parseError();
            }
        }

        return $this->success;
    }

    public function getHeaders(): array
    {
        $headers = [];
        if ($this->xlsx) {
            $row1 = $this->xlsx->rows()[$this->headerLine];
            for ($i = 0; $i < count($row1); $i++) {
                $name = mb_strtolower(trim($row1[$i]));
                $name = ASCII::to_ascii(($name));
                $headers[] = $name;
            }
        }

        return $headers;
    }

    public function verifyExcel($isvp): array
    {
        $productList = [];
        if ($this->check($isvp)) {
            $headers = $this->getHeaders();
            $productRefKey = array_search($this->productKey, $headers);
            $promoKey = array_search('taux de promo', $headers);
            $multipleKey = array_search('reference produit vendue (avec multiples)', $headers);
            $design = array_search('qualite livree multiples', $headers);
            $prix_cession = array_search('prix cession propose a vp multiples', $headers);
            $date_deb = array_search('date debut', $headers);
            $date_fin = array_search('date fin', $headers);
            $comm = array_search('commentaire', $headers);
            $ref = array_intersect($this->getHeaders(), $this->requiredRefCol);
            $listRefProduit = [];
            $productList = [];
            foreach ($this->xlsx->rows() as $i => $exelRow) {
                if ($i < 1) {
                    continue;
                }
                $ref = $exelRow[$productRefKey]; //article ref database
                $ref = ref_pad($ref);

                if ($multipleKey !== false && isset($exelRow[$multipleKey])) {
                    $explode = explode('-', $exelRow[$multipleKey]);
                    $multiple = $explode[1] ?? 1;
                } else {
                    $multiple = null;
                }
                $promo = blank($exelRow[$promoKey]) ? null : (float) $exelRow[$promoKey] * 100;
                $productList[$ref] = [
                    'promo'        => $promo,
                    'multiple'     => $multiple ?? null,
                    'designation'  => $design ? $exelRow[$design] : null,
                    'prix_cession' => $prix_cession ? round($exelRow[$prix_cession], 2) : null,
                    'date_debut'   => $date_deb ? $exelRow[$date_deb] : null,
                    'date_fin'     => $date_fin ? $exelRow[$date_fin] : null,
                    'commentaire'  => $comm ? $exelRow[$comm] : null,
                ];
                $listRefProduit[] = "'$ref'";
            }
            //load articles from from db
            $sql = "select a.AR_Ref from {$this->F_ARTICLE} a   
                where  a.AR_Ref in (".implode(',', $listRefProduit).')   ;';
            $dbRows = select($sql, \PDO::FETCH_COLUMN);

            //remove non existing product
            foreach ($productList as $ref => $d) {
                if (!in_array($ref, $dbRows)) {
                    unset($productList[$ref]);
                    $this->errors[] = "La ref $ref n'existe pas en base";
                }
            }
        }

        return $productList;
    }

    public function isSuccess()
    {
        return $this->success;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->hasErrors;
    }
}

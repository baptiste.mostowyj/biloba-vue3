<?php

namespace App\Classes;

class OrdersOfDay
{
    public const MAX_DAYS = 43;

    public $ordersByDelaiDEP;
    public $total = 0;
    public $over6Count = 0;
    public $fait = 0;
    public $prepa = 0;
    public $reste = 0;

    public function __construct()
    {
        $this->ordersByDelaiDEP = new \SplFixedArray(self::MAX_DAYS);
    }

    public function addOrder($commandeEx, $displayMode)
    {
        if ($displayMode == 1) {
            $delai = min($commandeEx->delaiDEP, self::MAX_DAYS - 1);
        } else {
            $nextMonday = strtotime('next monday') - 86400 * 7;
            $nextMonday = $nextMonday - ($nextMonday % 86400);
            if ($displayMode == 3) {
                $dep = $commandeEx->date_dep_RT->getTimestamp();
            } else {
                $dep = $commandeEx->do_datelivr->getTimestamp();
            }
            $delta = $dep - $nextMonday;
            $delta = intval($delta / 86400);
            $delai = min($delta, self::MAX_DAYS - 1);
        }
        $this->total++;
        if ($commandeEx->do_type == 1) {
            $this->reste++;
        } elseif ($commandeEx->do_type == 2) {
            $this->prepa++;
        } else {
            $this->fait++;
        }

        if ($delai < 0) {
            return;
        }

        $indexDEP = $delai;

        if ($this->ordersByDelaiDEP[$indexDEP] === null) {
            $this->ordersByDelaiDEP[$indexDEP] = [];
        }

        $subArray = $this->ordersByDelaiDEP[$indexDEP];
        $subArray[] = $commandeEx;
        $this->ordersByDelaiDEP[$indexDEP] = $subArray;
    }

    public function getOrdersCountOfDelaiDEP($delaiDEP)
    {
        if ($this->ordersByDelaiDEP[$delaiDEP] === null) {
            return 0;
        }

        return count($this->ordersByDelaiDEP[$delaiDEP]);
    }

    public function updateStats()
    {
        // $this->total = 0;
        $this->over6Count = 0;
        for ($i = 0; $i < self::MAX_DAYS; $i++) {
            $c = $this->getOrdersCountOfDelaiDEP($i);
            //$this->total += $c;
            if ($i > 6) {
                $this->over6Count += $c;
            }
        }
    }
}

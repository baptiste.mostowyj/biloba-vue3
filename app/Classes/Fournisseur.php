<?php

namespace App\Classes;

class Fournisseur
{
    private static function query($code_fournisseur = null)
    {
        $where = $code_fournisseur ? "and ct.CT_Num = $code_fournisseur " : '';
        $sql = "select af.CT_Num as codeFournisseur, ct.ct_intitule as nomFournisseur
          FROM [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a
          inner join [sage].[WILLEMSE_MAQUETTE].[dbo].F_ARTFOURNISS af on af.ar_ref=a.ar_ref and af.AF_Principal=1
          inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] ct on af.CT_Num = ct.CT_Num
          where a.STATUT_PRODUIT='Commercialisé'
          and a.methode_vente<>'LDF'
          and af.AF_Principal=1
          and af.CT_Num NOT IN ('000', -- 'TEST'
                                '002', -- 'Divers'
                                '041') -- 'Hortus box'
          $where
          group by af.CT_Num,ct.ct_intitule
          order by ct.ct_intitule";
        $conn = \DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();

        return $statement;
    }

    public static function findFournisseur($code_fournisseur)
    {
        $statement = self::query($code_fournisseur);

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function getAllFournisseurs()
    {
        $statement = self::query();

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public static function getAllFournisseursId()
    {
        $statement = self::query();

        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }
}

<?php

namespace App\Classes;

use DB;
use Storage;
use voku\helper\ASCII;

class Helper
{
    public static function getSeoScore($descritpionLength, $imagesCount)
    {
        $score = min(4, intval(ceil($descritpionLength / 400)));
        if ($imagesCount > 1) {
            $score += 1;
        }

        return $score;
    }

    public static function getFournisseurs()
    {
        $sql = "select af.CT_Num as codeFournisseur,ct.ct_intitule as nomFournisseur
      FROM [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a
      inner join [sage].[WILLEMSE_MAQUETTE].[dbo].F_ARTFOURNISS af on af.ar_ref=a.ar_ref and af.AF_Principal=1
      inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] ct on af.CT_Num = ct.CT_Num
      where a.STATUT_PRODUIT='Commercialisé'
      and a.methode_vente<>'LDF'
      and af.AF_Principal=1
      and af.CT_Num NOT IN ('000', -- 'TEST'
                            '002', -- 'Divers'
                            '041') -- 'Hortus box'
      group by af.CT_Num,ct.ct_intitule
      order by ct.ct_intitule";

        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    public static function deleteOldFiles($directory, $days = 15)
    {
        $files = Storage::allFiles($directory);
        $now = time();
        foreach ($files as $file) {
            $file_path = storage_path('app').'/'.$file;
            if (is_file($file_path)) {
                if ($now - filemtime($file_path) >= 86400 * $days) {
                    Storage::delete($file);
                }
            }
        }
    }

    public static function simplifyString($string)
    {
        $string = ASCII::to_ascii(($string)); //supprime acent
        $string = mb_strtolower($string);
        $string = preg_replace('/[^a-z0-9]/', '', $string); //supprime tout non alpha (espace, tiret..)

        return $string;
    }

    /**
     * Explode a string, then trim, sort, filtre doublon, and remove null element.
     */
    public static function explode($value, string $separator = ';', $sort = true): array
    {
        $array = explode($separator, $value);
        $array = array_map(function ($value) {
            return trim($value, " \t\n\r\0\x0B\xC2\xA0");
        }, $array);
        if ($sort) {
            sort($array);
        }

        return array_filter(array_unique($array));
    }

    public static function array_flatten(array $array)
    {
        $return = [];
        array_walk_recursive($array, function ($a) use (&$return) {
            $return[] = $a;
        });

        return $return;
    }
}

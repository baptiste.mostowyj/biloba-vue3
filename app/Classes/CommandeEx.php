<?php

namespace App\Classes;

use DB;

class CommandeEx
{
    public $do_piece;
    public $do_piece_orig;
    public $do_type;
    public $do_domaine;
    public $do_date;
    public $do_date_int;
    public $do_datelivr;
    public $do_datelivr2;
    public $ca_num;
    public $delaiDEP;
    public $tout_en_cours_livrabilite;
    public $ldf;
    public $multicolis;
    public $date_dep_RT;

    public static function getOrdersFromDayAndDelaiDep(\DateTime $day, $delaiDep)
    {
        $conn = DB::connection()->getPdo();

        $timestampStart = $day->getTimestamp();
        $timestampStart = $timestampStart - ($timestampStart % 86400);
        $timestampEnd = $timestampStart + 86399;

        $sql = 'SELECT * from sampi.liste_catalogue_erp.dbo.CommandeEx where delaiDEP=? and do_date between ? and ?';
        $params = [$delaiDep, date('Y-m-d', $timestampStart), date('Y-m-d', $timestampEnd)];

        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $orders = [];
        foreach ($rows as $row) {
            $order = self::fromDbRow($row);
            $orders[] = $order;
        }

        return $orders;
    }

    public static function getOrdersFromDayAndDepDate(\DateTime $day, \DateTime $dep_date)
    {
        $conn = DB::connection()->getPdo();

        $timestampStart = $day->getTimestamp();
        $timestampStart = $timestampStart - ($timestampStart % 86400);
        $timestampEnd = $timestampStart + 86399;

        $timestampDep = $dep_date->getTimestamp();
        $timestampDep = $timestampDep - ($timestampDep % 86400);

        $sql = 'SELECT * from sampi.liste_catalogue_erp.dbo.CommandeEx where do_date between ? and ? and date_dep_RT = ?';
        $params = [date('Y-m-d', $timestampStart), date('Y-m-d', $timestampEnd), date('Y-m-d', $timestampDep)];

        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $orders = [];
        foreach ($rows as $row) {
            $order = self::fromDbRow($row);
            $orders[] = $order;
        }

        return $orders;
    }

    public static function fromDbRow($row)
    {
        $cde = new self();
        $cde->do_piece = $row['do_piece'];
        $cde->do_piece_orig = $row['do_piece_orig'];
        $cde->do_type = intval($row['do_type']);
        $cde->do_domaine = intval($row['do_domaine']);
        $cde->do_date = \DateTime::createFromFormat('Y-m-d', $row['do_date']);
        $cde->do_date_int = intval($row['do_date_int']);
        $cde->do_datelivr = \DateTime::createFromFormat('Y-m-d', $row['do_datelivr']);
        $cde->do_datelivr2 = \DateTime::createFromFormat('Y-m-d', $row['do_datelivr2']);
        $cde->ca_num = $row['ca_num'];
        $cde->delaiDEP = intval($row['delaiDEP']);
        $cde->tout_en_cours_livrabilite = intval($row['tout_en_cours_livrabilite']);
        $cde->ldf = intval($row['ldf']);
        $cde->multicolis = intval($row['multicolis']);
        $cde->date_dep_RT = \DateTime::createFromFormat('Y-m-d', $row['date_dep_RT']);

        return $cde;
    }

    public static function getRoudedDateFromTimestamp($timestamp)
    {
        $ts = $timestamp - ($timestamp % 86400);
        $d = new \DateTime("@$ts");

        return $d;
    }
}

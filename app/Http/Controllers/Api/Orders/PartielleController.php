<?php

namespace App\Http\Controllers\Api\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\WillemseData;

class PartielleController extends Controller
{
    protected $sqldate;
    protected $willemseData;
    protected $willemseDataDay;
    protected $willemseDataP;
    protected $willemseDataDayP;
    protected $willemseDataMono;
    protected $queryString = [
        'do_type' => ['except' => 1],
    ];

    private function prepareWillemseDate()
    {
        $sqlB = 'SELECT [date_MAJ] FROM sampi.liste_catalogue_erp.[dbo].[DEP_date];';
        $this->sqldate = new WillemseData($sqlB, null, null);
    }

    private function prepareWillemseData()
    {
        $sql = 'SELECT TOP (1000) [semaine] as DO_DateLivr
        ,[annee]
        ,[monoWeb]
        ,[Web]
        ,[monoBK]
        ,[BK]
        ,[monoCata]
        ,[Catalogue]
        ,[monoVP]
        ,[VP]
        ,[monoAutres]
        ,[Autres]
        ,[LDF]
        ,[TotalCom]
        ,[WebP]
        ,BKP
        ,[CatalogueP]
        ,[VPP]
        ,[AutresP]
        ,[LDFP]
        ,[TotalComP]
        ,[nbRefmonoWeb]
        ,[nbRefWeb]
        ,[nbRefmonoBK]
        ,[nbRefBK]
        ,[nbRefmonoCata]
        ,[nbRefCatalogue]
        ,[nbRefmonoVP]
        ,[nbRefVP]
        ,[nbRefmonoAutres]
        ,[nbRefAutres]
        ,[nbRefLDF]
        ,[nbRefTotalCom]
        ,[nbRefWebP]
        ,[nbRefCatalogueP]
        ,[nbRefVPP]
        ,[nbRefAutresP]
        ,[nbRefLDFP]
        ,nbRefBKP
        ,[nbRefTotalComP]
        FROM [INTRANET].[dbo].[DEP_SEMAINE]
      ';

        $this->willemseData = new WillemseData($sql, null, null);
    }

    private function prepareWillemseDataDay()
    {
        $sqlB = 'SELECT TOP (1000) [semaine]
            ,[DO_DateLivr]
            ,[monoWeb]
        ,[Web]
        ,[monoBK]
        ,[BK]
        ,[monoCata]
        ,[Catalogue]
        ,[monoVP]
        ,[VP]
        ,[monoAutres]
        ,[Autres]
        ,[LDF]
        ,[TotalCom]
        ,[WebP]
        ,BKP
        ,[CatalogueP]
        ,[VPP]
        ,[AutresP]
        ,[LDFP]
        ,[TotalComP]
        ,[nbRefmonoWeb]
        ,[nbRefWeb]
        ,[nbRefmonoBK]
        ,[nbRefBK]
        ,[nbRefmonoCata]
        ,[nbRefCatalogue]
        ,[nbRefmonoVP]
        ,[nbRefVP]
        ,[nbRefmonoAutres]
        ,[nbRefAutres]
        ,[nbRefLDF]
        ,[nbRefTotalCom]
        ,[nbRefWebP]
        ,[nbRefCatalogueP]
        ,[nbRefVPP]
        ,[nbRefAutresP]
        ,[nbRefLDFP]
        ,nbRefBKP
        ,[nbRefTotalComP]
        FROM [INTRANET].[dbo].[DEP_JOUR]';
        $this->willemseDataDay = new WillemseData($sqlB, null, null);
    }

    private function prepareWillemseDataP()
    {
        $sql = 'SELECT TOP (1000) [semaine] as DO_DateLivr
        ,[annee]
        ,[monoWeb]
        ,[Web]
        ,[monoBK]
        ,[BK]
        ,[monoCata]
        ,[Catalogue]
        ,[monoVP]
        ,[VP]
        ,[monoAutres]
        ,[Autres]
        ,[LDF]
        ,[TotalCom]
        ,[WebP]
        ,BKP
        ,[CatalogueP]
        ,[VPP]
        ,[AutresP]
        ,[LDFP]
        ,[TotalComP]
        ,[nbRefmonoWeb]
        ,[nbRefWeb]
        ,[nbRefmonoBK]
        ,[nbRefBK]
        ,[nbRefmonoCata]
        ,[nbRefCatalogue]
        ,[nbRefmonoVP]
        ,[nbRefVP]
        ,[nbRefmonoAutres]
        ,[nbRefAutres]
        ,[nbRefLDF]
        ,[nbRefTotalCom]
        ,[nbRefWebP]
        ,[nbRefCatalogueP]
        ,[nbRefVPP]
        ,[nbRefAutresP]
        ,[nbRefLDFP]
        ,nbRefBKP
        ,[nbRefTotalComP]
        FROM [INTRANET].[dbo].[DEP_SEMAINE_2]     
      ';

        $this->willemseDataP = new WillemseData($sql, null, null);
    }

    private function prepareWillemseDataDayP()
    {
        $sqlB = 'SELECT TOP (1000) [semaine]
        ,[DO_DateLivr]
        ,[monoWeb]
        ,[Web]
        ,[monoBK]
        ,[BK]
        ,[monoCata]
        ,[Catalogue]
        ,[monoVP]
        ,[VP]
        ,[monoAutres]
        ,[Autres]
        ,[LDF]
        ,[TotalCom]
        ,[WebP]
        ,BKP
        ,[CatalogueP]
        ,[VPP]
        ,[AutresP]
        ,[LDFP]
        ,[TotalComP]
        ,[nbRefmonoWeb]
        ,[nbRefWeb]
        ,[nbRefmonoBK]
        ,[nbRefBK]
        ,[nbRefmonoCata]
        ,[nbRefCatalogue]
        ,[nbRefmonoVP]
        ,[nbRefVP]
        ,[nbRefmonoAutres]
        ,[nbRefAutres]
        ,[nbRefLDF]
        ,[nbRefTotalCom]
        ,[nbRefWebP]
        ,[nbRefCatalogueP]
        ,[nbRefVPP]
        ,[nbRefAutresP]
        ,[nbRefLDFP]
        ,nbRefBKP
        ,[nbRefTotalComP]
        FROM [INTRANET].[dbo].[DEP_JOUR_2]';
        $this->willemseDataDayP = new WillemseData($sqlB, null, null);
    }

    public function index(Request $request,$dotype)
    {
        $dotype = (int) $dotype;
        $rows = [];
        if ($dotype === 1) {
            $this->prepareWillemseData();
            $this->prepareWillemseDataDay();
            $rows = $this->willemseData->getRows();
            $days = $this->willemseDataDay->getRows();
        }
        if ($dotype === 2) {
            $this->prepareWillemseDataP();
            $this->prepareWillemseDataDayP();
            $rows = $this->willemseDataP->getRows();
            $days = $this->willemseDataDayP->getRows();
        }
        foreach($rows as $key=>$r){
            $child = [];
            foreach($days as $d){
                if($d['semaine']==$r['DO_DateLivr'] && date('Y', strtotime($d['DO_DateLivr'])) == $r['annee']){
                    $date=date_create($d["DO_DateLivr"] );
                    $d["DO_DateLivr"] = date_format($date,"d/m/Y");
                    $d["annee"] = date_format($date,"Y");
                    array_push($child,$d);
                }
            }
            $rows[$key]['child'] = $child;
        }
        
        return response()->json($rows);
    }

}

<?php

namespace App\Http\Controllers\API\Operation;

use App\Http\Controllers\Controller;
use App\Classes\WillemseData;
use Illuminate\Http\Request;
use App\Models\Operation as OperationModel;
use Carbon\Carbon;
use DB;

class Operation extends Controller
{
    private function getOpes(){

        $sql = "select o.*,count(p.ref) as nb_refs
        from [liste_catalogue_erp].[dbo].[operations] o
        left join liste_catalogue_erp.[dbo].operation_produits p on id = p.operation_id 
        WHERE (o.date_debut between '2023-01-01' and '2023-06-01' OR o.date_fin between '2023-01-01' and '2023-06-01')
        group by o.id, o.nom, o.type, o.date_debut, o.date_fin,shop
        Order by  o.date_debut, o.date_fin Desc";

        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
            
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opes = $this->getOpes();
        $hp1 = '#ccb2d4';
        $hp2 = '#c83df2';
        $hp3 = '#78039c';
        $colors = ['VF' => '#19b1f7',
        'HP1'           => $hp1,
        'HP2'           => $hp2,
        'HP3'           => $hp3,
        'BONNE AFFAIRE' => '#f03f13', ];
        $chart = [];
        foreach($opes as &$op){
            $color = $colors[strtoupper($op['type'])] ?? '#69b145';

            $chart[] = [
                'category' => "{$op['type']} {$op['nom']} {$op['date_debut']} {$op['date_fin']}",
                'startV'   => $op['date_debut'],
                'endV'     => (new \Carbon\Carbon($op['date_fin']))->addDay()->format('Y-m-d'),
                'color'    => $color,
                'type'     => $op['nom'] ? "{$op['nom']}  ({$op['type']})" : $op['type'],
                'deb'      => date_fr($op['date_debut']),
                'fin'      => date_fr($op['date_fin']),
            ];
            $op["date_debut"] = Carbon::parse($op['date_debut'])->format('d/m/Y');
            $op["date_fin"] = Carbon::parse($op['date_fin'])->format('d/m/Y');

        }

        return response()->json([$opes,$chart]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\WillemseData;


class Product extends Controller
{
    protected $willemseData;
    private function prepareWillemseData($ref)
    {
        $sql = "SELECT [refart]
                ,a.[name]
                ,[statut]
                ,[nomencl]
                ,[fournisseur]
                ,[delai_fournisseur]
                ,[methode_vente]
                ,[ldf]
                ,[en_sommeil]
                ,[nom_fournisseur]
                ,[categorie_web]
                ,i.[url]
                ,i.[coverImage]
                ,i.[descriptionLength]
                ,i.[imagesCount]
                ,[dispo_fournisseur]
                ,[INTITULE_LATIN]
                ,[cbCreation]
                ,[BLOCAGE_VENTE_WEB]
            FROM [INTRANET].[dbo].[Articles] a with(nolock)
            LEFT JOIN [SCRAPER].[dbo].[item] i
            on a.refart = i.ref
            where refart='$ref'";

        $this->willemseData = new WillemseData($sql, null, null);
    }

    public function index()
    {
        $ref = request('ref');
        $this->prepareWillemseData($ref);
        $rows = $this->willemseData->getRows();

        return response()->json($rows[0]);
    }
}

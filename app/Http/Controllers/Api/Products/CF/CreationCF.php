<?php

namespace App\Http\Controllers\API\Products\CF;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\WillemseData;
use App\Classes\Product\Cf;
use App\Classes\Product\CfXlsChecker;
use App\Classes\Helper;
use Config;
use DB;
use Http;
use Illuminate\Support\Str;
use Mail;
use Storage;


class CreationCF extends Controller
{

    
    private function getFournisseurs()
    {
        $sql = "set nocount on;
            IF object_id('tempdb..#four_q','U') IS NOT NULL DROP TABLE #four_q

            select CT_Num as codeFournisseur,CT_Intitule as nomFournisseur,CAST(0 as int) as q 
            INTO #four_q
            from liste_catalogue_erp.[dbo].StockEx  group by CT_Num, CT_Intitule order by CT_Intitule;
            
            UPDATE ex SET q = ex2.q
            FROM #four_q ex 
            INNER JOIN (
            select CT_Num,count(s.refart) as q from liste_catalogue_erp.[dbo].StockEx s
            left join liste_catalogue_erp.dbo.ArticleEx ex on ex.refart = s.refart
            where sugg > 0 
            and deb_commandabilite IS NOT NULL AND GETDATE() >= DATEADD(DAY, -7, deb_commandabilite)
            and fin_commandabilite IS NOT NULL AND GETDATE() <= DATEADD(DAY, 7, ex.fin_cur)
            group by CT_Num
            ) as ex2 on ex2.CT_Num = ex.codeFournisseur
            
            SELECT * from #four_q";

        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }

    private function getSuggestionCommande($codeFournisseur, $sugg){
        $today = now()->format('Y-m-d');
        $sql = "SET NOCOUNT ON ;
            SELECT  se.refart as refart,
            ex.dispo_fournisseur as dispo_fournisseur, 
            a.AR_Design as nom, 
            a.STATUT_PRODUIT as statut, 
            a.EMPLACEMENT_PICKING as depotStock,
            i.descriptionLength, 
            i.url,
            i.price,
            i.imagesCount,
            af.AF_PrixAch, 
            af.AF_Unite,
            a.INTITULE_LATIN, 
            se.NB_PIECES_PAQUET as NB_PIECES_PAQUET, 
            af.AF_RefFourniss, 
            se.METHODE_VENTE, 
            se.CT_Num, 
            se.CT_Intitule, 
            se.PCB, 
            se.Q_MIN, 
            COALESCE(CAST(s.AS_QteSto - s.AS_QtePrepa as int), 0) stockSage, 
            COALESCE(CAST(s.AS_QteRes as INT), 0) reserveSage, 
            COALESCE(CAST(s.AS_QteCom as INT), 0) commandéSage,  
            se.stock_terme as stockTermeSage, 
            se.qte_lastweek2 as ventesLastWeek2, 
            se.qte_lastweek as ventesLastWeek, 
            se.coef_progression as coef_progression,
            lastOPC,
            ( select TOP 1 operation_id from liste_catalogue_erp.[dbo].operation_produits where ref = se.refart and date_fin >= '$today' order by date_fin ASC ) as last_opc_new,
            lastOPC_type,
            lastOPC_debut,
            lastOPC_fin,
            ( select TOP 1  operation_id from liste_catalogue_erp.[dbo].operation_produits where ref = se.refart and date_debut >= '$today' order by date_debut ASC ) as next_opc_new,
            nextOPC,
            nextOPC_type,
            nextOPC_debut,
            nextOPC_fin,
            se.demandeDateLivr as datelivr, 
            se.sugg as suggestionQte, 
            se.suggFour as suggestionQteFour, 
            se.standbye,
            se.delai_appro,
            ex.deb_cur,
            ex.fin_cur,
            CAST( CASE WHEN ex.deb_cur > GETDATE() THEN 0 ELSE 1 END as bit ) as debliv_ok,
            CAST( CASE WHEN ex.fin_cur < GETDATE() THEN 0 ELSE 1 END as bit ) as finliv_ok,
            CAST( CASE WHEN deb_commandabilite > GETDATE() THEN 0 ELSE 1 END as bit ) as debappro_ok,
            CAST( CASE WHEN fin_commandabilite > GETDATE() THEN 1 ELSE 0 END as bit ) as finappro_ok,
            CAST( CASE WHEN DATEADD(DAY, -7, fin_commandabilite) > GETDATE() THEN 1 ELSE 0 END as bit ) as finappro_warning,
            achats_saison,
            prev_saison,
            fb.Fournisseurs,
            a.rotation
            
            FROM  liste_catalogue_erp.[dbo].StockEx se
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=se.refart
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].F_ARTFOURNISS af on af.ar_ref=a.ar_ref
            left join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTSTOCK] s on s.ar_ref=a.ar_ref
            left join liste_catalogue_erp.dbo.ArticleEx ex on ex.refart = a.ar_ref
            left join [SCRAPER].[dbo].[item] i on i.[ref] = a.ar_ref
            Inner join (SELECT Main.AR_Ref,
                LEFT(Main.Fournisseurs,Len(Main.Fournisseurs)-1) As Fournisseurs
                FROM
                (
                    SELECT DISTINCT Fourn2.AR_Ref, 
                        (
                            SELECT Fourn1.CT_Num + ',' AS [text()]
                            FROM [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTFOURNISS] Fourn1
                            WHERE Fourn1.AR_Ref = Fourn2.AR_Ref
                            ORDER BY Fourn1.AR_Ref
                            FOR XML PATH ('')
                        ) [Fournisseurs]
                    FROM [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTFOURNISS] Fourn2
                ) Main) fb 	on fb.AR_Ref =se.refart
            WHERE se.CT_Num = '".$codeFournisseur."' ".$sugg.'
            and coalesce(s.DE_No,1)=1  and af.AF_Principal=1
            and deb_commandabilite IS NOT NULL AND GETDATE() >= DATEADD(DAY, -7, deb_commandabilite)
            and fin_commandabilite IS NOT NULL AND GETDATE() <= DATEADD(DAY, 7, fin_cur)
            order by refart;';

        $this->willemseData = new WillemseData($sql, null, null);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fournisseurs = $this->getFournisseurs();
        return response()->json($fournisseurs);
    }




    /**
     * Display the specified resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$fournisseur)
    {
       
        if (filter_var($request->get('sugg'), FILTER_VALIDATE_BOOLEAN)) {
            $diplayAll = '';
            dd(filter_var($request->get('sugg'), FILTER_VALIDATE_BOOLEAN));
        } else {
            $diplayAll = 'and sugg > 0';
        }
        $this->getSuggestionCommande($fournisseur, $diplayAll);
        $rows = $this->willemseData->getRows();
        $refCount = 0;
        $articlesCount = 0;
        $refCountIndispo = 0;
        $articlesCountIndispo = 0;
        $totalPvTTC = 0;
        $totalPaHT = 0;
        foreach ($rows as &$row) {
            $debliv = \DateTime::createFromFormat('Y-m-j', $row['deb_cur']);
            $finliv = \DateTime::createFromFormat('Y-m-j', $row['fin_cur']);
            if ($debliv && $finliv) {
                $row['debliv'] = $debliv->format('d/m');
                $row['finliv'] = $finliv->format('d/m');
            } else {
                $row['debliv'] = '';
                $row['finliv'] = '';
            }
            
            $datelivr = \DateTime::createFromFormat('Y-m-j', $row['datelivr']);
            $row['datelivrJour'] = $datelivr->format('D d/m/y');
            $row['datelivrJour'] = str_replace('Mon', 'Lun', $row['datelivrJour']);
            $row['datelivrJour'] = str_replace('Tue', 'Mar', $row['datelivrJour']);
            $row['datelivrJour'] = str_replace('Wed', 'Mer', $row['datelivrJour']);
            $row['datelivrJour'] = str_replace('Thu', 'Jeu', $row['datelivrJour']);
            $row['datelivrJour'] = str_replace('Fri', 'Ven', $row['datelivrJour']);
            $row['seostars'] = Helper::getSeoScore(intval($row['descriptionLength']), intval($row['imagesCount']));
            $refart = $row['refart'];
           // $row['productlink'] = route('product', [$refart]);
            
            if (intval($row['dispo_fournisseur']) == 1) {
                $refCount++;
                $articlesCount += intval($row['suggestionQteFour']);
                $totalPvTTC += $row['suggestionQte'] * $row['price'];
                $totalPaHT += $row['suggestionQte'] * $row['AF_PrixAch'] * $row['NB_PIECES_PAQUET'] / $row['AF_Unite'];
            } else {
                $refCountIndispo++;
                $articlesCountIndispo += intval($row['suggestionQteFour']);
            }
            $row['RefVDS'] = 'lot'.trim($row['NB_PIECES_PAQUET']).'_'.$row['refart'];
            $row['coef_progression'] = number_format($row['coef_progression'], 1);
        }
        $totalPaHT = number_format($totalPaHT, 2);
        $totalPvTTC = number_format($totalPvTTC, 2);
        
        return response()->json( ['rows' => $rows, 'refCount' => $refCount, 'articlesCount' => $articlesCount, 'refCountIndispo' => $refCountIndispo, 'articlesCountIndispo' => $articlesCountIndispo, 'totalPaHT' => $totalPaHT, 'totalPvTTC' => $totalPvTTC]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

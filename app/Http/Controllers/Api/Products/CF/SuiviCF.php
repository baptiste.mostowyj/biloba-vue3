<?php

namespace App\Http\Controllers\API\Products\CF;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\WillemseData;
use App\Classes\Product\Cf;
use App\Classes\Product\CfXlsChecker;
use App\Classes\Helper;
use Config;
use DB;
use Http;
use Illuminate\Support\Str;
use Mail;
use Storage;

class SuiviCF extends Controller
{

    protected $cas_part = ['033', '014'];
    private function getAllCfs()
    {
        $cf = new CF();
        $CFs = $cf->getAllCFs();
        foreach ($CFs as &$cf) {
            if ($cf['CA_Num'] == '' and in_array($cf['do_tiers'], $this->cas_part)) {
                $cf['CA_Num'] = 'lot '.$this->getLotGuido($cf['do_piece'])['nb_pieces_paquet'];
            }

            $cf['depot'] = 'WILLEMSE';
            if ($cf['de_no'] == 2) {
                $cf['depot'] = 'ANNULATION';
            }
            if ($cf['de_no'] == 3) {
                $cf['depot'] = 'VEEPEE';
            }
            if ($cf['de_no'] == 4) {
                $cf['depot'] = 'VEEPEE-WIL';
            }

            // Cas des CFs créées directement dans Sage. Génère la ligne manquante dans la table [INTRANET].[dbo].[MailsCF]
            if (empty($cf['m_do_piece'])) {
                $this->upsertEmailCfLine($cf['do_piece'], 0);
            }
        }

        return $CFs;
    }
    private function getLotGuido($do_piece)
    {
        $sql = 'select top 1 ct.ct_intitule,nb_pieces_paquet,e.CA_Num
            from [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCENTETE] e
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_DOCLIGNE] l on e.do_domaine=l.do_domaine and e.do_piece=l.do_piece and l.ar_ref is not null
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_COMPTET] ct on ct.CT_Num = e.do_tiers
            inner join [sage].[WILLEMSE_MAQUETTE].[dbo].[F_ARTICLE] a on a.ar_ref=l.ar_ref
            where e.do_domaine=1
            and e.do_piece=?';

        $params = [$do_piece];
        $conn = DB::connection()->getPdo();
        $statement = $conn->prepare($sql);
        $statement->execute($params);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        return $row;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $CFs = $this->getAllCfs();
        return response()->json($CFs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

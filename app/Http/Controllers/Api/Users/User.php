<?php

namespace App\Http\Controllers\API\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\User as UserM;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;



class User extends Controller
{

    private function getUsers(){

        $usersM = UserM::all();
        $users = [];
        foreach($usersM as $key=>$user)
        {
            $users[$key] = $user->toArray();
            $roles = [];
            foreach(\App\Models\User::$roles as $role){
                if($user->getRoleNames()->contains($role)) {
                    $roles[$role] = 1;
                }
                else{
                    $roles[$role] = 0;
                }
            }
            $users[$key] = array_merge($users[$key],["roles" => $roles]);
        }
        return $users;
            
    }

        /**
     * Return user if exists; create and return if doesn't.
     *
     * @param $user
     * @return User
     */
    private function findOrCreateUser($user): User
    {
        if (UserM::where('email', $user['mail'])->first()) {
            $user = UserM::where('email',  $user->mail)
            ->update([
                'updated_at' =>  new DateTime(), 
            ]);
            return $user;
        }
       
        return UserM::create([
            'name'          =>$user['name'],
            'email'         => $user['mail'],
            'microsoft_id'  => $user['id'],
            'updated_at'    => new DateTime(),
            'password'      => bcrypt(\Str::random(7)),
        ]);

        
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = [];
        try{
            $users = $this->getUsers();
        }catch (Exception $e) {
            return response(400)->json($e);
        }
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $user = $request->get('user');
            $this->findOrCreateUser($user);
            return response()->json($user);
        }catch (Exception $e) {
            return response(400)->json($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $role = $request->get('role');
            $user_id = $id;
            $checked = $request->get('checked');
            $rules = ['role' => Rule::in(UserM::$roles), 'user_id' => 'exists:users,id', $checked => 'in:true,false'];
            $request->validate($rules);
            $user = UserM::find($user_id);
            if ($checked == 'true') {
                $user->assignRole($role);
            } else {
                $user->removeRole($role);
            }
        }catch (Exception $e) {
            return response(400)->json($e);
        }
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            User::find($id)->delete();
        }catch (Exception $e) {
            return response(400)->json($e);
        }
        return response()->json();
    }
}

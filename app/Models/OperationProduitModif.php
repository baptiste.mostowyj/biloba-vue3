<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationProduitModif extends Model
{
    use HasFactory;

    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
    protected $table = 'liste_catalogue_erp.dbo.operation_produits_modif';
    protected $fillable = ['nom', 'type', 'promo', 'date_debut', 'date_fin', 'ref',
                           'vp_designation', 'vp_multiple', 'vp_prix_cession', 'updated_at', ];
    protected $dates = ['date_debut', 'date_fin'];
}

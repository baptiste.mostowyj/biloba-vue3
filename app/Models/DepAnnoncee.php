<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepAnnoncee extends Model
{
    protected $table = 'sampi.liste_catalogue_erp.dbo.CommandeEx';
    protected $primaryKey = 'do_piece';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * Indicates if the model should be timestamped with created_at and updated_at table fields.
     *
     * @var bool
     */
    public $timestamps = false;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'do_piece',
        'do_piece_orig',
        'do_type',
        'do_domaine',
        'do_date',
        'do_date_int',
        'do_datelivr',
        'do_datelivr2',
        'ca_num',
        'delaiDEP',
        'tout_en_cours_livrabilite',
        'ldf',
        'multicolis',
        'date_dep_RT',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'do_date'      => 'datetime',
        'do_datelivr'  => 'datetime',
        'do_datelivr2' => 'datetime',
        'date_dep_RT'  => 'datetime',
    ];
}

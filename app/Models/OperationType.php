<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationType extends Model
{
    use HasFactory;

    protected $table = 'liste_catalogue_erp.dbo.operation_type';
    protected $fillable = ['name', 'is_vp'];
    protected $dateFormat = 'd-m-Y H:i:s.000';
    protected $casts = ['is_vp' => 'boolean'];
}

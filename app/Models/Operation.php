<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'type', 'date_debut', 'date_fin', 'shop'];
    public $timestamps = false;
    protected $table = 'liste_catalogue_erp.dbo.operations';
    protected $dates = ['date_debut', 'date_fin'];

    public function produits()
    {
        return $this->hasMany(OperationProduit::class, 'operation_id');
    }

    public function produitsModif()
    {
        return $this->hasMany(OperationProduitModif::class, 'operation_id');
    }

    public function OperationType()
    {
        return $this->hasOne(OperationType::class, 'name', 'type');
    }
}

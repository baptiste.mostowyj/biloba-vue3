<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductExcel extends Model
{
    protected $table = 'intranet.dbo.ProduitExcel';
    protected $primaryKey = null;
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped with created_at and updated_at table fields.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'excel_column',
        'db_column',
    ];
}

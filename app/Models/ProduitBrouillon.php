<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProduitBrouillon extends Model
{
    use HasFactory;

    protected $table = 'produit_brouillon';
    protected $dateFormat = 'd-m-Y H:i:s.000';
    protected $dates = ['date_application'];
    protected $fillable = [
        'ref',
        'designation',
        'intitule_latin',
        'qualite_livre',
        'date_application',
    ];
}

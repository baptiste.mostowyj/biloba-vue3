<?php

namespace App\Models;

use App\Classes\Product\Product;
use Illuminate\Database\Eloquent\Model;

class ProductRollback extends Model
{
    protected $table = 'intranet.dbo.ProductRollback_DEV';
    protected $primaryKey = 'save_id';
    public $timestamps = false;
    public $dates = ['created_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref',
        'col',
        'value_before',
        'value_after',
        'filename',
        'rolled_back',
        'user_id',
        'created_at',
    ];

    public function getCleanValueBeforeAttribute()
    {
        return Product::transformDbAttribute($this->col, $this->value_before);
    }

    public function getCleanValueAfterAttribute()
    {
        return Product::transformDbAttribute($this->col, $this->value_after);
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['clean_value_before'] = $this->clean_value_before;
        $array['clean_value_after'] = $this->clean_value_after;

        return $array;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (\App::environment('production')) {
            $this->table = 'intranet.dbo.ProductRollback';
        }
    }
}

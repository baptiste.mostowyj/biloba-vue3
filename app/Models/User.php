<?php

namespace App\Models;

use App\Classes\Fournisseur;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    //available role
    public static $roles = ['admin', 'supplier', 'willemse'];
    protected $dateFormat = 'd-m-Y H:i:s.000';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'microsoft_id',
        'fournisseur_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isFournisseur()
    {
        return $this->fournisseur_id != null;
    }

    public function fournisseur()
    {
        if (session('supplier')) {
            $fournisseur = session('supplier');
        } elseif ($this->fournisseur_id) {
            $fournisseur = $this->fournisseur_id;
        } else {
            return null;
        }

        $fournisseur = Fournisseur::findFournisseur($fournisseur);

        return $fournisseur[0] ?? null;
    }

    public function getFournisseurAttribute()
    {
        if (session('supplier')) {
            $fournisseur = session('supplier');
        } elseif ($this->fournisseur_id) {
            $fournisseur = $this->fournisseur_id;
        } else {
            return null;
        }

        $fournisseur = Fournisseur::findFournisseur($fournisseur);

        return isset($fournisseur[0]) ? $fournisseur[0]['nomFournisseur'] : null;
    }
}

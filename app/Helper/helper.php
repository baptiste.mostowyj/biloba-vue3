<?php

use Carbon\Carbon;

function select(string $sql, $fetch_mode = \PDO::FETCH_ASSOC, $connection = null)
{
    $conn = DB::connection($connection)->getPdo();
    $statement = $conn->prepare($sql);
    $statement->execute();

    return $statement->fetchAll($fetch_mode);
}

function execute(string $sql)
{
    $conn = DB::connection()->getPdo();
    $conn->exec($sql);
}

function quote($s)
{
    return (DB::connection()->getPdo())->quote($s);
}

function quotes($s)
{
    return str_replace("'", "''", $s);
}

function d($var, ...$moreVars)
{
    return dump($var, ...$moreVars);
}

function db_startlog()
{
    \DB::enableQueryLog();
}

function db_getlog()
{
    dd(\DB::getQueryLog());
}

function switch_database($database)
{
    \Config::set('database.connections.sqlsrv.database', $database);
    \DB::purge('sqlsrv');
}

/**
 * Vérifie qu'un liste contient une chaine, ex :
 * lundi,mardi,mercredi contient 'mardi'
 * La liste passe en array avec explode.
 */
function arr_str_contains($needle, $haystack, $separator = ',')
{
    return in_array($needle, explode($separator, $haystack));
}

/**
 * Ajoute les zeros sur les refs : 383 => 000383.
 * @param string $ref
 * @return string
 */
function ref_pad(string $ref): string
{
    return str_pad($ref, 6, '0', STR_PAD_LEFT);
}

/**
 * Converti une date.
 * @param string $date
 * @return string
 */
function date_convert(string $date, $output_format = 'd/m/Y', $input_format = 'Y-m-d'): string
{
    return Carbon::createFromFormat($input_format, $date)->format($output_format);
}

/**
 * Converti date 2021-09-21 => 21/09/2021  pour affichage front..
 * @param string $date
 * @return string
 */
function date_fr(string $date, $output_format = 'd/m/Y', $input_format = 'Y-m-d'): string
{
    return date_convert($date, $output_format, $input_format);
}

/**
 * Format sql willemse.
 */
const date_willemse = 'Y-d-m H:i:s.v';
const date_willemse_sql = 'Y-m-d H:i:s.v';
const date_fr = 'd/m/Y';
const datetime_fr = 'd/m/Y à H:i';

/**
 * Converti une date au format SQL Willemse 2021-09-31 => 2021-31-09.
 * @param string $date
 * @return string
 */
function date_sql(string $date, $output_format = 'Y-d-m', $input_format = 'Y-m-d'): string
{
    return date_convert($date, $output_format, $input_format);
}

/**
 * Return true si on est en prod.
 * @return bool|string
 */
function prod()
{
    return \App::environment('production');
}

/**
 * Background exec cmd in the app path, don't wait for the end of execution.
 */
function bg_exec(string $cmd)
{
    $path = base_path();
    exec("cd $path && $cmd > /dev/null 2>&1 &");
}

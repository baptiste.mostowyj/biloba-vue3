var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
import "bootstrap";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faDownload, faCartShopping, faBox, faSeedling, faReceipt, faTruckField, faCalendarDays, faUserShield, faWarehouse, faHeadset, faScrewdriverWrench, faTableColumns, faClipboardCheck, faMagnifyingGlass, faCircleUser, faBoxesStacked, faBug, faTruckRampBox, faTrashCan, faPenToSquare, faRightFromBracket, faPalette, faHandsClapping, faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { toRefs, ref, reactive, computed, provide, inject, getCurrentInstance, watch, useSSRContext, resolveComponent, mergeProps, withCtx, createVNode, renderSlot, resolveDynamicComponent, createTextVNode, toDisplayString, openBlock, createBlock, Transition, createCommentVNode, onMounted, onUnmounted, nextTick, Fragment, renderList, defineComponent, unref, markRaw, h, createApp } from "vue";
import { ssrRenderAttrs, ssrRenderSlot, ssrRenderComponent, ssrRenderVNode, ssrInterpolate, ssrRenderStyle, ssrRenderClass, ssrRenderList, ssrRenderAttr } from "vue/server-renderer";
import { InteractionStatus, LogLevel, PublicClientApplication, InteractionRequiredAuthError, NavigationClient, EventType, EventMessageUtils, InteractionType } from "@azure/msal-browser";
import { useRoute, createRouter, createWebHistory } from "vue-router";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { createPinia } from "pinia";
import axios from "axios";
import { DateTime } from "luxon";
import Vue3EasyDataTable from "vue3-easy-data-table";
const bootstrap_min = "";
library.add(
  faDownload,
  faCartShopping,
  faBox,
  faSeedling,
  faReceipt,
  faTruckField,
  faCalendarDays,
  faUserShield,
  faWarehouse,
  faHeadset,
  faScrewdriverWrench,
  faTableColumns,
  faClipboardCheck,
  faMagnifyingGlass,
  faCircleUser,
  faBoxesStacked,
  faBug,
  faTruckRampBox,
  faTrashCan,
  faPenToSquare,
  faRightFromBracket,
  faPalette,
  faHandsClapping,
  faArrowDown
);
const style = "";
const initSidebar = (props, context) => {
  const { collapsed, relative, width, widthCollapsed, rtl } = toRefs(props);
  const sidebarRef = ref(null);
  const isCollapsed = ref(collapsed.value);
  const activeShow = ref(null);
  const mobileItem = reactive({
    item: null,
    rect: {
      top: 0,
      height: 0,
      padding: "",
      maxHeight: 0,
      maxWidth: 0
    },
    timeout: null
  });
  const getMobileItem = computed(() => mobileItem.item);
  const getMobileItemRect = computed(() => mobileItem.rect);
  const currentRoute = ref(window.location.pathname + window.location.search + window.location.hash);
  const updateIsCollapsed = (val) => {
    isCollapsed.value = val;
  };
  const updateActiveShow = (id) => {
    activeShow.value = id;
  };
  const setMobileItem = ({ item, itemEl }) => {
    clearMobileItemTimeout();
    const linkEl = itemEl.children[0];
    const { top: linkTop, bottom: linkBottom, height: linkHeight } = linkEl.getBoundingClientRect();
    const { left: sidebarLeft, right: sidebarRight } = sidebarRef.value.getBoundingClientRect();
    const offsetParentTop = linkEl.offsetParent.getBoundingClientRect().top;
    let parentHeight = window.innerHeight;
    let parentWidth = window.innerWidth;
    let parentTop = 0;
    let parentRight = parentWidth;
    const maxWidth = parseInt(width.value) - parseInt(widthCollapsed.value);
    if (relative.value) {
      const parent = sidebarRef.value.parentElement;
      parentHeight = parent.clientHeight;
      parentWidth = parent.clientWidth;
      parentTop = parent.getBoundingClientRect().top;
      parentRight = parent.getBoundingClientRect().right;
    }
    const rectWidth = rtl.value ? parentWidth - (parentRight - sidebarLeft) : parentRight - sidebarRight;
    updateMobileItem(item);
    updateMobileItemRect({
      top: linkTop - offsetParentTop,
      height: linkHeight,
      padding: window.getComputedStyle(linkEl).paddingRight,
      maxWidth: rectWidth <= maxWidth ? rectWidth : maxWidth,
      maxHeight: parentHeight - (linkBottom - parentTop)
    });
  };
  const unsetMobileItem = (immediate = true, delay = 800) => {
    if (!getMobileItem.value)
      return;
    clearMobileItemTimeout();
    if (immediate) {
      updateMobileItem(null);
      return;
    }
    mobileItem.timeout = setTimeout(() => {
      updateMobileItem(null);
    }, delay);
  };
  const clearMobileItemTimeout = () => {
    if (mobileItem.timeout)
      clearTimeout(mobileItem.timeout);
  };
  const updateMobileItem = (item) => {
    mobileItem.item = item;
  };
  const updateMobileItemRect = ({ top, height, padding, maxWidth, maxHeight }) => {
    mobileItem.rect.top = top;
    mobileItem.rect.height = height;
    mobileItem.rect.padding = padding;
    mobileItem.rect.maxWidth = maxWidth;
    mobileItem.rect.maxHeight = maxHeight;
  };
  const updateCurrentRoute = () => {
    const route = window.location.pathname + window.location.search + window.location.hash;
    currentRoute.value = route;
  };
  const onItemClick = (event, item) => {
    context.emit("item-click", event, item);
  };
  provide("vsmProps", props);
  provide("getSidebarRef", sidebarRef);
  provide("getIsCollapsed", isCollapsed);
  provide("getActiveShow", activeShow);
  provide("getMobileItem", getMobileItem);
  provide("getMobileItemRect", getMobileItemRect);
  provide("getCurrentRoute", currentRoute);
  provide("updateIsCollapsed", updateIsCollapsed);
  provide("updateActiveShow", updateActiveShow);
  provide("setMobileItem", setMobileItem);
  provide("unsetMobileItem", unsetMobileItem);
  provide("clearMobileItemTimeout", clearMobileItemTimeout);
  provide("onRouteChange", updateCurrentRoute);
  provide("emitItemClick", onItemClick);
  return {
    getSidebarRef: sidebarRef,
    getIsCollapsed: isCollapsed,
    getActiveShow: activeShow,
    getMobileItem,
    getMobileItemRect,
    getCurrentRoute: currentRoute,
    updateIsCollapsed,
    updateActiveShow,
    setMobileItem,
    unsetMobileItem,
    clearMobileItemTimeout,
    updateCurrentRoute,
    onItemClick
  };
};
const useSidebar = () => ({
  getSidebarProps: inject("vsmProps"),
  getSidebarRef: inject("getSidebarRef"),
  getIsCollapsed: inject("getIsCollapsed"),
  getActiveShow: inject("getActiveShow"),
  getMobileItem: inject("getMobileItem"),
  getMobileItemRect: inject("getMobileItemRect"),
  getCurrentRoute: inject("getCurrentRoute"),
  updateIsCollapsed: inject("updateIsCollapsed"),
  updateActiveShow: inject("updateActiveShow"),
  setMobileItem: inject("setMobileItem"),
  unsetMobileItem: inject("unsetMobileItem"),
  clearMobileItemTimeout: inject("clearMobileItemTimeout"),
  onRouteChange: inject("onRouteChange"),
  emitItemClick: inject("emitItemClick")
});
function activeRecordIndex(route, currentRoute) {
  const { matched } = route;
  const { length } = matched;
  const routeMatched = matched[length - 1];
  const currentMatched = currentRoute.matched;
  if (!routeMatched || !currentMatched.length)
    return -1;
  const index = currentMatched.findIndex(
    isSameRouteRecord.bind(null, routeMatched)
  );
  if (index > -1)
    return index;
  const parentRecordPath = getOriginalPath(
    matched[length - 2]
  );
  return length > 1 && getOriginalPath(routeMatched) === parentRecordPath && currentMatched[currentMatched.length - 1].path !== parentRecordPath ? currentMatched.findIndex(
    isSameRouteRecord.bind(null, matched[length - 2])
  ) : index;
}
function isSameRouteLocationParams(a, b) {
  if (Object.keys(a).length !== Object.keys(b).length)
    return false;
  for (const key in a) {
    if (!isSameRouteLocationParamsValue(a[key], b[key]))
      return false;
  }
  return true;
}
function includesParams(outer, inner) {
  for (const key in inner) {
    const innerValue = inner[key];
    const outerValue = outer[key];
    if (typeof innerValue === "string") {
      if (innerValue !== outerValue)
        return false;
    } else {
      if (!Array.isArray(outerValue) || outerValue.length !== innerValue.length || innerValue.some((value, i) => value !== outerValue[i])) {
        return false;
      }
    }
  }
  return true;
}
function getOriginalPath(record) {
  return record ? record.aliasOf ? record.aliasOf.path : record.path : "";
}
function isSameRouteRecord(a, b) {
  return (a.aliasOf || a) === (b.aliasOf || b);
}
function isSameRouteLocationParamsValue(a, b) {
  return Array.isArray(a) ? isEquivalentArray(a, b) : Array.isArray(b) ? isEquivalentArray(b, a) : a === b;
}
function isEquivalentArray(a, b) {
  return Array.isArray(b) ? a.length === b.length && a.every((value, i) => value === b[i]) : a.length === 1 && a[0] === b;
}
function useItem(props) {
  const router2 = getCurrentInstance().appContext.config.globalProperties.$router;
  const {
    getSidebarProps: sidebarProps,
    getIsCollapsed: isCollapsed,
    getActiveShow: activeShow,
    getMobileItem: mobileItem,
    getMobileItemRect: mobileItemRect,
    getCurrentRoute: currentRoute,
    updateActiveShow,
    setMobileItem,
    unsetMobileItem,
    clearMobileItemTimeout,
    emitItemClick
  } = useSidebar();
  const emitScrollUpdate = inject("emitScrollUpdate");
  const itemShow = ref(false);
  const itemHover = ref(false);
  const active = computed(() => {
    return isLinkActive(props.item) || isChildActive(props.item.children);
  });
  const exactActive = computed(() => {
    return isLinkActive(props.item, true);
  });
  const isLinkActive = (item, exact) => {
    if (!item.path || item.external)
      return false;
    if (router2) {
      const route = router2.resolve(item.path);
      const routerCurrentRoute = router2.currentRoute.value;
      const activeIndex = activeRecordIndex(route, routerCurrentRoute);
      if (exact || item.exact) {
        return activeIndex > -1 && activeIndex === routerCurrentRoute.matched.length - 1 && isSameRouteLocationParams(routerCurrentRoute.params, route.params);
      }
      return activeIndex > -1 && includesParams(routerCurrentRoute.params, route.params);
    } else {
      return item.path === currentRoute.value;
    }
  };
  const isChildActive = (child) => {
    if (!child)
      return false;
    return child.some((item) => {
      return isLinkActive(item) || isChildActive(item.children);
    });
  };
  const onLinkClick = (event) => {
    if (!props.item.path || props.item.disabled) {
      event.preventDefault();
      if (props.item.disabled)
        return;
    }
    emitMobileItem(event, event.currentTarget.parentElement);
    if (hasChild.value) {
      if (!props.item.path || active.value) {
        show.value = !show.value;
      }
    }
    emitItemClick(event, props.item);
  };
  const onMouseOver = (event) => {
    if (props.item.disabled)
      return;
    event.stopPropagation();
    itemHover.value = true;
  };
  const onMouseOut = (event) => {
    event.stopPropagation();
    itemHover.value = false;
  };
  const onMouseEnter = (event) => {
    if (props.item.disabled)
      return;
    if (sidebarProps.disableHover) {
      if (isMobileItem.value && hasChild.value) {
        clearMobileItemTimeout();
      }
    } else {
      clearMobileItemTimeout();
      emitMobileItem(event, event.currentTarget);
    }
  };
  const onMouseLeave = () => {
    if (sidebarProps.disableHover && !hasChild.value)
      return;
    if (isMobileItem.value) {
      unsetMobileItem(false, !sidebarProps.disableHover ? 300 : void 0);
    }
  };
  const emitMobileItem = (event, itemEl) => {
    if (isMobileItem.value)
      return;
    if (isCollapsed.value) {
      setTimeout(() => {
        if (isFirstLevel.value) {
          if (!isMobileItem.value) {
            setMobileItem({ item: props.item, itemEl });
          }
        }
        if (event.type === "click" && !hasChild.value) {
          unsetMobileItem(false, !isFirstLevel.value ? 300 : void 0);
        }
      }, 0);
    }
  };
  const onExpandEnter = (el) => {
    el.style.height = el.scrollHeight + "px";
  };
  const onExpandAfterEnter = (el) => {
    el.style.height = "auto";
    if (!isCollapsed.value) {
      emitScrollUpdate();
    }
  };
  const onExpandBeforeLeave = (el) => {
    if (isCollapsed.value && isFirstLevel.value) {
      el.style.display = "none";
      return;
    }
    el.style.height = el.scrollHeight + "px";
  };
  const onExpandAfterLeave = () => {
    if (!isCollapsed.value) {
      emitScrollUpdate();
    }
  };
  const show = computed({
    get: () => {
      if (!hasChild.value)
        return false;
      if (isCollapsed.value && isFirstLevel.value)
        return hover.value;
      if (sidebarProps.showChild)
        return true;
      return sidebarProps.showOneChild && isFirstLevel.value ? props.item.id === activeShow.value : itemShow.value;
    },
    set: (show2) => {
      if (sidebarProps.showOneChild && isFirstLevel.value) {
        show2 ? updateActiveShow(props.item.id) : updateActiveShow(null);
      }
      itemShow.value = show2;
    }
  });
  const hover = computed(() => {
    return isCollapsed.value && isFirstLevel.value ? isMobileItem.value : itemHover.value;
  });
  const isFirstLevel = computed(() => {
    return props.level === 1;
  });
  const isHidden = computed(() => {
    if (isCollapsed.value) {
      if (props.item.hidden && props.item.hiddenOnCollapse === void 0) {
        return true;
      } else {
        return props.item.hiddenOnCollapse === true;
      }
    } else {
      return props.item.hidden === true;
    }
  });
  const hasChild = computed(() => {
    return !!(props.item.children && props.item.children.length > 0);
  });
  const linkClass = computed(() => {
    return [
      "vsm--link",
      `vsm--link_level-${props.level}`,
      { "vsm--link_mobile": isMobileItem.value },
      { "vsm--link_hover": hover.value },
      { "vsm--link_active": active.value },
      { "vsm--link_disabled": props.item.disabled },
      { "vsm--link_open": show.value },
      props.item.class
    ];
  });
  const linkAttrs = computed(() => {
    const href = props.item.path ? props.item.path : "#";
    const target = props.item.external ? "_blank" : "_self";
    const tabindex = props.item.disabled ? -1 : null;
    const ariaCurrent = exactActive.value ? "page" : null;
    const ariaHaspopup = hasChild.value ? true : null;
    const ariaExpanded = hasChild.value ? show.value : null;
    return {
      href,
      target,
      tabindex,
      "aria-current": ariaCurrent,
      "aria-haspopup": ariaHaspopup,
      "aria-expanded": ariaExpanded,
      ...props.item.attributes
    };
  });
  const itemClass = computed(() => {
    return [
      "vsm--item",
      { "vsm--item_mobile": isMobileItem.value }
    ];
  });
  const isMobileItem = computed(() => {
    var _a, _b;
    if (props.item.id === void 0 && ((_a = mobileItem.value) == null ? void 0 : _a.id) === void 0) {
      return false;
    }
    return props.item.id === ((_b = mobileItem.value) == null ? void 0 : _b.id);
  });
  const mobileItemDropdownStyle = computed(() => {
    return [
      { position: "absolute" },
      { top: `${mobileItemRect.value.top + mobileItemRect.value.height}px` },
      !sidebarProps.rtl ? { left: sidebarProps.widthCollapsed } : { right: sidebarProps.widthCollapsed },
      { width: `${mobileItemRect.value.maxWidth}px` },
      { "max-height": `${mobileItemRect.value.maxHeight}px` },
      { "overflow-y": "auto" }
    ];
  });
  const mobileItemStyle = computed(() => {
    return [
      { position: "absolute" },
      { top: `${mobileItemRect.value.top}px` },
      !sidebarProps.rtl ? { left: sidebarProps.widthCollapsed } : { right: sidebarProps.widthCollapsed },
      { width: `${mobileItemRect.value.maxWidth}px` },
      { height: `${mobileItemRect.value.height}px` },
      { "padding-right": `${mobileItemRect.value.padding}` },
      { "padding-left": `${mobileItemRect.value.padding}` },
      { "z-index": "20" }
    ];
  });
  const mobileItemBackgroundStyle = computed(() => {
    return [
      { position: "absolute" },
      { top: `${mobileItemRect.value.top}px` },
      !sidebarProps.rtl ? { left: "0px" } : { right: "0px" },
      { width: `${mobileItemRect.value.maxWidth + parseInt(sidebarProps.widthCollapsed)}px` },
      { height: `${mobileItemRect.value.height}px` },
      { "z-index": "10" }
    ];
  });
  watch(() => active.value, () => {
    if (active.value) {
      show.value = true;
    }
  }, {
    immediate: true
  });
  return {
    active,
    exactActive,
    show,
    hover,
    isFirstLevel,
    isHidden,
    hasChild,
    linkClass,
    linkAttrs,
    itemClass,
    isMobileItem,
    mobileItemDropdownStyle,
    mobileItemStyle,
    mobileItemBackgroundStyle,
    onLinkClick,
    onMouseOver,
    onMouseOut,
    onMouseEnter,
    onMouseLeave,
    onExpandEnter,
    onExpandAfterEnter,
    onExpandBeforeLeave,
    onExpandAfterLeave
  };
}
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
const _sfc_main$j = {
  name: "SidebarMenuLink",
  inheritAttrs: false,
  props: {
    item: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      router: false
    };
  },
  computed: {
    isHyperLink() {
      return !!(!this.item.path || this.item.external || !this.router);
    }
  },
  mounted() {
    this.router = !!this.$router;
  }
};
function _sfc_ssrRender$h(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_router_link = resolveComponent("router-link");
  if ($options.isHyperLink) {
    _push(`<a${ssrRenderAttrs(mergeProps(_ctx.$attrs, _attrs))}>`);
    ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
    _push(`</a>`);
  } else {
    _push(ssrRenderComponent(_component_router_link, mergeProps({
      custom: "",
      to: this.item.path
    }, _attrs), {
      default: withCtx(({ path, navigate }, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(`<a${ssrRenderAttrs(mergeProps(_ctx.$attrs, { href: path }))}${_scopeId}>`);
          ssrRenderSlot(_ctx.$slots, "default", {}, null, _push2, _parent2, _scopeId);
          _push2(`</a>`);
        } else {
          return [
            createVNode("a", mergeProps(_ctx.$attrs, {
              href: path,
              onClick: navigate
            }), [
              renderSlot(_ctx.$slots, "default")
            ], 16, ["href", "onClick"])
          ];
        }
      }),
      _: 3
    }, _parent));
  }
}
const _sfc_setup$j = _sfc_main$j.setup;
_sfc_main$j.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/SidebarMenuLink.vue");
  return _sfc_setup$j ? _sfc_setup$j(props, ctx) : void 0;
};
const SidebarMenuLink = /* @__PURE__ */ _export_sfc(_sfc_main$j, [["ssrRender", _sfc_ssrRender$h]]);
const _sfc_main$i = {
  name: "SidebarMenuIcon",
  props: {
    icon: {
      type: [String, Object],
      default: ""
    }
  }
};
function _sfc_ssrRender$g(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  ssrRenderVNode(_push, createVNode(resolveDynamicComponent($props.icon.element ? $props.icon.element : "i"), mergeProps({
    class: ["vsm--icon", typeof $props.icon === "string" || $props.icon instanceof String ? $props.icon : $props.icon.class],
    "aria-hidden": "true"
  }, $props.icon.attributes, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`${ssrInterpolate($props.icon.text)}`);
      } else {
        return [
          createTextVNode(toDisplayString($props.icon.text), 1)
        ];
      }
    }),
    _: 1
  }), _parent);
}
const _sfc_setup$i = _sfc_main$i.setup;
_sfc_main$i.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/SidebarMenuIcon.vue");
  return _sfc_setup$i ? _sfc_setup$i(props, ctx) : void 0;
};
const SidebarMenuIcon = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["ssrRender", _sfc_ssrRender$g]]);
const _sfc_main$h = {
  name: "SidebarMenuBadge",
  props: {
    badge: {
      type: Object,
      default: () => {
      }
    }
  }
};
function _sfc_ssrRender$f(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  ssrRenderVNode(_push, createVNode(resolveDynamicComponent($props.badge.element ? $props.badge.element : "span"), mergeProps({
    class: ["vsm--badge", $props.badge.class]
  }, $props.badge.attributes, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`${ssrInterpolate($props.badge.text)}`);
      } else {
        return [
          createTextVNode(toDisplayString($props.badge.text), 1)
        ];
      }
    }),
    _: 1
  }), _parent);
}
const _sfc_setup$h = _sfc_main$h.setup;
_sfc_main$h.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/SidebarMenuBadge.vue");
  return _sfc_setup$h ? _sfc_setup$h(props, ctx) : void 0;
};
const SidebarMenuBadge = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["ssrRender", _sfc_ssrRender$f]]);
const _sfc_main$g = {
  name: "SidebarMenuItem",
  components: {
    SidebarMenuLink,
    SidebarMenuIcon,
    SidebarMenuBadge
  },
  props: {
    item: {
      type: Object,
      required: true
    },
    level: {
      type: Number,
      default: 1
    }
  },
  setup(props) {
    const {
      getSidebarProps,
      getIsCollapsed: isCollapsed
    } = useSidebar();
    const { linkComponentName } = toRefs(getSidebarProps);
    const {
      active,
      exactActive,
      show,
      hover,
      isFirstLevel,
      isHidden,
      hasChild,
      linkClass,
      linkAttrs,
      itemClass,
      isMobileItem,
      mobileItemStyle,
      mobileItemDropdownStyle,
      mobileItemBackgroundStyle,
      onLinkClick,
      onMouseOver,
      onMouseOut,
      onMouseEnter,
      onMouseLeave,
      onExpandEnter,
      onExpandAfterEnter,
      onExpandBeforeLeave,
      onExpandAfterLeave
    } = useItem(props);
    return {
      isCollapsed,
      linkComponentName,
      active,
      exactActive,
      isMobileItem,
      mobileItemStyle,
      mobileItemDropdownStyle,
      mobileItemBackgroundStyle,
      show,
      hover,
      isFirstLevel,
      isHidden,
      hasChild,
      linkClass,
      linkAttrs,
      itemClass,
      onLinkClick,
      onMouseOver,
      onMouseOut,
      onMouseEnter,
      onMouseLeave,
      onExpandEnter,
      onExpandAfterEnter,
      onExpandBeforeLeave,
      onExpandAfterLeave
    };
  }
};
function _sfc_ssrRender$e(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_sidebar_menu_icon = resolveComponent("sidebar-menu-icon");
  const _component_sidebar_menu_badge = resolveComponent("sidebar-menu-badge");
  const _component_sidebar_menu_item = resolveComponent("sidebar-menu-item");
  if ($props.item.component && !$setup.isHidden) {
    _push(`<li${ssrRenderAttrs(_attrs)}>`);
    ssrRenderVNode(_push, createVNode(resolveDynamicComponent($props.item.component), $props.item.props, null), _parent);
    _push(`</li>`);
  } else if ($props.item.header && !$setup.isHidden) {
    _push(`<li${ssrRenderAttrs(mergeProps({
      class: ["vsm--header", $props.item.class]
    }, $props.item.attributes, _attrs))}>${ssrInterpolate($props.item.header)}</li>`);
  } else if (!$setup.isHidden) {
    _push(`<li${ssrRenderAttrs(mergeProps({ class: $setup.itemClass }, _attrs))}>`);
    ssrRenderVNode(_push, createVNode(resolveDynamicComponent($setup.linkComponentName ? $setup.linkComponentName : "SidebarMenuLink"), mergeProps({
      item: $props.item,
      class: $setup.linkClass
    }, $setup.linkAttrs, { onClick: $setup.onLinkClick }), {
      default: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          if ($setup.isCollapsed && $setup.isFirstLevel) {
            if ($setup.hover) {
              _push2(`<div class="vsm--mobile-bg" style="${ssrRenderStyle($setup.mobileItemBackgroundStyle)}"${_scopeId}></div>`);
            } else {
              _push2(`<!---->`);
            }
          } else {
            _push2(`<!---->`);
          }
          if ($props.item.icon) {
            _push2(ssrRenderComponent(_component_sidebar_menu_icon, {
              icon: $props.item.icon
            }, null, _parent2, _scopeId));
          } else {
            _push2(`<!---->`);
          }
          _push2(`<div class="${ssrRenderClass([$setup.isCollapsed && $setup.isFirstLevel && !$setup.isMobileItem && "vsm--title_hidden", "vsm--title"])}" style="${ssrRenderStyle($setup.isMobileItem && $setup.mobileItemStyle)}"${_scopeId}><span${_scopeId}>${ssrInterpolate($props.item.name)}</span>`);
          if ($props.item.badge) {
            _push2(ssrRenderComponent(_component_sidebar_menu_badge, {
              badge: $props.item.badge
            }, null, _parent2, _scopeId));
          } else {
            _push2(`<!---->`);
          }
          if ($setup.hasChild) {
            _push2(`<div class="${ssrRenderClass([{ "vsm--arrow_open": $setup.show }, "vsm--arrow"])}"${_scopeId}>`);
            ssrRenderSlot(_ctx.$slots, "dropdown-icon", { isOpen: $setup.show }, null, _push2, _parent2, _scopeId);
            _push2(`</div>`);
          } else {
            _push2(`<!---->`);
          }
          _push2(`</div>`);
        } else {
          return [
            $setup.isCollapsed && $setup.isFirstLevel ? (openBlock(), createBlock(Transition, {
              key: 0,
              name: "slide-animation"
            }, {
              default: withCtx(() => [
                $setup.hover ? (openBlock(), createBlock("div", {
                  key: 0,
                  class: "vsm--mobile-bg",
                  style: $setup.mobileItemBackgroundStyle
                }, null, 4)) : createCommentVNode("", true)
              ]),
              _: 1
            })) : createCommentVNode("", true),
            $props.item.icon ? (openBlock(), createBlock(_component_sidebar_menu_icon, {
              key: 1,
              icon: $props.item.icon
            }, null, 8, ["icon"])) : createCommentVNode("", true),
            createVNode("div", {
              class: ["vsm--title", $setup.isCollapsed && $setup.isFirstLevel && !$setup.isMobileItem && "vsm--title_hidden"],
              style: $setup.isMobileItem && $setup.mobileItemStyle
            }, [
              createVNode("span", null, toDisplayString($props.item.name), 1),
              $props.item.badge ? (openBlock(), createBlock(_component_sidebar_menu_badge, {
                key: 0,
                badge: $props.item.badge
              }, null, 8, ["badge"])) : createCommentVNode("", true),
              $setup.hasChild ? (openBlock(), createBlock("div", {
                key: 1,
                class: ["vsm--arrow", { "vsm--arrow_open": $setup.show }]
              }, [
                renderSlot(_ctx.$slots, "dropdown-icon", { isOpen: $setup.show })
              ], 2)) : createCommentVNode("", true)
            ], 6)
          ];
        }
      }),
      _: 3
    }), _parent);
    if ($setup.hasChild) {
      if ($setup.show) {
        _push(`<div class="${ssrRenderClass([$setup.isMobileItem && "vsm--child_mobile", "vsm--child"])}" style="${ssrRenderStyle($setup.isMobileItem && $setup.mobileItemDropdownStyle)}"><ul class="vsm--dropdown"><!--[-->`);
        ssrRenderList($props.item.children, (subItem) => {
          _push(ssrRenderComponent(_component_sidebar_menu_item, {
            key: subItem.id,
            item: subItem,
            level: $props.level + 1
          }, {
            "dropdown-icon": withCtx(({ isOpen }, _push2, _parent2, _scopeId) => {
              if (_push2) {
                ssrRenderSlot(_ctx.$slots, "dropdown-icon", { isOpen }, null, _push2, _parent2, _scopeId);
              } else {
                return [
                  renderSlot(_ctx.$slots, "dropdown-icon", { isOpen })
                ];
              }
            }),
            _: 2
          }, _parent));
        });
        _push(`<!--]--></ul></div>`);
      } else {
        _push(`<!---->`);
      }
    } else {
      _push(`<!---->`);
    }
    _push(`</li>`);
  } else {
    _push(`<!---->`);
  }
}
const _sfc_setup$g = _sfc_main$g.setup;
_sfc_main$g.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/MenuItem.vue");
  return _sfc_setup$g ? _sfc_setup$g(props, ctx) : void 0;
};
const SidebarMenuItem = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["ssrRender", _sfc_ssrRender$e]]);
const _sfc_main$f = {
  name: "SidebarMenuScroll",
  setup() {
    const { getIsCollapsed: isCollapsed } = useSidebar();
    const scrollRef = ref(null);
    const scrollBarRef = ref(null);
    const scrollThumbRef = ref(null);
    const thumbYPerc = ref(0);
    const thumbHeightPerc = ref(0);
    let cursorY = 0;
    let cursorDown = false;
    const thumbStyle = computed(() => {
      return {
        height: `${thumbHeightPerc.value}%`,
        transform: `translateY(${thumbYPerc.value}%)`
      };
    });
    const onScrollUpdate = () => {
      if (!scrollRef.value)
        return;
      nextTick(() => {
        updateThumb();
      });
    };
    const onScroll = () => {
      requestAnimationFrame(onScrollUpdate);
    };
    const onClick = (e) => {
      const offset = Math.abs(scrollBarRef.value.getBoundingClientRect().y - e.clientY);
      const thumbHalf = scrollThumbRef.value.offsetHeight / 2;
      updateScrollTop(offset - thumbHalf);
    };
    const onMouseDown = (e) => {
      e.stopImmediatePropagation();
      cursorDown = true;
      window.addEventListener("mousemove", onMouseMove);
      window.addEventListener("mouseup", onMouseUp);
      cursorY = scrollThumbRef.value.offsetHeight - (e.clientY - scrollThumbRef.value.getBoundingClientRect().y);
    };
    const onMouseMove = (e) => {
      if (!cursorDown)
        return;
      const offset = e.clientY - scrollBarRef.value.getBoundingClientRect().y;
      const thumbClickPosition = scrollThumbRef.value.offsetHeight - cursorY;
      updateScrollTop(offset - thumbClickPosition);
    };
    const onMouseUp = (e) => {
      cursorDown = false;
      cursorY = 0;
      window.removeEventListener("mousemove", onMouseMove);
      window.removeEventListener("mouseup", onMouseUp);
    };
    const updateThumb = () => {
      const heightPerc = scrollRef.value.clientHeight * 100 / scrollRef.value.scrollHeight;
      thumbHeightPerc.value = heightPerc < 100 ? heightPerc : 0;
      thumbYPerc.value = scrollRef.value.scrollTop * 100 / scrollRef.value.clientHeight;
    };
    const updateScrollTop = (y) => {
      const scrollPerc = y * 100 / scrollBarRef.value.offsetHeight;
      scrollRef.value.scrollTop = scrollPerc * scrollRef.value.scrollHeight / 100;
    };
    watch(() => isCollapsed.value, () => {
      onScrollUpdate();
    });
    onMounted(() => {
      onScrollUpdate();
      window.addEventListener("resize", onScrollUpdate);
    });
    onUnmounted(() => {
      window.removeEventListener("resize", onScrollUpdate);
    });
    provide("emitScrollUpdate", onScrollUpdate);
    return {
      scrollRef,
      scrollBarRef,
      scrollThumbRef,
      thumbStyle,
      onScroll,
      onClick,
      onMouseDown
    };
  }
};
function _sfc_ssrRender$d(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "vsm--scroll-wrapper" }, _attrs))}><div class="vsm--scroll-overflow"><div class="vsm--scroll">`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</div><div class="vsm--scroll-bar"><div class="vsm--scroll-thumb" style="${ssrRenderStyle($setup.thumbStyle)}"></div></div></div></div>`);
}
const _sfc_setup$f = _sfc_main$f.setup;
_sfc_main$f.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/SidebarMenuScroll.vue");
  return _sfc_setup$f ? _sfc_setup$f(props, ctx) : void 0;
};
const SidebarMenuScroll = /* @__PURE__ */ _export_sfc(_sfc_main$f, [["ssrRender", _sfc_ssrRender$d]]);
const _sfc_main$e = {
  name: "Sidebar",
  components: {
    SidebarMenuItem,
    SidebarMenuScroll
  },
  props: {
    menu: {
      type: Array,
      required: true
    },
    collapsed: {
      type: Boolean,
      default: true
    },
    width: {
      type: String,
      default: "290px"
    },
    widthCollapsed: {
      type: String,
      default: "65px"
    },
    showChild: {
      type: Boolean,
      default: false
    },
    theme: {
      type: String,
      default: ""
    },
    showOneChild: {
      type: Boolean,
      default: false
    },
    rtl: {
      type: Boolean,
      default: false
    },
    relative: {
      type: Boolean,
      default: false
    },
    hideToggle: {
      type: Boolean,
      default: false
    },
    disableHover: {
      type: Boolean,
      default: false
    },
    linkComponentName: {
      type: String,
      default: void 0
    }
  },
  data() {
    return {};
  },
  setup(props, context) {
    const {
      getSidebarRef: sidebarMenuRef,
      getIsCollapsed: isCollapsed,
      updateIsCollapsed,
      unsetMobileItem,
      updateCurrentRoute
    } = initSidebar(props, context);
    const computedMenu = computed(() => {
      let id = 0;
      function transformItems(items) {
        function randomId() {
          return `${Date.now() + "" + id++}`;
        }
        return items.map((item) => {
          let idRandom = randomId();
          return { id: idRandom, ...item, ...item.child && { child: idRandom, ...item.child } };
        });
      }
      return transformItems(props.menu);
    });
    const sidebarWidth = computed(() => {
      return isCollapsed.value ? props.widthCollapsed : props.width;
    });
    const sidebarClass = computed(() => {
      return [
        !isCollapsed.value ? "vsm_expanded" : "vsm_collapsed",
        props.theme ? `vsm_${props.theme}` : "",
        props.rtl ? "vsm_rtl" : "",
        props.relative ? "vsm_relative" : ""
      ];
    });
    const onToggleClick = () => {
      unsetMobileItem();
      updateIsCollapsed(!isCollapsed.value);
      context.emit("update:collapsed", isCollapsed.value);
    };
    watch(() => props.collapsed, (currentCollapsed) => {
      unsetMobileItem();
      updateIsCollapsed(currentCollapsed);
    });
    const router2 = getCurrentInstance().appContext.config.globalProperties.$router;
    if (!router2) {
      onMounted(() => {
        window.addEventListener("hashchange", updateCurrentRoute);
      });
      onUnmounted(() => {
        window.removeEventListener("hashchange", updateCurrentRoute);
      });
    }
    return {
      sidebarMenuRef,
      isCollapsed,
      computedMenu,
      sidebarWidth,
      sidebarClass,
      onToggleClick,
      onRouteChange: updateCurrentRoute
    };
  },
  computed: {
    routes() {
      return this.$router.options.routes;
    }
  },
  created() {
  },
  methods: {}
};
function _sfc_ssrRender$c(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_sidebar_menu_scroll = resolveComponent("sidebar-menu-scroll");
  const _component_sidebar_menu_item = resolveComponent("sidebar-menu-item");
  _push(`<div${ssrRenderAttrs(mergeProps({
    ref: "sidebarMenuRef",
    class: ["v-sidebar-menu", $setup.sidebarClass],
    style: { "max-width": $setup.sidebarWidth }
  }, _attrs))}>`);
  ssrRenderSlot(_ctx.$slots, "header", {}, null, _push, _parent);
  _push(ssrRenderComponent(_component_sidebar_menu_scroll, null, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="p-3" style="${ssrRenderStyle({ "width": "290px" })}"${_scopeId}><a href="/" class="d-flex align-items-center link-dark text-decoration-none"${_scopeId}><img src="/img/willemse_icon40.png" style="${ssrRenderStyle({ "margin-right": "2px" })}"${_scopeId}><span class="fw-semibold vsm--header"${_scopeId}>Intranet<span style="${ssrRenderStyle({ "color": "#69b145!important" })}"${_scopeId}> Willemse</span></span></a></div><ul class="vsm--menu" style="${ssrRenderStyle({ "width": $setup.sidebarWidth })}"${_scopeId}><li${_scopeId}><hr style="${ssrRenderStyle({ "border": "1px solid", "margin": "20px" })}"${_scopeId}></li><!--[-->`);
        ssrRenderList($setup.computedMenu, (item) => {
          _push2(ssrRenderComponent(_component_sidebar_menu_item, {
            key: item.id,
            item
          }, {
            "dropdown-icon": withCtx(({ isOpen }, _push3, _parent3, _scopeId2) => {
              if (_push3) {
                ssrRenderSlot(_ctx.$slots, "dropdown-icon", { isOpen }, () => {
                  _push3(`<span class="vsm--arrow_default"${_scopeId2}></span>`);
                }, _push3, _parent3, _scopeId2);
              } else {
                return [
                  renderSlot(_ctx.$slots, "dropdown-icon", { isOpen }, () => [
                    createVNode("span", { class: "vsm--arrow_default" })
                  ])
                ];
              }
            }),
            _: 2
          }, _parent2, _scopeId));
        });
        _push2(`<!--]--></ul>`);
      } else {
        return [
          createVNode("div", {
            class: "p-3",
            style: { "width": "290px" }
          }, [
            createVNode("a", {
              href: "/",
              class: "d-flex align-items-center link-dark text-decoration-none"
            }, [
              createVNode("img", {
                src: "/img/willemse_icon40.png",
                style: { "margin-right": "2px" }
              }),
              createVNode("span", { class: "fw-semibold vsm--header" }, [
                createTextVNode("Intranet"),
                createVNode("span", { style: { "color": "#69b145!important" } }, " Willemse")
              ])
            ])
          ]),
          createVNode("ul", {
            class: "vsm--menu",
            style: { "width": $setup.sidebarWidth }
          }, [
            createVNode("li", null, [
              createVNode("hr", { style: { "border": "1px solid", "margin": "20px" } })
            ]),
            (openBlock(true), createBlock(Fragment, null, renderList($setup.computedMenu, (item) => {
              return openBlock(), createBlock(_component_sidebar_menu_item, {
                key: item.id,
                item
              }, {
                "dropdown-icon": withCtx(({ isOpen }) => [
                  renderSlot(_ctx.$slots, "dropdown-icon", { isOpen }, () => [
                    createVNode("span", { class: "vsm--arrow_default" })
                  ])
                ]),
                _: 2
              }, 1032, ["item"]);
            }), 128))
          ], 4)
        ];
      }
    }),
    _: 3
  }, _parent));
  ssrRenderSlot(_ctx.$slots, "footer", {}, null, _push, _parent);
  if (!$props.hideToggle) {
    _push(`<button class="vsm--toggle-btn">`);
    ssrRenderSlot(_ctx.$slots, "toggle-icon", {}, () => {
      _push(`<span class="vsm--toggle-btn_default"></span>`);
    }, _push, _parent);
    _push(`</button>`);
  } else {
    _push(`<!---->`);
  }
  _push(`</div>`);
}
const _sfc_setup$e = _sfc_main$e.setup;
_sfc_main$e.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Sidebar/Sidebar.vue");
  return _sfc_setup$e ? _sfc_setup$e(props, ctx) : void 0;
};
const Sidebar = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["ssrRender", _sfc_ssrRender$c]]);
const _sfc_main$d = {
  components: {},
  data() {
    return {
      dark: ref(false)
    };
  },
  mounted() {
    if (localStorage) {
      if (localStorage.selectedTheme !== "white-theme") {
        this.dark = true;
      } else {
        this.dark = false;
      }
    }
  },
  methods: {
    toggleDark() {
      this.dark = !this.dark;
      this.$emit("get-dark", this.dark);
    }
  }
};
const ThemesButton_vue_vue_type_style_index_0_scoped_38a5c918_lang = "";
function _sfc_ssrRender$b(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<button${ssrRenderAttrs(mergeProps({ class: "btn btn-link" }, _attrs))} data-v-38a5c918><svg width="50px" height="25px" viewBox="0 0 200 94" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="${ssrRenderClass({ dark: $data.dark })}" data-v-38a5c918><defs data-v-38a5c918><circle id="path-1" cx="25" cy="25" r="25" data-v-38a5c918></circle></defs><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" data-v-38a5c918><g id="dark-mode-toggle" data-v-38a5c918><rect id="bg" fill="#364050" fill-rule="nonzero" x="0" y="0" width="189" height="88" rx="44" data-v-38a5c918></rect><circle id="main-circle" fill="#FFFFFF" fill-rule="nonzero" cx="46" cy="44" r="25" data-v-38a5c918></circle><g id="moon-accents" transform="translate(21.000000, 19.000000)" data-v-38a5c918><mask id="mask-2" fill="white" data-v-38a5c918><use xlink:href="#path-1" data-v-38a5c918></use></mask><use id="Mask" fill-rule="nonzero" xlink:href="#path-1" data-v-38a5c918></use><circle id="moon-accent-2" fill="#EEEEEE" fill-rule="nonzero" mask="url(#mask-2)" cx="44" cy="23" r="11" data-v-38a5c918></circle><circle id="moon-accent-1" fill="#EEEEEE" fill-rule="nonzero" mask="url(#mask-2)" cx="9" cy="8" r="7" data-v-38a5c918></circle><circle id="moon-accent-3" fill="#EEEEEE" fill-rule="nonzero" mask="url(#mask-2)" cx="9" cy="38" r="10" data-v-38a5c918></circle><circle id="moon-accent-4" fill="#EEEEEE" fill-rule="nonzero" mask="url(#mask-2)" cx="24" cy="15" r="6" data-v-38a5c918></circle><circle id="moon-accent-5" fill="#EEEEEE" fill-rule="nonzero" mask="url(#mask-2)" cx="31.5" cy="39.5" r="3.5" data-v-38a5c918></circle></g><g id="stars" transform="translate(89.000000, 18.000000)" fill="#FFFFFF" data-v-38a5c918><polygon id="Star" points="6.5 9.75 2.67939586 11.7586105 3.40906632 7.50430523 0.318132644 4.49138954 4.58969793 3.87069477 6.5 0 8.41030207 3.87069477 12.6818674 4.49138954 9.59093368 7.50430523 10.3206041 11.7586105" data-v-38a5c918></polygon><polygon id="Star-Copy" points="21 43 15.1221475 46.0901699 16.2447174 39.545085 11.4894348 34.9098301 18.0610737 33.954915 21 28 23.9389263 33.954915 30.5105652 34.9098301 25.7552826 39.545085 26.8778525 46.0901699" data-v-38a5c918></polygon><polygon id="Star-Copy-2" points="52 8 49.648859 9.23606798 50.097887 6.61803399 48.1957739 4.76393202 50.8244295 4.38196601 52 2 53.1755705 4.38196601 55.8042261 4.76393202 53.902113 6.61803399 54.351141 9.23606798" data-v-38a5c918></polygon><polygon id="Star-Copy-3" points="60 31 56.4732885 32.854102 57.1468305 28.927051 54.2936609 26.145898 58.2366442 25.572949 60 22 61.7633558 25.572949 65.7063391 26.145898 62.8531695 28.927051 63.5267115 32.854102" data-v-38a5c918></polygon></g><g id="clouds" transform="translate(24.000000, 17.000000)" fill="#FFFFFF" fill-rule="nonzero" data-v-38a5c918><path d="M22.4581401,8.64610504 C22.458794,8.60836037 22.4595787,8.57068044 22.4595787,8.53280628 C22.4595787,4.92485595 19.5054963,2 15.8614938,2 C12.7211834,2 10.0935887,4.17216375 9.42701247,7.08174056 C8.80548929,6.7372476 8.08954429,6.53997828 7.32658453,6.53997828 C4.9459644,6.53997828 3.01613657,8.45077798 3.01613657,10.8077806 C3.01613657,10.9041169 3.02051764,10.9993526 3.02686038,11.0941351 C1.2595244,11.743693 0,13.42796 0,15.4038903 C0,17.9422356 2.07832643,20 4.64203597,20 L21.1974387,20 C24.402092,20 27,17.4277945 27,14.2548467 C27,11.5105566 25.0565712,9.2160949 22.4581401,8.64610504 Z" id="Path" data-v-38a5c918></path><path d="M75.4581401,38.646105 C75.458794,38.6083604 75.4595787,38.5706804 75.4595787,38.5328063 C75.4595787,34.9248559 72.5054963,32 68.8614938,32 C65.7211834,32 63.0935887,34.1721638 62.4270125,37.0817406 C61.8054893,36.7372476 61.0895443,36.5399783 60.3265845,36.5399783 C57.9459644,36.5399783 56.0161366,38.450778 56.0161366,40.8077806 C56.0161366,40.9041169 56.0205176,40.9993526 56.0268604,41.0941351 C54.2595244,41.743693 53,43.42796 53,45.4038903 C53,47.9422356 55.0783264,50 57.642036,50 L74.1974387,50 C77.402092,50 80,47.4277945 80,44.2548467 C80,41.5105566 78.0565712,39.2160949 75.4581401,38.646105 Z" id="Path-Copy-2" data-v-38a5c918></path><path d="M37.4466786,38.851297 C37.1439054,38.851297 36.8417385,38.8815517 36.5450288,38.9418581 C36.3212839,37.8199968 35.5979024,36.8630135 34.5822578,36.3450283 C33.5668154,35.8270431 32.3708814,35.8053165 31.3372484,36.2859402 C30.4018448,33.6974356 27.5548062,32.3609484 24.978202,33.3006723 C22.4015977,34.2403961 21.071255,37.1007872 22.0066586,39.6890888 C19.7496033,39.7607661 17.9667177,41.6375741 18.0004714,43.905866 C18.0344273,46.1743611 19.8726934,47.9963451 22.1309613,48 L37.4466786,48 C39.9612326,48 42,45.9520193 42,43.4256485 C42,40.8992777 39.9612326,38.851297 37.4466786,38.851297 Z" id="Path" data-v-38a5c918></path><path d="M56.585009,4.2909511 C56.357929,4.2909511 56.1313039,4.31313789 55.9087716,4.3573626 C55.7409629,3.5346643 55.1984268,2.83287655 54.4366934,2.45302076 C53.6751116,2.07316494 52.778161,2.05723213 52.0029363,2.4096895 C51.3013836,0.511452738 49.1661047,-0.468637832 47.2336515,0.22049299 C45.3011983,0.909623811 44.3034412,3.00724393 45.0049939,4.9053318 C43.3122025,4.95789514 41.9750383,6.33422103 42.0003536,7.99763509 C42.0258205,9.66119812 43.40452,10.9973197 45.098221,11 L56.585009,11 C58.4709244,11 60,9.4981475 60,7.64547555 C60,5.79280364 58.4709244,4.2909511 56.585009,4.2909511 Z" id="Path-Copy" data-v-38a5c918></path></g></g></g></svg></button>`);
}
const _sfc_setup$d = _sfc_main$d.setup;
_sfc_main$d.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Navbar/ThemesButton.vue");
  return _sfc_setup$d ? _sfc_setup$d(props, ctx) : void 0;
};
const ThemeButton = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["ssrRender", _sfc_ssrRender$b], ["__scopeId", "data-v-38a5c918"]]);
const _sfc_main$c = {
  data() {
    return {
      search: "",
      url: ""
    };
  },
  methods: {
    getDark(event) {
      this.$emit("get-dark", event);
    },
    onSubmit() {
      if (this.search.length <= 6) {
        this.$router.push({ name: "Product", params: { id: this.search } });
      }
    }
  }
};
const Search_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$a(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_font_awesome_icon = resolveComponent("font-awesome-icon");
  _push(`<form${ssrRenderAttrs(mergeProps({
    class: "d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search",
    role: "search",
    action: $data.url
  }, _attrs))}><div class="input-group"><input type="text"${ssrRenderAttr("value", $data.search)} class="form-control bg-light border-0 small" placeholder="Ref, Commande..." aria-label="Ref, Commande..." aria-describedby="basic-addon2"><button class="btn btn-primary" type="button">`);
  _push(ssrRenderComponent(_component_font_awesome_icon, { icon: "fa-solid fa-magnifying-glass" }, null, _parent));
  _push(`</button></div></form>`);
}
const _sfc_setup$c = _sfc_main$c.setup;
_sfc_main$c.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Navbar/Search.vue");
  return _sfc_setup$c ? _sfc_setup$c(props, ctx) : void 0;
};
const SearchVue = /* @__PURE__ */ _export_sfc(_sfc_main$c, [["ssrRender", _sfc_ssrRender$a]]);
function useMsal() {
  const internalInstance = getCurrentInstance();
  if (!internalInstance) {
    throw "useMsal() cannot be called outside the setup() function of a component";
  }
  const { instance, accounts: accounts2, inProgress } = toRefs(internalInstance.appContext.config.globalProperties.$msal);
  if (!instance || !accounts2 || !inProgress) {
    throw "Please install the msalPlugin";
  }
  if (inProgress.value === InteractionStatus.Startup) {
    instance.value.handleRedirectPromise().catch(() => {
      return;
    });
  }
  return {
    instance: instance.value,
    accounts: accounts2,
    inProgress
  };
}
const msalConfig = {
  auth: {
    clientId: "50114aa5-0d5e-4b25-98c4-6fe9252e5b14",
    authority: "https://login.microsoftonline.com/b1882751-825a-4cc5-8110-a7f80a860c73",
    redirectUri: "https://biloba-vue3.test/auth/callback",
    postLogoutRedirectUri: "/"
  },
  cache: {
    cacheLocation: "localStorage"
  },
  system: {
    loggerOptions: {
      loggerCallback: (level, message, containsPii) => {
        if (containsPii) {
          return;
        }
        switch (level) {
          case LogLevel.Error:
            console.error(message);
            return;
          case LogLevel.Info:
            console.info(message);
            return;
          case LogLevel.Verbose:
            console.debug(message);
            return;
          case LogLevel.Warning:
            console.warn(message);
            return;
          default:
            return;
        }
      },
      logLevel: LogLevel.Verbose
    }
  }
};
const msalInstance = new PublicClientApplication(msalConfig);
const loginRequest = {
  scopes: ["User.Read"]
};
const graphConfig = {
  graphMeEndpoint: "https://graph.microsoft.com/v1.0/me",
  graphImgEndpoint: "https://graph.microsoft.com/v1.0/me/photo/$value"
};
async function callMsGraph(accessToken) {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;
  headers.append("Authorization", bearer);
  const options = {
    method: "GET",
    headers
  };
  return fetch(graphConfig.graphMeEndpoint, options).then((response) => response.json()).catch((error) => {
    console.log(error);
    throw error;
  });
}
async function callMsGraphImg(accessToken) {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;
  headers.append("Authorization", bearer);
  const options = {
    method: "GET",
    headers
  };
  return fetch(graphConfig.graphImgEndpoint, options).then((response) => response.blob()).catch((error) => {
    console.log(error);
    throw error;
  });
}
const _sfc_main$b = /* @__PURE__ */ defineComponent({
  __name: "Navbar",
  __ssrInlineRender: true,
  props: {
    theme: {
      type: String,
      required: true,
      default: ""
    }
  },
  emits: ["get-dark"],
  setup(__props, { emit }) {
    const props = __props;
    const { instance, inProgress } = useMsal();
    let urlImg = ref("");
    const state = reactive({
      resolved: false,
      data: {}
    });
    async function getGraphData() {
      const response = await instance.acquireTokenSilent({
        ...loginRequest
      }).catch(async (e) => {
        if (e instanceof InteractionRequiredAuthError) {
          await instance.acquireTokenRedirect(loginRequest);
        }
        throw e;
      });
      if (inProgress.value === InteractionStatus.None) {
        const graphData = await callMsGraph(response.accessToken);
        state.data = graphData;
        const graphDataImg = await callMsGraphImg(response.accessToken);
        dataUrl(graphDataImg);
        state.resolved = true;
        stopWatcher();
      }
    }
    function dataUrl(image) {
      urlImg.value = URL.createObjectURL(image);
    }
    function getDark(event) {
      if (localStorage) {
        localStorage.setItem("selectedTheme", event);
      }
      emit("get-dark", event);
    }
    onMounted(() => {
      getGraphData();
      props.theme = `vsm_${props.theme}`;
    });
    const stopWatcher = watch(inProgress, () => {
      if (!state.resolved) {
        getGraphData();
      }
    });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_font_awesome_icon = resolveComponent("font-awesome-icon");
      _push(`<nav${ssrRenderAttrs(mergeProps({
        class: ["navbar navbar-expand navbar-light topbar static-top shadow", `vsm_${props.theme}`]
      }, _attrs))}><div class="container-fluid">`);
      _push(ssrRenderComponent(SearchVue, null, null, _parent));
      _push(`<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent"><ul class="navbar-nav ml-auto">`);
      _push(ssrRenderComponent(ThemeButton, {
        onGetDark: ($event) => getDark($event)
      }, null, _parent));
      _push(`<div class="vr"></div><li class="nav-item dropdown no-arrow"><div class="dropdown no-arrow dropstart"><a href="#" class="d-flex align-items-center justify-content-center p-3 link-dark text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><img${ssrRenderAttr("src", unref(urlImg))} alt="mdo" class="rounded-circle" width="35" height="35"></a><ul class="dropdown-menu dropdown-menu-dark"><li><h6 class="dropdown-header">${ssrInterpolate(state.data.displayName)}</h6></li><li><hr class="dropdown-divider"></li><li><a class="dropdown-item">`);
      _push(ssrRenderComponent(_component_font_awesome_icon, { icon: "fa-solid fa-palette" }, null, _parent));
      _push(` Mon Th\xE8me</a></li><li><a class="dropdown-item">`);
      _push(ssrRenderComponent(_component_font_awesome_icon, { icon: "fa-solid fa-right-from-bracket" }, null, _parent));
      _push(` D\xE9connexion</a></li></ul></div></li></ul></div></div></nav>`);
    };
  }
});
const _sfc_setup$b = _sfc_main$b.setup;
_sfc_main$b.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Navbar/Navbar.vue");
  return _sfc_setup$b ? _sfc_setup$b(props, ctx) : void 0;
};
const faIcon = (props) => {
  return {
    element: markRaw({
      render: () => h("div", [
        h(FontAwesomeIcon, { size: "lg", ...props })
      ])
    })
  };
};
const _sfc_main$a = {
  components: {
    Sidebar,
    Navbar: _sfc_main$b
  },
  data() {
    return {
      items: this.getMenu(),
      collapsed: true,
      themes: [
        {
          name: "Default theme",
          input: ""
        },
        {
          name: "White theme",
          input: "white-theme"
        }
      ],
      selectedTheme: localStorage.selectedTheme,
      isOnMobile: true
    };
  },
  setup() {
    const currentRoute = computed(() => {
      return useRoute().name || " ";
    });
    return { currentRoute };
  },
  mounted() {
    this.onResize();
    window.addEventListener("resize", this.onResize);
    if (localStorage.selectedTheme) {
      this.selectedTheme = localStorage.selectedTheme;
    }
    if (localStorage.collapsed) {
      const isTrueSet = localStorage.collapsed === "true";
      this.collapsed = isTrueSet;
    }
  },
  methods: {
    getDark(event) {
      if (event) {
        this.selectedTheme = "";
      } else {
        this.selectedTheme = "white-theme";
      }
      localStorage.setItem("selectedTheme", this.selectedTheme);
    },
    onToggleCollapse(collapsed) {
      localStorage.setItem("collapsed", collapsed);
    },
    onItemClick(event, item) {
    },
    onResize() {
      if (window.innerWidth <= 767) {
        this.isOnMobile = true;
        this.collapsed = true;
      } else {
        this.isOnMobile = false;
        this.collapsed = false;
      }
    },
    getMenu() {
      const items = this.$router.options.routes;
      let menu = items.map((item) => {
        delete item.component;
        let child = [];
        if (item.children !== void 0) {
          child = item.children.map((i) => {
            i.path = item.path + "/" + i.path;
            delete i.component;
            i.nameIcon = i.icon;
            i.icon = faIcon({ icon: i.icon });
            return { ...i };
          });
        }
        item.nameIcon = item.icon;
        item.icon = faIcon({ icon: item.icon });
        return { children: child, ...item };
      });
      return menu;
    },
    getIcon(currentRoute) {
      let found = this.items.find((element) => element.name === currentRoute);
      let icon = (found == null ? void 0 : found.nameIcon) || "";
      if (!found) {
        found = this.items.find((element) => element.children.find((el) => el.name === currentRoute));
        let foundChild = found == null ? void 0 : found.children.find((el) => el.name === currentRoute);
        icon = (foundChild == null ? void 0 : foundChild.nameIcon) || "fa-solid fa-bug";
      }
      return icon;
    }
  }
};
const App_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$9(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Sidebar = resolveComponent("Sidebar");
  const _component_Navbar = resolveComponent("Navbar");
  const _component_font_awesome_icon = resolveComponent("font-awesome-icon");
  const _component_router_view = resolveComponent("router-view");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Sidebar, {
    menu: $data.items,
    collapsed: $data.collapsed,
    "onUpdate:collapsed": [($event) => $data.collapsed = $event, $options.onToggleCollapse],
    theme: $data.selectedTheme,
    "show-one-child": true,
    onItemClick: $options.onItemClick
  }, null, _parent));
  if ($data.isOnMobile && !$data.collapsed) {
    _push(`<div class="sidebar-overlay"></div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<div id="containerApp" class="${ssrRenderClass([{ "collapsed": $data.collapsed }, { "onmobile": false }])}">`);
  _push(ssrRenderComponent(_component_Navbar, {
    onGetDark: ($event) => $options.getDark($event),
    theme: $data.selectedTheme
  }, null, _parent));
  if ($setup.currentRoute != "Welcome") {
    _push(`<div><header class="page-header page-header-dark pb-10"><div class="container-xl px-4"><div class="page-header-content pt-4"><div class="row align-items-center justify-content-between"><div class="col-auto mt-4"><h1 class="page-header-title"><div class="page-header-icon">`);
    _push(ssrRenderComponent(_component_font_awesome_icon, {
      icon: $options.getIcon($setup.currentRoute)
    }, null, _parent));
    _push(`</div> ${ssrInterpolate($setup.currentRoute)}</h1><div class="page-header-subtitle">La DEP a \xE9t\xE9 mis \xE0 jour le 06/12/2022 - 16:18:29</div></div></div></div></div></header><div class="container-fluid container-xl px-4 mt-n10">`);
    _push(ssrRenderComponent(_component_router_view, null, null, _parent));
    _push(`</div></div>`);
  } else {
    _push(`<div><header class="page-header page-header-dark pb-10"><div class="container-xl px-4"><div class="page-header-content pt-4"><div class="row align-items-center justify-content-between"><div class="col-auto mt-4"><h1 class="page-header-title"><div class="page-header-icon">`);
    _push(ssrRenderComponent(_component_font_awesome_icon, { icon: "fa-solid fa-hands-clapping" }, null, _parent));
    _push(`</div> Bonjour Baptiste </h1><div class="page-header-subtitle">Tu peux retrouver l&#39;actu de Willemse ci-dessous `);
    _push(ssrRenderComponent(_component_font_awesome_icon, { icon: "fa-solid fa-arrow-down" }, null, _parent));
    _push(`</div><div class="page-header-subtitle">Si tu veux ajouter ou changer des informations du Dashboard, vous pouvez vous rapprochez de Batou</div></div></div></div></div></header><div class="container-fluid container-xl px-4 mt-n10">`);
    _push(ssrRenderComponent(_component_router_view, null, null, _parent));
    _push(`</div></div>`);
  }
  _push(`</div><!--]-->`);
}
const _sfc_setup$a = _sfc_main$a.setup;
_sfc_main$a.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/App.vue");
  return _sfc_setup$a ? _sfc_setup$a(props, ctx) : void 0;
};
const App = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["ssrRender", _sfc_ssrRender$9]]);
class CustomNavigationClient extends NavigationClient {
  constructor(router2) {
    super();
    __publicField(this, "router");
    this.router = router2;
  }
  async navigateInternal(url, options) {
    const relativePath = url.replace(window.location.origin, "");
    if (options.noHistory) {
      this.router.replace(relativePath);
    } else {
      this.router.push(relativePath);
    }
    return false;
  }
}
function accountArraysAreEqual(arrayA, arrayB) {
  if (arrayA.length !== arrayB.length) {
    return false;
  }
  const comparisonArray = [...arrayB];
  return arrayA.every((elementA) => {
    const elementB = comparisonArray.shift();
    if (!elementA || !elementB) {
      return false;
    }
    return elementA.homeAccountId === elementB.homeAccountId && elementA.localAccountId === elementB.localAccountId && elementA.username === elementB.username;
  });
}
const msalPlugin = {
  install: (app2, msalInstance2) => {
    const inProgress = InteractionStatus.Startup;
    const accounts2 = msalInstance2.getAllAccounts();
    const state = reactive({
      instance: msalInstance2,
      inProgress,
      accounts: accounts2
    });
    app2.config.globalProperties.$msal = state;
    msalInstance2.addEventCallback((message) => {
      switch (message.eventType) {
        case EventType.ACCOUNT_ADDED:
        case EventType.ACCOUNT_REMOVED:
        case EventType.LOGIN_SUCCESS:
        case EventType.SSO_SILENT_SUCCESS:
        case EventType.HANDLE_REDIRECT_END:
        case EventType.LOGIN_FAILURE:
        case EventType.SSO_SILENT_FAILURE:
        case EventType.LOGOUT_END:
        case EventType.ACQUIRE_TOKEN_SUCCESS:
        case EventType.ACQUIRE_TOKEN_FAILURE:
          const currentAccounts = msalInstance2.getAllAccounts();
          if (!accountArraysAreEqual(currentAccounts, state.accounts)) {
            state.accounts = currentAccounts;
          }
          break;
      }
      const status = EventMessageUtils.getInteractionStatusFromEvent(message, state.inProgress);
      if (status !== null) {
        state.inProgress = status;
      }
    });
  }
};
function registerGuard(router2) {
  router2.beforeEach(async (to, from) => {
    if (to.meta.requiresAuth) {
      const request = {
        ...loginRequest,
        redirectStartPage: to.fullPath
      };
      const shouldProceed = await isAuthenticated(msalInstance, InteractionType.Redirect, request);
      return shouldProceed || "/failed";
    }
    return true;
  });
}
async function isAuthenticated(instance, interactionType, loginRequest2) {
  return instance.handleRedirectPromise().then(() => {
    const accounts2 = instance.getAllAccounts();
    if (accounts2.length > 0) {
      return true;
    }
    if (interactionType === InteractionType.Popup) {
      return instance.loginPopup(loginRequest2).then(() => {
        return true;
      }).catch(() => {
        return false;
      });
    } else if (interactionType === InteractionType.Redirect) {
      return instance.loginRedirect(loginRequest2).then(() => {
        return true;
      }).catch(() => {
        return false;
      });
    }
    return false;
  }).catch(() => {
    return false;
  });
}
const _sfc_main$9 = /* @__PURE__ */ defineComponent({
  __name: "AuthCallback",
  __ssrInlineRender: true,
  setup(__props) {
    const { instance, inProgress } = useMsal();
    let urlImg = ref("");
    const state = reactive({
      resolved: false,
      data: {}
    });
    async function getGraphData() {
      const response = await instance.acquireTokenSilent({
        ...loginRequest
      }).catch(async (e) => {
        if (e instanceof InteractionRequiredAuthError) {
          await instance.acquireTokenRedirect(loginRequest);
        }
        throw e;
      });
      if (inProgress.value === InteractionStatus.None) {
        const graphData = await callMsGraph(response.accessToken);
        console.log(graphData);
        state.data = graphData;
        const graphDataImg = await callMsGraphImg(response.accessToken);
        console.log(graphDataImg);
        dataUrl(graphDataImg);
        state.resolved = true;
        stopWatcher();
      }
    }
    function dataUrl(image) {
      urlImg.value = URL.createObjectURL(image);
    }
    onMounted(() => {
      getGraphData();
    });
    const stopWatcher = watch(inProgress, () => {
      if (!state.resolved) {
        getGraphData();
      }
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<!--[--><div><h1>Page de callback d&#39;authentification</h1><p>Traitement de la r\xE9ponse d&#39;authentification...</p></div>`);
      if (state.resolved) {
        _push(`<div><p>Name: ${ssrInterpolate(state.data.displayName)}</p><p>Title: ${ssrInterpolate(state.data.jobTitle)}</p><p>Mail: ${ssrInterpolate(state.data.mail)}</p><p>Phone: ${ssrInterpolate(state.data.businessPhones ? state.data.businessPhones[0] : "")}</p><p>Location: ${ssrInterpolate(state.data.officeLocation)}</p><img${ssrRenderAttr("src", unref(urlImg))}></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`<!--]-->`);
    };
  }
});
const _sfc_setup$9 = _sfc_main$9.setup;
_sfc_main$9.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/views/AuthCallback.vue");
  return _sfc_setup$9 ? _sfc_setup$9(props, ctx) : void 0;
};
const _sfc_main$8 = {};
function _sfc_ssrRender$8(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<!--[--><div class="row container-xl px-4 mt-10"><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-primary shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Earnings (Monthly)</div><div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div></div><div class="col-auto"><i class="fas fa-calendar fa-2x text-gray-300"></i></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-success shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Earnings (Annual)</div><div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div></div><div class="col-auto"><i class="fas fa-dollar-sign fa-2x text-gray-300"></i></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-info shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks </div><div class="row no-gutters align-items-center"><div class="col-auto"><div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div></div><div class="col"><div class="progress progress-sm mr-2"><div class="progress-bar bg-info" role="progressbar" style="${ssrRenderStyle({ "width": "50%" })}" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div></div></div></div></div><div class="col-auto"><i class="fas fa-clipboard-list fa-2x text-gray-300"></i></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-warning shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-warning text-uppercase mb-1"> Pending Requests</div><div class="h5 mb-0 font-weight-bold text-gray-800">18</div></div><div class="col-auto"><i class="fas fa-comments fa-2x text-gray-300"></i></div></div></div></div></div></div><div class="row"><div class="col-xl-8 col-lg-7"><div class="card shadow mb-4"><div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6><div class="dropdown no-arrow"><a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i></a><div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" style="${ssrRenderStyle({})}"><div class="dropdown-header">Dropdown Header:</div><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><div class="dropdown-divider"></div><a class="dropdown-item" href="#">Something else here</a></div></div></div><div class="card-body"><div class="chart-area"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div><canvas id="myAreaChart" style="${ssrRenderStyle({ "display": "block", "height": "320px", "width": "782px" })}" class="chartjs-render-monitor" width="977" height="400"></canvas></div></div></div></div><div class="col-xl-4 col-lg-5"><div class="card shadow mb-4"><div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="m-0 font-weight-bold text-primary">Revenue Sources</h6><div class="dropdown no-arrow"><a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i></a><div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink"><div class="dropdown-header">Dropdown Header:</div><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><div class="dropdown-divider"></div><a class="dropdown-item" href="#">Something else here</a></div></div></div><div class="card-body"><div class="chart-pie pt-4 pb-2"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div><canvas id="myPieChart" style="${ssrRenderStyle({ "display": "block", "height": "245px", "width": "358px" })}" class="chartjs-render-monitor" width="447" height="306"></canvas></div><div class="mt-4 text-center small"><span class="mr-2"><i class="fas fa-circle text-primary"></i> Direct </span><span class="mr-2"><i class="fas fa-circle text-success"></i> Social </span><span class="mr-2"><i class="fas fa-circle text-info"></i> Referral </span></div></div></div></div></div><div class="row"><div class="col-lg-6 mb-4"><div class="card shadow mb-4"><div class="card-header py-3"><h6 class="m-0 font-weight-bold text-primary">Op\xE9ration</h6></div><div class="card-body"><h4 class="small font-weight-bold">Server Migration <span class="float-right">20%</span></h4><div class="progress mb-4"><div class="progress-bar bg-danger" role="progressbar" style="${ssrRenderStyle({ "width": "20%" })}" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div></div><h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4><div class="progress mb-4"><div class="progress-bar bg-warning" role="progressbar" style="${ssrRenderStyle({ "width": "40%" })}" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div></div><h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4><div class="progress mb-4"><div class="progress-bar" role="progressbar" style="${ssrRenderStyle({ "width": "60%" })}" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div></div><h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4><div class="progress mb-4"><div class="progress-bar bg-info" role="progressbar" style="${ssrRenderStyle({ "width": "80%" })}" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div></div><h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4><div class="progress"><div class="progress-bar bg-success" role="progressbar" style="${ssrRenderStyle({ "width": "100%" })}" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div></div></div></div><div class="row"><div class="col-lg-6 mb-4"><div class="card bg-primary text-white shadow"><div class="card-body"> Primary <div class="text-white-50 small">#4e73df</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-success text-white shadow"><div class="card-body"> Success <div class="text-white-50 small">#1cc88a</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-info text-white shadow"><div class="card-body"> Info <div class="text-white-50 small">#36b9cc</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-warning text-white shadow"><div class="card-body"> Warning <div class="text-white-50 small">#f6c23e</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-danger text-white shadow"><div class="card-body"> Danger <div class="text-white-50 small">#e74a3b</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-secondary text-white shadow"><div class="card-body"> Secondary <div class="text-white-50 small">#858796</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-light text-black shadow"><div class="card-body"> Light <div class="text-black-50 small">#f8f9fc</div></div></div></div><div class="col-lg-6 mb-4"><div class="card bg-dark text-white shadow"><div class="card-body"> Dark <div class="text-white-50 small">#5a5c69</div></div></div></div></div></div><div class="col-lg-6 mb-4"><div class="card shadow mb-4"><div class="card-header py-3"><h6 class="m-0 font-weight-bold text-primary">Illustrations</h6></div><div class="card-body"><div class="text-center"><img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="${ssrRenderStyle({ "width": "25rem" })}" src="img/undraw_posting_photo.svg" alt="..."></div><p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p><a target="_blank" rel="nofollow" href="https://undraw.co/">Browse Illustrations on unDraw \u2192</a></div></div><div class="card shadow mb-4"><div class="card-header py-3"><h6 class="m-0 font-weight-bold text-primary">Development Approach</h6></div><div class="card-body"><p>SB Admin 2 makes extensive use of Bootstrap 4 utility classes in order to reduce CSS bloat and poor page performance. Custom CSS classes are used to create custom components and custom utility classes.</p><p class="mb-0">Before working with this theme, you should become familiar with the Bootstrap framework, especially the utility classes.</p></div></div></div></div><!--]-->`);
}
const _sfc_setup$8 = _sfc_main$8.setup;
_sfc_main$8.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/views/Dashboard.vue");
  return _sfc_setup$8 ? _sfc_setup$8(props, ctx) : void 0;
};
const Dashboard = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["ssrRender", _sfc_ssrRender$8]]);
const axiosClient$2 = axios.create({
  baseURL: "https://biloba-vue3.test/api/orders/partielles"
});
async function getPartielles(do_type) {
  try {
    const { data } = await axiosClient$2.get(`/${do_type}`);
    return [null, data];
  } catch (error) {
    return [error];
  }
}
const _sfc_main$7 = {
  props: {
    child: {
      type: Object,
      default: {}
    }
  },
  data() {
    return {
      opened: []
    };
  },
  methods: {
    getClass(annee, semaine) {
      const weekNumber = DateTime.now().weekNumber;
      const year = DateTime.now().year;
      let classTD = "";
      if (semaine == weekNumber && annee == year) {
        classTD = "table-info";
        console.log("=");
      } else if (semaine < weekNumber && annee == year || annee < year) {
        classTD = "table-danger";
      }
      return classTD;
    }
  },
  mounted() {
  }
};
const TableSubRow_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$7(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<table${ssrRenderAttrs(mergeProps({
    class: "table table-responsive",
    style: { "font-size": "10px" }
  }, _attrs))}><thead class="text-center"><tr class="text-center"><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Sem</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>LDF</th><th>Total</th><th style="${ssrRenderStyle({ "border-left": "5px solid #ddd" })}">Web</th><th>BK</th><th>Print</th><th>VP</th><th>Autres</th><th>LDF</th><th>Total</th></tr></thead><tbody><!--[-->`);
  ssrRenderList($props.child, (row) => {
    _push(`<tr class="text-center"><td class="${ssrRenderClass($options.getClass(row.annee, row.semaine))}" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.DO_DateLivr)}</td><td>${ssrInterpolate(row.monoWeb)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.Web)}</td><td>${ssrInterpolate(row.monoBK)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.BK)}</td><td>${ssrInterpolate(row.monoCata)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.Catalogue)}</td><td>${ssrInterpolate(row.monoVP)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.VP)}</td><td>${ssrInterpolate(row.monoAutres)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.Autres)}</td><td>${ssrInterpolate(row.LDF)}</td><td class="table-success" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate(row.TotalCom)}</td><td>${ssrInterpolate(row.WebP)}</td><td>${ssrInterpolate(row.BKP)}</td><td>${ssrInterpolate(row.CatalogueP)}</td><td>${ssrInterpolate(row.VPP)}</td><td>${ssrInterpolate(row.AutresP)}</td><td>${ssrInterpolate(row.LDFP)}</td><td class="table-success">${ssrInterpolate(row.TotalComP)}</td></tr>`);
  });
  _push(`<!--]--></tbody></table>`);
}
const _sfc_setup$7 = _sfc_main$7.setup;
_sfc_main$7.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/orders/partielles/Table/TableSubRow.vue");
  return _sfc_setup$7 ? _sfc_setup$7(props, ctx) : void 0;
};
const TableSubRowVue = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["ssrRender", _sfc_ssrRender$7]]);
const _sfc_main$6 = {
  components: { TableSubRowVue },
  props: {
    row: {
      type: Object,
      default: {}
    }
  },
  data() {
    return {
      opened: [],
      classTD: "",
      classTR: "",
      subDay: []
    };
  },
  methods: {
    toggle(id) {
      const index = this.opened.indexOf(id);
      if (index > -1) {
        this.opened.splice(index, 1);
      } else {
        this.opened.push(id);
      }
    }
  },
  mounted() {
    const weekNumber = DateTime.now().weekNumber;
    const year = DateTime.now().year;
    if (this.$props.row.DO_DateLivr == weekNumber && this.$props.row.annee == year) {
      this.classTD = "table-info";
    } else if (this.$props.row.DO_DateLivr < weekNumber && this.$props.row.annee == year || this.$props.row.annee < year) {
      this.classTD = "table-danger";
    }
  }
};
const TableRow_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$6(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_TableSubRowVue = resolveComponent("TableSubRowVue");
  _push(`<!--[--><tr class="${ssrRenderClass([$data.classTR, "text-center"])}"><td colspan="2" class="${ssrRenderClass($data.classTD)}" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.DO_DateLivr)} - ${ssrInterpolate($props.row.annee)}</td><td>${ssrInterpolate($props.row.monoWeb)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.Web)}</td><td>${ssrInterpolate($props.row.monoBK)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.BK)}</td><td>${ssrInterpolate($props.row.monoCata)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.Catalogue)}</td><td>${ssrInterpolate($props.row.monoVP)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.VP)}</td><td>${ssrInterpolate($props.row.monoAutres)}</td><td style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.Autres)}</td><td>${ssrInterpolate($props.row.LDF)}</td><td class="table-success" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">${ssrInterpolate($props.row.TotalCom)}</td><td>${ssrInterpolate($props.row.WebP)}</td><td>${ssrInterpolate($props.row.BKP)}</td><td>${ssrInterpolate($props.row.CatalogueP)}</td><td>${ssrInterpolate($props.row.VPP)}</td><td>${ssrInterpolate($props.row.AutresP)}</td><td>${ssrInterpolate($props.row.LDFP)}</td><td class="table-success">${ssrInterpolate($props.row.TotalComP)}</td></tr>`);
  if ($data.opened.includes($props.row.id)) {
    _push(`<tr class="collapsed"><td colspan="21">`);
    _push(ssrRenderComponent(_component_TableSubRowVue, {
      child: $props.row.child
    }, null, _parent));
    _push(`</td></tr>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<!--]-->`);
}
const _sfc_setup$6 = _sfc_main$6.setup;
_sfc_main$6.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/orders/partielles/Table/TableRow.vue");
  return _sfc_setup$6 ? _sfc_setup$6(props, ctx) : void 0;
};
const TableRowVue = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["ssrRender", _sfc_ssrRender$6]]);
const _sfc_main$5 = {
  components: {
    TableRowVue
  },
  props: {
    datas: {
      type: Object,
      default: {}
    }
  },
  data() {
    return {
      opened: [],
      day: {}
    };
  },
  methods: {
    toggle(id) {
      const index = this.opened.indexOf(id);
      if (index > -1) {
        this.opened.splice(index, 1);
      } else {
        this.opened.push(id);
      }
    }
  }
};
const Table_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$5(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_TableRowVue = resolveComponent("TableRowVue");
  _push(`<table${ssrRenderAttrs(mergeProps({
    class: "table table-responsive",
    style: { "font-size": "10px" }
  }, _attrs))}><thead class="text-center"><tr><th class="text-center"></th><th class="text-center"></th><th colspan="12" class="text-center">Pleines</th><th colspan="7" class="text-center" style="${ssrRenderStyle({ "border-left": "5px solid #ddd" })}">Partielles</th></tr><tr><th class="text-center"></th><th class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">D.E.P</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Web</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">BK</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Print</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">VP</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">MK</th><th colspan="2" class="text-center" style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">LDF</th><th colspan="7" class="text-center"></th></tr><tr class="text-center"><th>+</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Sem</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>Mo</th><th style="${ssrRenderStyle({ "border-right": "5px solid #ddd" })}">Mu</th><th>LDF</th><th>Total</th><th style="${ssrRenderStyle({ "border-left": "5px solid #ddd" })}">Web</th><th>BK</th><th>Print</th><th>VP</th><th>Autres</th><th>LDF</th><th>Total</th></tr></thead><tbody><!--[-->`);
  ssrRenderList($props.datas, (row) => {
    _push(`<!--[-->`);
    if (row.TotalCom > 0 || row.TotalComP > 0) {
      _push(ssrRenderComponent(_component_TableRowVue, { row }, null, _parent));
    } else {
      _push(`<!---->`);
    }
    _push(`<!--]-->`);
  });
  _push(`<!--]--></tbody></table>`);
}
const _sfc_setup$5 = _sfc_main$5.setup;
_sfc_main$5.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/orders/partielles/Table/Table.vue");
  return _sfc_setup$5 ? _sfc_setup$5(props, ctx) : void 0;
};
const Table = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["ssrRender", _sfc_ssrRender$5]]);
const _sfc_main$4 = {
  name: "LoadingBar"
};
const Loading_vue_vue_type_style_index_0_scoped_bf33c572_lang = "";
function _sfc_ssrRender$4(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "loader" }, _attrs))} data-v-bf33c572></div>`);
}
const _sfc_setup$4 = _sfc_main$4.setup;
_sfc_main$4.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/partials/Datas/Loading.vue");
  return _sfc_setup$4 ? _sfc_setup$4(props, ctx) : void 0;
};
const LoadingBar = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["ssrRender", _sfc_ssrRender$4], ["__scopeId", "data-v-bf33c572"]]);
const _sfc_main$3 = {
  components: {
    Table,
    LoadingBar
  },
  data() {
    return {
      datas: {},
      isLoad: false,
      do_type: 1
    };
  },
  setup(props, context) {
    const selectedItem = ref(0);
    let isActive = false;
    const items = ["Commandes", "Pr\xE9parations"];
    const selectItem = (i) => {
      selectedItem.value = i;
      items.forEach((item, index) => {
        return isActive = item == items[index];
      });
      return selectedItem;
    };
    return {
      items,
      selectedItem,
      selectItem,
      isActive
    };
  },
  watch: {
    dotype() {
      this.updateData(this.do_type);
    }
  },
  methods: {
    async updateData(do_type) {
      this.isLoad = false;
      const [error, data] = await getPartielles(do_type);
      if (error)
        console.error(error);
      else {
        this.datas = data;
        this.isLoad = true;
      }
    },
    async updateDotype(do_type) {
      this.do_type = do_type;
    }
  },
  async created() {
    this.updateData(1);
  }
};
const Partielles_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$3(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Table = resolveComponent("Table");
  const _component_LoadingBar = resolveComponent("LoadingBar");
  if ($data.isLoad) {
    _push(`<div${ssrRenderAttrs(mergeProps({ class: "mx-auto shadow-sm p-3 mb-5 bg-body rounded row" }, _attrs))}><div class="row justify-content"><ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist"><!--[-->`);
    ssrRenderList($setup.items, (item, index) => {
      _push(`<li class="${ssrRenderClass([{ "active": index == $setup.selectedItem }, "nav-link"])}">${ssrInterpolate(item)}</li>`);
    });
    _push(`<!--]--></ul></div>`);
    _push(ssrRenderComponent(_component_Table, { datas: $data.datas }, null, _parent));
    _push(`</div>`);
  } else {
    _push(`<div${ssrRenderAttrs(mergeProps({ class: "mx-auto shadow-sm p-3 mb-5 bg-body rounded row" }, _attrs))}>`);
    _push(ssrRenderComponent(_component_LoadingBar, null, null, _parent));
    _push(`</div>`);
  }
}
const _sfc_setup$3 = _sfc_main$3.setup;
_sfc_main$3.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/orders/partielles/Partielles.vue");
  return _sfc_setup$3 ? _sfc_setup$3(props, ctx) : void 0;
};
const Partielle = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["ssrRender", _sfc_ssrRender$3]]);
const axiosClient$1 = axios.create({
  baseURL: "https://biloba-vue3.test/api/products"
});
async function getProduct(ref2) {
  try {
    const { data } = await axiosClient$1.get(`/${ref2}`);
    return [null, data];
  } catch (error) {
    return [error];
  }
}
const _sfc_main$2 = {
  props: {
    id: {
      type: String,
      required: true
    }
  },
  data() {
    return {
      ref: "",
      product: {
        refart: "",
        name: "",
        statut: "",
        nomencl: "",
        fournisseur: "",
        delai_fournisseur: "",
        methode_vente: "",
        ldf: "",
        en_sommeil: "",
        nom_fournisseur: "",
        categorie_web: "",
        url: "",
        coverImage: "",
        descriptionLength: "",
        imagesCount: "",
        dispo_fournisseur: "",
        INTITULE_LATIN: "",
        cbCreation: "",
        BLOCAGE_VENTE_WEB: ""
      }
    };
  },
  watch: {
    id() {
      this.getProduct();
    }
  },
  async mounted() {
    this.getProduct();
  },
  methods: {
    async getProduct() {
      this.ref = this.$props.id;
      const [error, data] = await getProduct(this.ref);
      if (error)
        console.error(error);
      else {
        this.$emit("get-name", data.name);
        this.product = data;
      }
    }
  }
};
function _sfc_ssrRender$2(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  if ($data.product) {
    _push(`<div${ssrRenderAttrs(_attrs)}><div class="row"><div class="col-3" style="${ssrRenderStyle({ "position": "unset" })}"><img${ssrRenderAttr("src", $data.product.coverImage)} class="img-fluid rounded"></div><div class="col-9"><table class="table table-borderless table-sm" style="${ssrRenderStyle({ "font-size": "12px" })}"><tbody><tr><td><h5>Ref ${ssrInterpolate($data.product.refart)} <a target="_blank" class="btn btn-secondary btn-sm shadow rounded"> Historique <i class="fas fa-external-link-alt"></i></a></h5></td></tr><tr><td></td></tr><tr><td> Fournisseur: ${ssrInterpolate($data.product.nom_fournisseur)} [${ssrInterpolate($data.product.fournisseur)}] `);
    if ($data.product.statut === "Commercialis\xE9") {
      _push(`<span class="badge badge-pill badge-success">${ssrInterpolate($data.product.statut)}</span>`);
    } else {
      _push(`<span class="badge badge-pill badge-danger">${ssrInterpolate($data.product.statut)}</span>`);
    }
    _push(`</td></tr><tr><td>Nomenclature: ${ssrInterpolate($data.product.nomencl)}</td></tr><tr><td>SEO: </td></tr><tr><td>En vente depuis: ${ssrInterpolate($data.product.cbCreation)}</td></tr><tr><td>Blocage Web : ${ssrInterpolate($data.product.BLOCAGE_VENTE_WEB)}</td></tr><tr><td>Dispo fourni : ${ssrInterpolate($data.product.dispo_fournisseur)}</td></tr></tbody></table></div></div></div>`);
  } else {
    _push(`<div${ssrRenderAttrs(_attrs)}> loader... </div>`);
  }
}
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/products/product/Product.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const Product = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["ssrRender", _sfc_ssrRender$2]]);
const axiosClient = axios.create({
  baseURL: "https://biloba-vue3.test/api/products/cf"
});
async function getCfsSuivi() {
  try {
    const { data } = await axiosClient.get("/suivi/all");
    return [null, data];
  } catch (error) {
    return [error];
  }
}
const _sfc_main$1 = defineComponent({
  components: {},
  setup() {
    const headers = [
      { text: "N\xB0 CF", value: "do_piece", fixed: true },
      { text: "D\xE9p\xF4t Stock", value: "de_no" },
      { text: "D\xE9p\xF4t Livr.", value: "INFO_LIVRAISON_1" },
      { text: "Etat du CF", value: "do_status" },
      { text: "Date cr\xE9a", value: "do_date" },
      { text: "Date livr", value: "do_datelivr" },
      { text: "Fournisseur", value: "ct_intitule" },
      { text: "Nb refs", value: "nb_ref" },
      { text: "Nb pces", value: "nb_pce" },
      { text: "Montant HT (restant)", value: "do_totalht" },
      { text: "Mention", value: "reliquat" },
      { text: "Statut du mail", value: "MailEnvoye" }
    ];
    ref([]);
    const items = ref([]);
    const loading = ref(true);
    async function updateData() {
      const [error, data] = await getCfsSuivi();
      if (error)
        console.error(error);
      else {
        console.log(data);
        loading.value = false;
        items.value = data;
      }
    }
    return {
      loading,
      updateData,
      headers,
      items
    };
  },
  async created() {
    this.updateData();
  }
});
const SuiviCF_vue_vue_type_style_index_0_lang = "";
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_EasyDataTable = resolveComponent("EasyDataTable");
  _push(`<!--[--><div class="row container-xl px-4"><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-primary shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> ZONE 1 </div></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-success shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-success text-uppercase mb-1"> ZONE 2 </div></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-info shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-info text-uppercase mb-1"> ZONE 3 </div></div></div></div></div></div><div class="col-xl-3 col-md-6 mb-4"><div class="card border-left-warning shadow h-100 py-2"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-warning text-uppercase mb-1"> ZONE 4 </div></div></div></div></div></div></div>`);
  _push(ssrRenderComponent(_component_EasyDataTable, {
    alternating: "",
    "items-selected": _ctx.itemsSelected,
    "onUpdate:items-selected": ($event) => _ctx.itemsSelected = $event,
    "buttons-pagination": "",
    headers: _ctx.headers,
    items: _ctx.items,
    loading: _ctx.loading,
    "theme-color": "#f48225"
  }, {
    player: withCtx(({ player, avator, page }, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="player-wrapper"${_scopeId}><img class="avator"${ssrRenderAttr("src", avator)} alt=""${_scopeId}><a target="_blank"${ssrRenderAttr("href", page)}${_scopeId}>${ssrInterpolate(player)}</a></div>`);
      } else {
        return [
          createVNode("div", { class: "player-wrapper" }, [
            createVNode("img", {
              class: "avator",
              src: avator,
              alt: ""
            }, null, 8, ["src"]),
            createVNode("a", {
              target: "_blank",
              href: page
            }, toDisplayString(player), 9, ["href"])
          ])
        ];
      }
    }),
    "item-team": withCtx(({ teamName, teamUrl }, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<a target="_blank"${ssrRenderAttr("href", teamUrl)}${_scopeId}>${ssrInterpolate(teamName)}</a>`);
      } else {
        return [
          createVNode("a", {
            target: "_blank",
            href: teamUrl
          }, toDisplayString(teamName), 9, ["href"])
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/components/products/CF/SuiviCF.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const SuiviCF = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  components: {
    Dashboard
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Dashboard = resolveComponent("Dashboard");
  _push(`<div${ssrRenderAttrs(_attrs)}>`);
  _push(ssrRenderComponent(_component_Dashboard, null, null, _parent));
  _push(`</div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/views/Welcome.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Welcome = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
const routes = [
  {
    path: "/",
    component: Welcome,
    name: "Welcome",
    icon: "fa-solid fa-hands-clapping",
    hidden: true
  },
  {
    path: "/auth/callback",
    component: _sfc_main$9,
    icon: "fa-solid fa-box",
    hidden: true
  },
  {
    path: "/orders",
    name: "Commandes",
    icon: "fa-solid fa-cart-shopping",
    meta: { requiresAuth: true },
    children: [
      {
        path: "partielles",
        name: "Partielles",
        component: Partielle,
        icon: "fa-solid fa-box",
        hidden: false,
        meta: { requiresAuth: true }
      }
    ]
  },
  {
    path: "/products",
    name: "Produits",
    icon: "fa-solid fa-seedling",
    children: [
      {
        path: "product/:id",
        name: "Product",
        component: Product,
        props: (route) => ({
          id: route.params.id
        }),
        icon: "fa-solid fa-seedling",
        hidden: true
      },
      {
        path: "cfcreation",
        name: "Cr\xE9ation CF",
        component: Dashboard,
        icon: "fa-solid fa-receipt",
        hidden: false
      },
      {
        path: "cfsuivi",
        name: "Suivi CF",
        component: SuiviCF,
        icon: "fa-solid fa-truck-ramp-box",
        hidden: false
      }
    ]
  },
  {
    path: "/supplier",
    name: "Fournisseur",
    icon: "fa-solid fa-truck-field",
    children: [
      {
        path: "cfcreation",
        name: "TEST 2",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    icon: "fa-solid fa-table-columns"
  },
  {
    path: "/offre",
    name: "Offre Commerciale",
    icon: "fa-solid fa-calendar-days",
    children: [
      {
        path: "cfcreation",
        name: "TEST 3",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/check",
    name: "Checks",
    icon: "fa-solid fa-clipboard-check",
    children: [
      {
        path: "cfcreation",
        name: "TEST 4",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/tools",
    name: "Outils",
    icon: "fa-solid fa-screwdriver-wrench",
    children: [
      {
        path: "cfcreation",
        name: "Creation CF",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/rc",
    name: "Relation Client",
    icon: "fa-solid fa-headset",
    children: [
      {
        path: "cfcreation",
        name: "TEST 5",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/logistque",
    name: "Logistque",
    icon: "fa-solid fa-warehouse",
    children: [
      {
        path: "cfcreation",
        name: "TEST 6",
        component: Partielle,
        icon: "fa-solid fa-receipt",
        hidden: false
      }
    ]
  },
  {
    path: "/admin",
    name: "Administration",
    icon: "fa-solid  fa-user-shield"
  }
];
const router = createRouter({
  history: createWebHistory(),
  routes
});
registerGuard(router);
const pinia = createPinia();
const navigationClient = new CustomNavigationClient(router);
msalInstance.setNavigationClient(navigationClient);
const accounts = msalInstance.getAllAccounts();
if (accounts.length > 0) {
  msalInstance.setActiveAccount(accounts[0]);
}
msalInstance.addEventCallback((event) => {
  if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
    const payload = event.payload;
    const account = payload.account;
    msalInstance.setActiveAccount(account);
  }
});
const app = createApp(App);
app.use(pinia);
app.use(router);
app.use(msalPlugin, msalInstance);
app.component("font-awesome-icon", FontAwesomeIcon);
app.component("EasyDataTable", Vue3EasyDataTable);
router.isReady().then(() => {
  app.mount("#app");
});
